package interpreter.grammar.tokenizer;

import interpreter.grammar.Grammar;
import interpreter.grammar.expression.And;
import interpreter.grammar.expression.Or;
import interpreter.grammar.expression.Repeat;
import interpreter.grammar.tokenizer.exception.TokenizerException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TokenizerTest {
    @Test
    void testGrammarAndRuleWithTerminalsOnly() {
        List<Rule> rules = new ArrayList<>();
        rules.add(new Rule(Grammar.MAIN_RULE,
                new And()
                        .add(new Symbol(SymbolType.TERMINAL, "0"))
                        .add(new Symbol(SymbolType.TERMINAL, "1"))
                        .add(new Symbol(SymbolType.TERMINAL, "2"))
        ));

        List<String> tokens = getTokens(rules, "012");

        assertEquals(3, tokens.size());
        assertEquals("0", tokens.get(0));
        assertEquals("1", tokens.get(1));
        assertEquals("2", tokens.get(2));
    }

    @Test
    void testGrammarAndRuleWithMultiCharTerminalsOnly() {
        List<Rule> rules = new ArrayList<>();
        rules.add(new Rule(Grammar.MAIN_RULE,
                new And()
                        .add(new Symbol(SymbolType.TERMINAL, "abc"))
                        .add(new Symbol(SymbolType.TERMINAL, "12345"))
        ));

        List<String> tokens = getTokens(rules, "abc12345");

        assertEquals(2, tokens.size());
        assertEquals("abc", tokens.get(0));
        assertEquals("12345", tokens.get(1));
    }

    @Test
    void testGrammarAndRuleWithMultiChar() {
        List<Rule> rules = new ArrayList<>();
        rules.add(new Rule(Grammar.MAIN_RULE,
                new And()
                        .add(new Symbol(SymbolType.TERMINAL, "11"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "math_operator"))
                        .add(new Symbol(SymbolType.TERMINAL, "222"))
        ));
        rules.add(getMathOperatorRule());

        List<String> tokens = getTokens(rules, "11-222");

        assertEquals(3, tokens.size());
        assertEquals("11", tokens.get(0));
        assertEquals("-", tokens.get(1));
        assertEquals("222", tokens.get(2));
    }

    @ParameterizedTest(name = "testExceptionGrammarAndRuleWithTerminalsOnly input: {0}")
    @ValueSource(strings = {"x12", "0x2", "01x", "0xx", "xxx"})
    void testExceptionGrammarAndRuleWithTerminalsOnly(String input) {
        List<Rule> rules = new ArrayList<>();
        rules.add(new Rule(Grammar.MAIN_RULE,
                new And()
                        .add(new Symbol(SymbolType.TERMINAL, "0"))
                        .add(new Symbol(SymbolType.TERMINAL, "1"))
                        .add(new Symbol(SymbolType.TERMINAL, "2"))
        ));

        Exception exception = assertThrows(TokenizerException.class,
                () -> getTokens(rules, input));
        assertTrue(exception.getMessage().contains("Expected"));
        assertTrue(exception.getMessage().contains("but found"));
    }

    @ParameterizedTest(name = "testGrammarAndRuleWithNonTerminalsOnly input: {0}")
    @ValueSource(strings = {"0+0", "1+1", "2+2", "0+9", "1+8", "2+7", "3+6", "4+5", "5+4"})
    void testGrammarAndRuleWithNonTerminalsOnly(String input) {
        List<Rule> rules = new ArrayList<>();
        rules.add(new Rule(Grammar.MAIN_RULE,
                new And()
                        .add(new Symbol(SymbolType.NON_TERMINAL, "digit"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "math_operator"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "digit"))
        ));
        rules.add(getDigitRule());
        rules.add(getMathOperatorRule());

        List<String> tokens = getTokens(rules, input);
        char[] inputCharArray = input.toCharArray();

        assertEquals(3, tokens.size());
        assertEquals(inputCharArray[0] + "", tokens.get(0));
        assertEquals(inputCharArray[1] + "", tokens.get(1));
        assertEquals(inputCharArray[2] + "", tokens.get(2));
    }

    @ParameterizedTest(name = "testExceptionGrammarAndRuleWithNonTerminalsOnly input: {0}")
    @ValueSource(strings = {"xyz", "xy0", "x+0", "1yz", "1+z"})
    void testExceptionGrammarAndRuleWithNonTerminalsOnly(String input) {
        List<Rule> rules = new ArrayList<>();
        rules.add(new Rule(Grammar.MAIN_RULE,
                new And()
                        .add(new Symbol(SymbolType.NON_TERMINAL, "digit"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "math_operator"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "digit"))
        ));
        rules.add(getDigitRule());
        rules.add(getMathOperatorRule());

        Exception exception = assertThrows(TokenizerException.class,
                () -> getTokens(rules, input));
        assertTrue(exception.getMessage().contains("Expected"));
        assertTrue(exception.getMessage().contains("but found"));
    }

    @ParameterizedTest(name = "testExceptionGrammarAndRuleWithMultiCharWithNonTerminalsOnly input: {0}")
    @ValueSource(strings = {"123*789"})
    void testExceptionGrammarAndRuleWithMultiCharWithNonTerminalsOnly(String input) {
        List<Rule> rules = new ArrayList<>();
        rules.add(new Rule(Grammar.MAIN_RULE,
                new And()
                        .add(new Symbol(SymbolType.NON_TERMINAL, "digit"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "digit"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "digit"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "math_operator"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "digit"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "digit"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "digit"))
        ));
        rules.add(getDigitRule());
        rules.add(getMathOperatorRule());

        List<String> tokens = getTokens(rules, input);

        assertEquals(7, tokens.size());
        assertEquals("1", tokens.get(0));
        assertEquals("2", tokens.get(1));
        assertEquals("3", tokens.get(2));
        assertEquals("*", tokens.get(3));
        assertEquals("7", tokens.get(4));
        assertEquals("8", tokens.get(5));
        assertEquals("9", tokens.get(6));

    }

    @ParameterizedTest(name = "testGrammarOrRuleWithTerminalsOnly input: {0}")
    @ValueSource(strings = {"0", "1", "2"})
    void testGrammarOrRuleWithTerminalsOnly(String input) {
        List<Rule> rules = new ArrayList<>();
        rules.add(new Rule(Grammar.MAIN_RULE,
                new Or()
                        .add(new Symbol(SymbolType.TERMINAL, "0"))
                        .add(new Symbol(SymbolType.TERMINAL, "1"))
                        .add(new Symbol(SymbolType.TERMINAL, "2"))
        ));

        List<String> tokens = getTokens(rules, input);

        assertEquals(1, tokens.size());
        assertEquals(input, tokens.get(0));
    }

    @ParameterizedTest(name = "testGrammarOrRuleWithMultiCharTerminals input: {0}")
    @ValueSource(strings = {"4444", "333", "22", "1"})
    void testGrammarOrRuleWithMultiCharTerminals(String input) {
        List<Rule> rules = new ArrayList<>();
        rules.add(new Rule(Grammar.MAIN_RULE,
                new Or()
                        .add(new Symbol(SymbolType.TERMINAL, "1"))
                        .add(new Symbol(SymbolType.TERMINAL, "22"))
                        .add(new Symbol(SymbolType.TERMINAL, "333"))
                        .add(new Symbol(SymbolType.TERMINAL, "4444"))
        ));

        List<String> tokens = getTokens(rules, input);

        assertEquals(1, tokens.size());
        assertEquals(input, tokens.get(0));
    }

    @ParameterizedTest(name = "testExceptionGrammarOrRuleWithTerminalsOnly input: {0}")
    @ValueSource(strings = {"3", "4", "8"})
    void testExceptionGrammarOrRuleWithTerminalsOnly(String input) {
        List<Rule> rules = new ArrayList<>();
        rules.add(new Rule(Grammar.MAIN_RULE,
                new Or()
                        .add(new Symbol(SymbolType.TERMINAL, "0"))
                        .add(new Symbol(SymbolType.TERMINAL, "1"))
                        .add(new Symbol(SymbolType.TERMINAL, "2"))
        ));

        Exception exception = assertThrows(TokenizerException.class,
                () -> getTokens(rules, input));
        assertTrue(exception.getMessage().contains("Expected"));
        assertTrue(exception.getMessage().contains("but found %s".formatted(input)));
    }

    @ParameterizedTest(name = "testGrammarOrRuleWithNonTerminalsOnly input: {0}")
    @ValueSource(strings = {"0", "1", "2", "9", "a", "b", "c", "j"})
    void testGrammarOrRuleWithNonTerminalsOnly(String input) {
        List<Rule> rules = new ArrayList<>();
        rules.add(new Rule(Grammar.MAIN_RULE,
                new Or()
                        .add(new Symbol(SymbolType.NON_TERMINAL, "alphanumeric"))
        ));
        rules.add(getAlphanumericRule());
        rules.add(getSmallLetterRule());
        rules.add(getDigitRule());

        List<String> tokens = getTokens(rules, input);

        assertEquals(1, tokens.size());
        assertEquals(input, tokens.get(0));
    }

    @ParameterizedTest(name = "testExceptionWhenGrammarOrRuleWithNonTerminalsOnly input: {0}")
    @ValueSource(strings = {"+", "-", "{", ";"})
    void testExceptionWhenGrammarOrRuleWithNonTerminalsOnly(String input) {
        List<Rule> rules = new ArrayList<>();
        rules.add(new Rule(Grammar.MAIN_RULE,
                new Or()
                        .add(new Symbol(SymbolType.NON_TERMINAL, "alphanumeric"))
        ));
        rules.add(getAlphanumericRule());
        rules.add(getSmallLetterRule());
        rules.add(getDigitRule());

        Exception exception = assertThrows(TokenizerException.class,
                () -> getTokens(rules, input));
        assertTrue(exception.getMessage().contains("Expected"));
        assertTrue(exception.getMessage().contains("but found %s".formatted(input)));
    }

    @ParameterizedTest(name = "testGrammarOrRuleMixedOrAnd1 input: {0}")
    @ValueSource(strings = {"a1b2", "99j9", "j0jj"})
    void testGrammarOrRuleMixedOrAnd1(String input) {
        List<Rule> rules = new ArrayList<>();
        rules.add(new Rule(Grammar.MAIN_RULE,
                new And()
                        .add(new Symbol(SymbolType.NON_TERMINAL, "alphanumeric"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "digit"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "small_letter"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "alphanumeric"))
        ));
        rules.add(getAlphanumericRule());
        rules.add(getSmallLetterRule());
        rules.add(getDigitRule());

        List<String> tokens = getTokens(rules, input);
        char[] inputCharArray = input.toCharArray();

        assertEquals(4, tokens.size());
        assertEquals(inputCharArray[0] + "", tokens.get(0));
        assertEquals(inputCharArray[1] + "", tokens.get(1));
        assertEquals(inputCharArray[2] + "", tokens.get(2));
        assertEquals(inputCharArray[3] + "", tokens.get(3));
    }

    @ParameterizedTest(name = "testGrammarOrRuleMixedOrAnd2 input: {0}")
    @ValueSource(strings = {"a0a0", "j9j9", "a0j9"})
    void testGrammarOrRuleMixedOrAnd2(String input) {
        List<Rule> rules = new ArrayList<>();
        rules.add(new Rule(Grammar.MAIN_RULE,
                new And()
                        .add(new Symbol(SymbolType.NON_TERMINAL, "small_letter_and_digit"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "small_letter_and_digit"))
        ));
        rules.add(getSmallLetterAndDigit());
        rules.add(getAlphanumericRule());
        rules.add(getSmallLetterRule());
        rules.add(getDigitRule());

        List<String> tokens = getTokens(rules, input);
        char[] inputCharArray = input.toCharArray();

        assertEquals(4, tokens.size());
        assertEquals(inputCharArray[0] + "", tokens.get(0));
        assertEquals(inputCharArray[1] + "", tokens.get(1));
        assertEquals(inputCharArray[2] + "", tokens.get(2));
        assertEquals(inputCharArray[3] + "", tokens.get(3));
    }

    @Test
    void testGrammarRepeatRuleWithTerminals() {
        List<Rule> rules = new ArrayList<>();
        rules.add(new Rule(Grammar.MAIN_RULE,
                new Repeat()
                        .add(new Symbol(SymbolType.TERMINAL, "1"))
                        .add(new Symbol(SymbolType.TERMINAL, "2"))
                        .add(new Symbol(SymbolType.TERMINAL, "3"))
        ));
        rules.add(getDigitRule());
        rules.add(getMathOperatorRule());

        List<String> tokens = getTokens(rules, "123123");

        assertEquals(6, tokens.size());
        assertEquals("1", tokens.get(0));
        assertEquals("2", tokens.get(1));
        assertEquals("3", tokens.get(2));
        assertEquals("1", tokens.get(3));
        assertEquals("2", tokens.get(4));
        assertEquals("3", tokens.get(5));
    }

    @Test
    void testExceptionWhenGrammarRepeatRuleWithTerminals() {
        List<Rule> rules = new ArrayList<>();
        rules.add(new Rule(Grammar.MAIN_RULE,
                new Repeat()
                        .add(new Symbol(SymbolType.TERMINAL, "1"))
                        .add(new Symbol(SymbolType.TERMINAL, "2"))
        ));
        rules.add(getDigitRule());
        rules.add(getMathOperatorRule());

        Exception exception = assertThrows(TokenizerException.class,
                () -> getTokens(rules, "1231"));
        assertTrue(exception.getMessage().contains("Expected"));
        assertTrue(exception.getMessage().contains("but found 3"));
    }

    @ParameterizedTest(name = "testGrammarRepeatRuleWithNonTerminalOnly input: {0}")
    @ValueSource(strings = {"1234"})
    void testGrammarRepeatRuleWithTerminalOnly(String input) {
        List<Rule> rules = new ArrayList<>();
        rules.add(new Rule(Grammar.MAIN_RULE,
                new Repeat()
                        .add(new Symbol(SymbolType.NON_TERMINAL, "digit"))
        ));
        rules.add(getDigitRule());
        rules.add(getMathOperatorRule());

        List<String> tokens = getTokens(rules, input);
        char[] inputCharArray = input.toCharArray();

        assertEquals(input.length(), tokens.size());
        assertEquals(inputCharArray[0] + "", tokens.get(0));
        assertEquals(inputCharArray[1] + "", tokens.get(1));
        assertEquals(inputCharArray[2] + "", tokens.get(2));
        assertEquals(inputCharArray[3] + "", tokens.get(3));
    }

    @ParameterizedTest(name = "testGrammarRepeatRuleWithOrNonTerminals input: {0}")
    @ValueSource(strings = {"a", "0", "aaaa", "0000", "abcd", "01234", "efdea", "736327"})
    void testGrammarRepeatRuleWithOrNonTerminals(String input) {
        List<Rule> rules = new ArrayList<>();

        rules.add(new Rule(Grammar.MAIN_RULE,
                new Or()
                        .add(new Symbol(SymbolType.NON_TERMINAL, "repeat_digit"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "repeat_small_letter"))
        ));

        rules.add(new Rule("repeat_digit", new Repeat()
                .add(new Symbol(SymbolType.NON_TERMINAL, "digit"))));
        rules.add(new Rule("repeat_small_letter", new Repeat()
                .add(new Symbol(SymbolType.NON_TERMINAL, "small_letter"))));
        rules.add(getSmallLetterRule());
        rules.add(getDigitRule());

        List<String> tokens = getTokens(rules, input);
        char[] inputCharArray = input.toCharArray();

        assertEquals(input.length(), tokens.size());
        for (int i = 0; i < inputCharArray.length; i++) {
            assertEquals(inputCharArray[i] + "", tokens.get(i));
        }
    }

    @ParameterizedTest(name = "testGrammarRepeatRuleWithAndNonTerminals input: {0}")
    @ValueSource(strings = {"0+0+0", "1+1+1", "9+9+9", "123+444+678", "0123456789+1+1", "0123456789+0123456789+0123456789"})
    void testGrammarRepeatRuleWithAndNonTerminals(String input) {
        List<Rule> rules = new ArrayList<>();

        rules.add(new Rule(Grammar.MAIN_RULE,
                new And()
                        .add(new Symbol(SymbolType.NON_TERMINAL, "repeat_digit"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "math_operator"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "repeat_digit"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "math_operator"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "repeat_digit"))
        ));

        rules.add(new Rule("repeat_digit", new Repeat()
                .add(new Symbol(SymbolType.NON_TERMINAL, "digit"))));
        rules.add(getDigitRule());
        rules.add(getMathOperatorRule());

        List<String> tokens = getTokens(rules, input);
        char[] inputCharArray = input.toCharArray();

        assertEquals(input.length(), tokens.size());
        for (int i = 0; i < inputCharArray.length; i++) {
            assertEquals(inputCharArray[i] + "", tokens.get(i));
        }
    }

    private List<String> getTokens(List<Rule> rules, String input) {
        Grammar grammar = new Grammar(rules);
        Tokenizer tokenizer = new Tokenizer(grammar, input);
        return tokenizer.tokenizeInput();
    }

    private Rule getSmallLetterAndDigit() {
        return new Rule("small_letter_and_digit",
                new And()
                        .add(new Symbol(SymbolType.NON_TERMINAL, "small_letter"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "digit"))
        );
    }

    private Rule getAlphanumericRule() {
        return new Rule("alphanumeric",
                new Or()
                        .add(new Symbol(SymbolType.NON_TERMINAL, "small_letter"))
                        .add(new Symbol(SymbolType.NON_TERMINAL, "digit"))
        );
    }

    private Rule getSmallLetterRule() {
        return new Rule("small_letter",
                new Or()
                        .add(new Symbol(SymbolType.TERMINAL, "a"))
                        .add(new Symbol(SymbolType.TERMINAL, "b"))
                        .add(new Symbol(SymbolType.TERMINAL, "c"))
                        .add(new Symbol(SymbolType.TERMINAL, "d"))
                        .add(new Symbol(SymbolType.TERMINAL, "e"))
                        .add(new Symbol(SymbolType.TERMINAL, "f"))
                        .add(new Symbol(SymbolType.TERMINAL, "g"))
                        .add(new Symbol(SymbolType.TERMINAL, "h"))
                        .add(new Symbol(SymbolType.TERMINAL, "i"))
                        .add(new Symbol(SymbolType.TERMINAL, "j"))
        );
    }

    private Rule getDigitRule() {
        return new Rule("digit",
                new Or()
                        .add(new Symbol(SymbolType.TERMINAL, "0"))
                        .add(new Symbol(SymbolType.TERMINAL, "1"))
                        .add(new Symbol(SymbolType.TERMINAL, "2"))
                        .add(new Symbol(SymbolType.TERMINAL, "3"))
                        .add(new Symbol(SymbolType.TERMINAL, "4"))
                        .add(new Symbol(SymbolType.TERMINAL, "5"))
                        .add(new Symbol(SymbolType.TERMINAL, "6"))
                        .add(new Symbol(SymbolType.TERMINAL, "7"))
                        .add(new Symbol(SymbolType.TERMINAL, "8"))
                        .add(new Symbol(SymbolType.TERMINAL, "9"))
        );
    }

    private Rule getMathOperatorRule() {
        return new Rule("math_operator",
                new Or()
                        .add(new Symbol(SymbolType.TERMINAL, "+"))
                        .add(new Symbol(SymbolType.TERMINAL, "-"))
                        .add(new Symbol(SymbolType.TERMINAL, "*"))
                        .add(new Symbol(SymbolType.TERMINAL, "/"))
                        .add(new Symbol(SymbolType.TERMINAL, "^"))
        );
    }
}
