package interpreter.grammar;

import interpreter.grammar.tokenizer.Rule;
import java.util.List;

public class Grammar {
    public static final String MAIN_RULE = "program";
    public List<Rule> rules;

    public Grammar(List<Rule> rules) {
        this.rules = rules;
    }

    public Rule getMainRule() {
        return getRule(MAIN_RULE);
    }

    public Rule getRule(String name) {
        return rules.stream()
                .filter(rule -> rule.getName().equals(name))
                .findFirst()
                .orElseThrow(() ->
                        new RuntimeException("Rule '%s' not found in grammar".formatted(name)));
    }
}
