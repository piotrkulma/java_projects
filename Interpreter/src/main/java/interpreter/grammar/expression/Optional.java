package interpreter.grammar.expression;

public class Optional extends Expression {
    public Optional() {
        super();
    }

    @Override
    public String getName() {
        return "optional";
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("[ ");
        symbols.forEach(symbol -> {
            builder.append(symbol);
        });
        builder.append(" ]");

        return builder.toString();
    }
}
