package interpreter.grammar.expression;

import interpreter.grammar.tokenizer.Symbol;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class Expression {
    protected List<Symbol> symbols;

    public Expression() {
        symbols = new ArrayList<>();
    }

    public abstract String getName();

    public Expression add(Symbol symbol) {
        this.symbols.add(symbol);
        return this;
    }

    public Iterator<Symbol> getIterator() {
        return symbols.stream().iterator();
    }
}
