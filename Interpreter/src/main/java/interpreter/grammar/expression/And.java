package interpreter.grammar.expression;

public class And extends Expression {
    public And() {
        super();
    }

    @Override
    public String getName() {
        return "and";
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        symbols.forEach(symbol -> {
            builder.append(symbol + " , ");
        });

        return builder.toString();
    }
}
