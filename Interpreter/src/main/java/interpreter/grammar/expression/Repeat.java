package interpreter.grammar.expression;

public class Repeat extends Expression {
    public Repeat() {
        super();
    }

    @Override
    public String getName() {
        return "repeat";
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("{ ");
        symbols.forEach(symbol -> {
            builder.append(symbol);
        });
        builder.append(" }");

        return builder.toString();
    }
}
