package interpreter.grammar.expression;

public class Or extends Expression {
    public Or() {
        super();
    }

    @Override
    public String getName() {
        return "or";
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        symbols.forEach(symbol -> {
            builder.append(symbol + " | ");
        });

        return builder.toString();
    }
}
