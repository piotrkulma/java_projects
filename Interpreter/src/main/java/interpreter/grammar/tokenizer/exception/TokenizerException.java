package interpreter.grammar.tokenizer.exception;

public class TokenizerException extends RuntimeException {
    public TokenizerException(String message) {
        super(message);
    }
}
