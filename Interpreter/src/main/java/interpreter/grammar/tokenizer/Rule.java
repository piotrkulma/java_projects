package interpreter.grammar.tokenizer;

import interpreter.grammar.expression.Expression;

public class Rule {
    private String name;
    private Expression expression;

    public Rule(String name, Expression expression) {
        this.name = name;
        this.expression = expression;
    }

    public String getName() {
        return name;
    }

    public Expression getExpression() {
        return expression;
    }

    @Override
    public String toString() {
        return "<%s>:=%s".formatted(name, expression);
    }
}
