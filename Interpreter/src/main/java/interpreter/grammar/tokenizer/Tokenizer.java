package interpreter.grammar.tokenizer;

import interpreter.grammar.Grammar;
import interpreter.grammar.tokenizer.exception.TokenizerException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Tokenizer {
    private Integer inputCharIndex;
    private String actualChar;
    private String input;
    private Grammar grammar;
    private List<String> tokens;

    private static String EXCEPT_MESS_EXPECTED_BUT_FOUND = "Error at column %d. Expected %s but found %s.";

    private enum TokenizeRuleReturn {
        TRUE,
        FALSE,
        END;
    }

    public Tokenizer(Grammar grammar, String input) {
        this.input = input;
        this.grammar = grammar;
        this.tokens = new ArrayList<>();
    }

    public List<String> tokenizeInput() {
        inputCharIndex = 0;
        Rule mainRule = grammar.getMainRule();
        actualChar = input.charAt(inputCharIndex) + "";
        tokenizeRule(mainRule, null, false);

        return tokens;
    }

    public TokenizeRuleReturn tokenizeRule(Rule rule, Rule higherRule, boolean isNextSymbol) {
        boolean repeat = false;
        boolean active = true;
        Symbol actualSymbol = null;
        Iterator<Symbol> symbolIterator = rule.getExpression().getIterator();

        while (active) {
            if (symbolIterator.hasNext() && inputCharIndex <= input.length() - 1) {
                actualSymbol = symbolIterator.next();
                setActualChar(actualSymbol);
            } else {
                if(repeat && inputCharIndex <= input.length() - 1) {
                    symbolIterator = rule.getExpression().getIterator();
                    actualSymbol = symbolIterator.next();
                    setActualChar(actualSymbol);
                } else {
                    active = false;
                    continue;
                }
            }

            if (isAnd(rule)) {
                if (isTerminal(actualSymbol)) {
                    if (actualSymbol.getValue().equals(actualChar)) {
                        tokens.add(actualChar);
                        inputCharIndex+=actualSymbol.getValue().length();
                    } else {
                        throw new TokenizerException(
                                EXCEPT_MESS_EXPECTED_BUT_FOUND
                                        .formatted(inputCharIndex, higherRule != null ? higherRule : rule, actualChar));
                    }
                } else {
                    Rule nonTerminal = grammar.getRule(actualSymbol.getValue());
                    tokenizeRule(nonTerminal, rule, symbolIterator.hasNext());
                }
            } else if (isOr(rule)) {
                if (isTerminal(actualSymbol)) {
                    if (actualSymbol.getValue().equals(actualChar)) {
                        tokens.add(actualChar);
                        inputCharIndex+=actualSymbol.getValue().length();

                        if (higherRule != null) {
                            return TokenizeRuleReturn.TRUE;
                        }
                        break;

                    } else {
                        if (symbolIterator.hasNext()) {
                            continue;
                        } else {
                            if (higherRule != null && isOr(higherRule) && isNextSymbol) {
                                return TokenizeRuleReturn.FALSE;
                            }

                            if (higherRule != null && isRepeat(higherRule)) {
                                return TokenizeRuleReturn.FALSE;
                            }
                            throw new TokenizerException(
                                    EXCEPT_MESS_EXPECTED_BUT_FOUND
                                            .formatted(inputCharIndex, higherRule != null ? higherRule : rule, actualChar));
                        }
                    }
                } else {
                    Rule nonTerminal = grammar.getRule(actualSymbol.getValue());
                    TokenizeRuleReturn isFulfilled = tokenizeRule(nonTerminal, rule, symbolIterator.hasNext());

                    if (isFulfilled == TokenizeRuleReturn.TRUE) {
                        return TokenizeRuleReturn.END;
                    }
                }
            } else if(isRepeat(rule)) {
                if(isTerminal(actualSymbol)) {
                    if (actualSymbol.getValue().equals(actualChar)) {
                        tokens.add(actualChar);
                        inputCharIndex+=actualSymbol.getValue().length();

                        repeat = true;
                    } else {
                        throw new TokenizerException(
                                EXCEPT_MESS_EXPECTED_BUT_FOUND
                                        .formatted(inputCharIndex, higherRule != null ? higherRule : rule, actualChar));
                    }
                } else {
                    Rule nonTerminal = grammar.getRule(actualSymbol.getValue());
                    TokenizeRuleReturn tokenizeRuleReturn = tokenizeRule(nonTerminal, rule, symbolIterator.hasNext());

                    repeat = tokenizeRuleReturn == TokenizeRuleReturn.TRUE;
                }
            }
        }

        if(symbolIterator.hasNext() && !isOr(rule) && inputCharIndex > input.length() - 1) {
            throw new TokenizerException("Error at column %d. Input '%s' ends before expression was fulfilled '%s'".formatted(inputCharIndex, input, rule));
        }

        return TokenizeRuleReturn.END;
    }

    private void setActualChar(Symbol symbol) {
        if (isTerminal(symbol)) {
            StringBuilder builder = new StringBuilder();
            int symbolLen = symbol.getValue().length();

            for (int i = 0; i < symbolLen; i++) {
                builder.append(input.charAt(inputCharIndex + i));
            }
            this.actualChar = builder.toString();
        }
    }

    private boolean isTerminal(Symbol symbol) {
        return symbol.getSymbolType() == SymbolType.TERMINAL;
    }

    private boolean isRepeat(Rule rule) {
        return rule.getExpression().getName().equals("repeat");
    }

    private boolean isOr(Rule rule) {
        return rule.getExpression().getName().equals("or");
    }

    private boolean isAnd(Rule rule) {
        return rule.getExpression().getName().equals("and");
    }
}
