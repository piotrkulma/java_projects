package interpreter.grammar.tokenizer;

public class Symbol {
    private SymbolType symbolType;
    private String value;

    public Symbol(SymbolType symbolType, String value) {
        this.symbolType = symbolType;
        this.value = value;
    }

    public SymbolType getSymbolType() {
        return symbolType;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        if(symbolType == SymbolType.TERMINAL) {
            return "'%s'".formatted(this.value);
        } else {
            return "<%s>".formatted(this.value);
        }
    }
}
