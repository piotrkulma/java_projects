package pl.piotrkulma.genalg;

import lombok.Data;

import java.util.Arrays;

@Data
public class DNA {
    private float fitness;
    private char[] genes;

    public DNA() {
    }

    public DNA(char[] genes) {
        this.genes = genes;
    }

    @Override
    public String toString() {
        return "DNA{" +
                "fitness=" + fitness +
                ", genes=" + Arrays.toString(genes) +
                '}';
    }
}
