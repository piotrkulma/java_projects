package pl.piotrkulma.genalg;

import pl.piotrkulma.genalg.util.FitnessUtil;
import pl.piotrkulma.genalg.util.GeneticsUtil;
import pl.piotrkulma.genalg.util.RandomUtil;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class Run {
    private static final int GENE_LENGTH = 18;
    private static final int POPULATION_LENGTH = 100;

    private static final float MUTATION_RATE = 0.01f;

    private static final String TARGET = "to be or not to be";

    private DNA[] population;

    public static void main(String[] args) {
        Run app = new Run();
        app.setup();
        app.draw();

        for (int i = 0; i < 100000; i++) {
            System.out.println(String.format("GENERATION %d", i + 1));
            ArrayList<DNA> matingPool = app.selection();
            app.reproduction(matingPool);
            app.draw();
            app.findBest();
        }
    }

    public void findBest() {
        Optional<DNA> best = Stream.of(population).max(Comparator.comparing(DNA::getFitness));
        best.ifPresent(dna -> {
            System.out.println(dna);
        });
    }

    public void setup() {
        population = new DNA[POPULATION_LENGTH];

        for (int i = 0; i < POPULATION_LENGTH; i++) {
            population[i] = new DNA(RandomUtil.randomGenes(GENE_LENGTH));
        }
    }

    public void draw() {
        for (int i = 0; i < POPULATION_LENGTH; i++) {
            population[i].setFitness(FitnessUtil.fitness(population[i].getGenes(), TARGET.toCharArray()));
        }
    }

    public ArrayList<DNA> selection() {
        ArrayList<DNA> matingPool = new ArrayList<>();
        for (int i = 0; i < POPULATION_LENGTH; i++) {
            int n = (int) (population[i].getFitness() * 100);

            for (int j = 0; j < n; j++) {
                matingPool.add(population[i]);
            }
        }

        return matingPool;
    }

    public void reproduction(ArrayList<DNA> matingPool) {
        for (int i = 0; i < population.length; i++) {
            int a = RandomUtil.random(0, matingPool.size() - 1);
            int b = RandomUtil.random(0, matingPool.size() - 1);

            DNA partnerA = matingPool.get(a);
            DNA partnerB = matingPool.get(b);

            DNA child = GeneticsUtil.crossover(partnerA, partnerB, GENE_LENGTH);
            GeneticsUtil.mutate(child, MUTATION_RATE);

            population[i] = child;
        }
    }
}
