package pl.piotrkulma.genalg.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class RandomUtil {
    public int random(int begin, int end) {
        int range = (end - begin) + 1;
        return (int) (Math.random() * range) + begin;
    }

    public char randomASCII() {
        return (char) random(32, 128);
    }

    public char[] randomGenes(int length) {
        char newGene[] = new char[length];
        for (int i = 0; i < length; i++) {
            newGene[i] = randomASCII();
        }

        return newGene;
    }
}
