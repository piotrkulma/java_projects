package pl.piotrkulma.genalg.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class FitnessUtil {
    public float fitness(char[] genes, char[] target) {
        float score = 0;

        for(int i=0; i<genes.length; i++) {
            if(genes[i] == target[i]) {
                score ++;
            }
        }

        return score / target.length;
    }
}
