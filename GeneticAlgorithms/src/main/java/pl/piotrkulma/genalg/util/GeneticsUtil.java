package pl.piotrkulma.genalg.util;

import lombok.experimental.UtilityClass;
import pl.piotrkulma.genalg.DNA;

@UtilityClass
public class GeneticsUtil {
    public DNA crossover(DNA a, DNA b, int geneLength) {
        DNA child = new DNA(RandomUtil.randomGenes(geneLength));

        int midpoint = RandomUtil.random(0, geneLength - 1);

        for (int i = 0; i < geneLength; i++) {
            if (i > midpoint) {
                child.getGenes()[i] = a.getGenes()[i];
            } else {
                child.getGenes()[i] = b.getGenes()[i];
            }
        }

        return child;
    }

    public void mutate(DNA a, float mutationRate) {
        for (int i = 0; i < a.getGenes().length; i++) {
            if(Math.random() < mutationRate) {
                a.getGenes()[i] = RandomUtil.randomASCII();
            }
        }
    }
}
