package pl.piotrkulma.ebnf;

import org.junit.jupiter.api.Test;
import pl.piotrkulma.ebnf.analyzer.LexicalAnalyzer;

class GrammarLexerTest {

    @Test
    void getTokenStream() {
/*        String grammar0 = "grammar2                       = B,_,[1|2],_,(E | e | end | x | y, z);";
        //String grammar0 = "grammar0                       = \"main()\",_,\"{\",_,(\"if(true)\",_,\"{\",_,\"}\" | a),_,\"}\";";

        String program0 = "program1                       = B,_,2,_,E;";

        LexicalAnalyzer lexicalAnalyzer = LexicalAnalyzer.getInstance(grammar0, program0);
        lexicalAnalyzer.analyze();
        System.out.println();*/
    }

    @Test
    void getFromFile() {
        LexicalAnalyzer lexicalAnalyzer = LexicalAnalyzer.getInstance(
                "/grammars/basic_grammar.txt", "/grammars/basic_grammar_program.txt");
        lexicalAnalyzer.analyze();
    }
}
