package pl.piotrkulma.ebnf.tokenstream;

public interface Validator {
    void validate(final TokenStreamLine tokenStreamLine);
}
