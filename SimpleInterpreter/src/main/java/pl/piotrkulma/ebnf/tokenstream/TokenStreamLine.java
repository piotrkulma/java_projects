package pl.piotrkulma.ebnf.tokenstream;

import pl.piotrkulma.ebnf.Token;

import java.util.ArrayList;
import java.util.List;

public class TokenStreamLine {
    private int actualIndex = 0;
    protected final List<Token> tokens;

    private TokenStreamLine() {
        this.tokens = new ArrayList<>();
    }

    public boolean hasMoreTokens() {
        return actualIndex < tokens.size();
    }

    public Token nextToken() {
        return this.tokens.get(this.actualIndex++).copy();
    }

    public static final class TokenStreamLineBuilder {
        private final TokenStreamLine tokenStreamLine;

        private Validator validator;

        public static TokenStreamLine.TokenStreamLineBuilder getInstance() {
            return new TokenStreamLine.TokenStreamLineBuilder();
        }

        public TokenStreamLine.TokenStreamLineBuilder append(final Token token) {
            this.tokenStreamLine.tokens.add(token);
            return this;
        }

        public TokenStreamLine.TokenStreamLineBuilder addValidator(final Validator validator) {
            this.validator = validator;
            return this;
        }

        public TokenStreamLine build() {
            validator.validate(this.tokenStreamLine);
            return tokenStreamLine;
        }

        private TokenStreamLineBuilder() {
            tokenStreamLine = new TokenStreamLine();
        }
    }
}
