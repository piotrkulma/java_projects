package pl.piotrkulma.ebnf.tokenstream;

import pl.piotrkulma.ebnf.TokenType;
import pl.piotrkulma.ebnf.exceptions.GrammarException;

public class TokenStreamValidator implements Validator {
    public void validate(final TokenStreamLine tokenStreamLine) {
        TokenStreamValidator validator = new TokenStreamValidator();
        validator.validateTokensListSize(tokenStreamLine);
        validator.validateDefinition(tokenStreamLine);
        validator.validateTerminationToken(tokenStreamLine);
    }

    public void validateTokensListSize(final TokenStreamLine tokenStreamLine) {
        if (tokenStreamLine.tokens.size() == 0) {
            throw new GrammarException("No tokens found");
        }
    }

    public void validateDefinition(final TokenStreamLine tokenStreamLine) {
        long defCount = tokenStreamLine.tokens.stream()
                .filter(t -> t.getType() == TokenType.OPERATION_DEF)
                .count();
        if (defCount == 0) {
            throw new GrammarException("No definition token found");
        } else if (defCount != 1) {
            throw new GrammarException("Only one definition token is required");
        }

        if (tokenStreamLine.tokens.size() < 3) {
            throw new GrammarException("Not enough tokens");
        }

        if (tokenStreamLine.tokens.get(0).getType() != TokenType.IDENTIFIER ||
                tokenStreamLine.tokens.get(1).getType() != TokenType.OPERATION_DEF) {
            throw new GrammarException("Incorrect identifier definition");
        }
    }

    public void validateTerminationToken(final TokenStreamLine tokenStreamLine) {
        long termCount = tokenStreamLine.tokens
                .stream()
                .filter(t -> t.getType() == TokenType.OPERATION_TERM).count();

        if (termCount == 0) {
            throw new GrammarException("No termination token (one is required)");
        }

        if (termCount != 1) {
            throw new GrammarException("Only one termination token is required");
        }

        if (tokenStreamLine.tokens.get(tokenStreamLine.tokens.size() - 1).getType() != TokenType.OPERATION_TERM) {
            throw new GrammarException("Termination token at the end is required");
        }
    }
}
