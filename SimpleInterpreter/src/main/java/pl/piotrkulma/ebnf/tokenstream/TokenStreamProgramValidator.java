package pl.piotrkulma.ebnf.tokenstream;

public class TokenStreamProgramValidator extends TokenStreamValidator {
    public void validate(final TokenStreamLine tokenStreamLine) {
        TokenStreamValidator validator = new TokenStreamValidator();
        validator.validateTokensListSize(tokenStreamLine);
        validator.validateDefinition(tokenStreamLine);
    }
}
