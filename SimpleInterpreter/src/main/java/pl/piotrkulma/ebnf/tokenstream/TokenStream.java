package pl.piotrkulma.ebnf.tokenstream;

import java.util.ArrayList;
import java.util.List;

public class TokenStream {
    private List<TokenStreamLine> tokenList;

    public List<TokenStreamLine> getTokenList() {
        return tokenList;
    }

    public static final class TokenStreamBuilder {
        private List<TokenStreamLine> tokenList;

        private TokenStreamBuilder() {
            this.tokenList = new ArrayList<>();
        }

        public static TokenStreamBuilder getInstance() {
            return new TokenStreamBuilder();
        }

        public TokenStreamBuilder appendLine(final TokenStreamLine tokenStreamLine) {
            this.tokenList.add(tokenStreamLine);
            return this;
        }

        public TokenStream build() {
            final TokenStream tokenStream = new TokenStream();
            tokenStream.tokenList = this.tokenList;
            return tokenStream;
        }
    }
}
