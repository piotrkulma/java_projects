package pl.piotrkulma.ebnf.parser;

import pl.piotrkulma.ebnf.Token;

public class ParsedTree {
    private String identifier;
    private TreeNode<Token> tree;

    public ParsedTree(final String identifier, final TreeNode<Token> tree) {
        this.identifier = identifier;
        this.tree = tree;
    }

    public String getIdentifier() {
        return identifier;
    }

    public TreeNode<Token> getTree() {
        return tree;
    }
}
