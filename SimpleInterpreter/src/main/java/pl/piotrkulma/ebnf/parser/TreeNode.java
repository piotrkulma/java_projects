package pl.piotrkulma.ebnf.parser;

import java.util.ArrayList;
import java.util.List;

public class TreeNode<T> {
    private T data = null;
    private TreeNode<T> parent = null;
    private List<TreeNode<T>> children = null;

    public T getData() {
        return data;
    }

    public void setData(final T data) {
        this.data = data;
    }

    public TreeNode<T> getParent() {
        return parent;
    }

    public void setParent(final TreeNode<T> parent) {
        this.parent = parent;
    }

    public List<TreeNode<T>> getChildren() {
        return children;
    }

    public void setChildren(final List<TreeNode<T>> children) {
        this.children = children;
    }

    public void appendChild(final TreeNode<T> node) {
        if(this.children == null) {
            this.children = new ArrayList<>();
        }
        this.children.add(node);
    }

    @Override
    public String toString() {
        return "TreeNode{" +
                "data=" + data +
                ", parent=" + parent +
                ", children=" + children +
                '}';
    }
}
