package pl.piotrkulma.ebnf.parser;

import pl.piotrkulma.ebnf.TokenType;
import pl.piotrkulma.ebnf.exceptions.GrammarException;

public final class ParserUtils {
    public static boolean isIdentifier(final TokenType type) {
        return type == TokenType.IDENTIFIER;
    }

    public static boolean isOmitted(final TokenType type) {
        return type == TokenType.OPERATION_DEF ||
                type == TokenType.OPERATION_TERM;
    }

    public static boolean isTerminal(final TokenType type) {
        return type == TokenType.TERMINAL;
    }

    public static boolean isNonTerminal(final TokenType type) {
        return type == TokenType.NON_TERMINAL;
    }

    public static int getOperationOrder(final TokenType type) {
        switch (type) {
            case OPERATION_CONCAT:
                return 1;
            case OPERATION_ALTER:
                return 2;
            default:
                throw new GrammarException(
                        String.format("Token '%s' is not operator token", type.name()));
        }
    }

    public static boolean isOperator(final TokenType type) {
        return type == TokenType.OPERATION_DEF ||
                type == TokenType.OPERATION_CONCAT ||
                type == TokenType.OPERATION_TERM ||
                type == TokenType.OPERATION_ALTER;
    }

    public static boolean isGroupOpen(final TokenType type) {
        return type == TokenType.OPERATION_GROUP_BEGIN;
    }

    public static boolean isGroupClose(final TokenType type) {
        return type == TokenType.OPERATION_GROUP_END;
    }

    public static boolean isOptionalOpen(TokenType type) {
        return type == TokenType.OPERATION_OPT_BEGIN;
    }

    public static boolean isOptionalClose(TokenType type) {
        return type == TokenType.OPERATION_OPT_END;
    }

    public static boolean isRepetitionOpen(TokenType type) {
        return type == TokenType.OPERATION_REP_BEGIN;
    }

    public static boolean isRepetitionClose(TokenType type) {
        return type == TokenType.OPERATION_REP_END;
    }

    private ParserUtils() {
    }
}
