package pl.piotrkulma.ebnf.parser;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import pl.piotrkulma.ebnf.Token;
import pl.piotrkulma.ebnf.exceptions.ParserException;
import pl.piotrkulma.ebnf.tokenstream.TokenStream;
import pl.piotrkulma.ebnf.tokenstream.TokenStreamLine;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

import static pl.piotrkulma.ebnf.parser.ParserUtils.*;

public class GrammarParser {
    private static final Logger LOG = LogManager.getLogger(GrammarParser.class);

    private final TokenStream tokenStream;

    public GrammarParser(TokenStream tokenStream) {
        this.tokenStream = tokenStream;
    }

    public List<ParsedTree> createTrees() {
        final List<ParsedTree> parsedTrees = new ArrayList<>();

        for (TokenStreamLine tokenStreamLine : tokenStream.getTokenList()) {
            parsedTrees.add(createTree(tokenStreamLine));
        }

        return parsedTrees;
    }

    private ParsedTree createTree(final TokenStreamLine tokenStreamLine) {
        LOG.info("Parsing line");
        final String identifier = tokenStreamLine.nextToken().getValue();

        final List<TreeNode<Token>> rpnOutput =
                createRpnOutput(tokenStreamLine).stream()
                        .map(this::createNode)
                        .collect(Collectors.toList());

        final Stack<TreeNode<Token>> tokenStack = new Stack<>();

        for (TreeNode<Token> token : rpnOutput) {
            LOG.info(token);

            if (isOperator(token.getData().getType())) {
                TreeNode<Token> treeNodeOne = tokenStack.pop();
                treeNodeOne.setParent(token);
                TreeNode<Token> treeNodeTwo = tokenStack.pop();
                treeNodeTwo.setParent(token);

                token.appendChild(treeNodeOne);
                token.appendChild(treeNodeTwo);
            }
            tokenStack.push(token);
        }

        if (tokenStack.size() != 1) {
            throw new ParserException(
                    String.format("Stack size should be equals to one at the end not %d", tokenStack.size()));
        }

        if(identifier == null) {
            throw new ParserException(String.format("Identifier not found"));
        }

        return new ParsedTree(identifier, tokenStack.pop());
    }

    private List<Token> createRpnOutput(final TokenStreamLine tokenStreamLine) {
        final Stack<Token> stack = new Stack<>();
        final List<Token> output = new ArrayList<>();

        while (tokenStreamLine.hasMoreTokens()) {
            final Token token = tokenStreamLine.nextToken();

            if (isOmitted(token.getType()) || isIdentifier(token.getType())) {
                continue;
            } else if (isTerminal(token.getType()) || isNonTerminal(token.getType())) {
                output.add(token);
            } else if (isOptionalOpen(token.getType())) {
                stack.push(token);
            } else if (isOptionalClose(token.getType())) {
                while (!stack.empty() && !isOptionalOpen(stack.peek().getType())) {
                    output.add(stack.pop());
                }
                stack.pop();
            } else if (isRepetitionOpen(token.getType())) {
                stack.push(token);
            } else if (isRepetitionClose(token.getType())) {
                while (!stack.empty() && !isRepetitionOpen(stack.peek().getType())) {
                    output.add(stack.pop());
                }
                stack.pop();
            } else if (isGroupOpen(token.getType())) {
                stack.push(token);
            } else if (isGroupClose(token.getType())) {
                while (!stack.empty() && !isGroupOpen(stack.peek().getType())) {
                    output.add(stack.pop());
                }
                //removing close bracket
                stack.pop();
            } else if (isOperator(token.getType())) {
                while (!stack.empty() &&
                        isOperator(stack.peek().getType()) &&
                        getOperationOrder(stack.peek().getType()) <= getOperationOrder(token.getType())) {
                    output.add(stack.pop());
                }
                stack.push(token);
            }
        }

        if (!stack.empty()) {
            while (!stack.empty()) {
                output.add(stack.pop());
            }
        }

        return output;
    }

    private TreeNode<Token> createNode(final Token token) {
        final TreeNode<Token> tokenTreeNode = new TreeNode<>();
        tokenTreeNode.setData(token);
        return tokenTreeNode;
    }
}
