package pl.piotrkulma.ebnf.exceptions;

public class LexerException extends RuntimeException {
    public LexerException(String message) {
        super(message);
    }
}
