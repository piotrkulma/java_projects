package pl.piotrkulma.ebnf.exceptions;

public class TokenStreamException extends RuntimeException {

    public TokenStreamException(String message) {
        super(message);
    }
}
