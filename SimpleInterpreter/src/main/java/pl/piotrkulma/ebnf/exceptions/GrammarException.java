package pl.piotrkulma.ebnf.exceptions;

public class GrammarException extends RuntimeException {
    public GrammarException(String message) {
        super(message);
    }
}
