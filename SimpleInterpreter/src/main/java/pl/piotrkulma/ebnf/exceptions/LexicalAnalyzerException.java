package pl.piotrkulma.ebnf.exceptions;

public class LexicalAnalyzerException extends RuntimeException {

    public LexicalAnalyzerException(String message) {
        super(message);
    }
}
