package pl.piotrkulma.ebnf.exceptions;

public class ParserException extends RuntimeException {
    public ParserException(String message) {
        super(message);
    }
}
