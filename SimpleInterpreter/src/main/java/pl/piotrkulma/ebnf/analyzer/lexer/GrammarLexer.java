package pl.piotrkulma.ebnf.analyzer.lexer;

import pl.piotrkulma.ebnf.tokenstream.TokenStreamValidator;
import pl.piotrkulma.ebnf.tokenstream.Validator;

public class GrammarLexer extends Lexer {
    private final StringBuilder partialLine;

    public GrammarLexer() {
        super();
        this.partialLine = new StringBuilder();
    }

    @Override
    public void append(final String line) {
        partialLine.append(line + " ");

        if (line.endsWith(OP_TERMINATION)) {
            rules.add(partialLine.toString());
            partialLine.setLength(0);
        }
    }

    @Override
    public void build() {
        this.built = true;
    }

    @Override
    public Validator getValidator() {
        return new TokenStreamValidator();
    }
}
