package pl.piotrkulma.ebnf.analyzer.lexer;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import pl.piotrkulma.ebnf.Token;
import pl.piotrkulma.ebnf.TokenType;
import pl.piotrkulma.ebnf.exceptions.LexerException;
import pl.piotrkulma.ebnf.tokenstream.TokenStream;
import pl.piotrkulma.ebnf.tokenstream.TokenStreamLine;
import pl.piotrkulma.ebnf.tokenstream.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class Lexer {
    private static final Logger LOG = LogManager.getLogger(GrammarLexer.class);

    public final static String OP_DEFINITION = "=";
    public final static String OP_CONCATENATION = ",";
    public final static String OP_TERMINATION = ";";
    public final static String OP_ALTERNATION = "|";
    public final static String OP_OPTIONAL_BEGIN = "[";
    public final static String OP_OPTIONAL_END = "]";
    public final static String OP_REPETITION_BEGIN = "{";
    public final static String OP_REPETITION_END = "}";
    public final static String OP_GROUPING_BEGIN = "(";
    public final static String OP_GROUPING_END = ")";

    public final static String TERMINAL_QUOTATION = "\"";

    public static final String WHITESPACES_REGEX = "\\s+";

    private final static String[] OPERATIONS = new String[]{
            OP_DEFINITION,
            OP_CONCATENATION,
            OP_TERMINATION,
            OP_ALTERNATION,
            OP_OPTIONAL_BEGIN,
            OP_OPTIONAL_END,
            OP_REPETITION_BEGIN,
            OP_REPETITION_END,
            OP_GROUPING_BEGIN,
            OP_GROUPING_END
    };

    protected final List<String> rules;

    private int actualIndex;
    private String actualContent;
    protected boolean built;

    public Lexer() {
        built = false;
        rules = new ArrayList<>();
    }

    public abstract void append(final String line);

    public abstract void build();

    public abstract Validator getValidator();

    public TokenStream getTokenStreams() {
        if (!built) {
            throw new LexerException("Lexer is not completed (not built)");
        }

        if (rules == null || rules.size() == 0) {
            throw new LexerException("Rule cannot be empty");
        }

        TokenStream.TokenStreamBuilder tokenStreamBuilder = TokenStream.TokenStreamBuilder.getInstance();
        for (String line : rules) {
            tokenStreamBuilder.appendLine(getTokenStreamLine(line));
        }

        return tokenStreamBuilder.build();
    }

    public TokenStreamLine getTokenStreamLine(final String content) {
        if (!built) {
            throw new LexerException("Lexer is not completed (not built)");
        }

        LOG.info(String.format("Received content: '%s'", content));

        String rule[] = content.split(OP_DEFINITION);

        if (rule.length != 2) {
            throw new LexerException("Rule is not correct it should form 'rule=definition'");
        }

        this.actualIndex = 0;
        this.actualContent = rule[0].trim() + OP_DEFINITION + rule[1].trim();

        final TokenStreamLine.TokenStreamLineBuilder builder =
                TokenStreamLine.TokenStreamLineBuilder.getInstance();


        String temp;
        while (this.actualIndex < this.actualContent.length()) {
            temp = getActualString();
            if (isOperation(temp)) {
                TokenType type = getOperationType(temp);
                builder.append(Token.getInstance(temp, type));
                this.actualIndex++;
                if (type != TokenType.OPERATION_DEF) {
                    appendConcatenation(builder);
                }
            } else if (isTerminalQuotation(temp)) {
                final String terminal = buildTerminal(this.actualContent, this.actualIndex);
                builder.append(Token.getInstance(terminal, TokenType.TERMINAL));
                appendConcatenation(builder);
            } else if (isNonTerminal(temp)) {
                final TokenType type = getNonTerminalType(this.actualIndex);
                final Optional<String> nonTerminal = buildNonTerminal(this.actualContent, this.actualIndex);
                nonTerminal.ifPresent(nt -> {
                    builder.append(Token.getInstance(nt, type));
                    if (type != TokenType.IDENTIFIER) {
                        appendConcatenation(builder);
                    }
                });
            } else if (isWhitespace(temp)) {
                buildWhitespace(this.actualContent, this.actualIndex);
            }
        }

        builder.addValidator(getValidator());
        return builder.build();
    }

    private void appendConcatenation(TokenStreamLine.TokenStreamLineBuilder builder) {
        if (this.actualIndex < this.actualContent.length()) {
            builder.append(Token.getInstance(OP_CONCATENATION, TokenType.OPERATION_CONCAT));
        }
    }

    private TokenType getNonTerminalType(final int actualIndex) {
        if (actualIndex == 0) {
            return TokenType.IDENTIFIER;
        }

        return TokenType.NON_TERMINAL;
    }

    private TokenType getOperationType(final String operation) {
        switch (operation) {
            case OP_DEFINITION:
                return TokenType.OPERATION_DEF;
            case OP_CONCATENATION:
                return TokenType.OPERATION_CONCAT;
            case OP_TERMINATION:
                return TokenType.OPERATION_TERM;
            case OP_ALTERNATION:
                return TokenType.OPERATION_ALTER;
            case OP_OPTIONAL_BEGIN:
                return TokenType.OPERATION_OPT_BEGIN;
            case OP_OPTIONAL_END:
                return TokenType.OPERATION_OPT_END;
            case OP_REPETITION_BEGIN:
                return TokenType.OPERATION_REP_BEGIN;
            case OP_REPETITION_END:
                return TokenType.OPERATION_REP_END;
            case OP_GROUPING_BEGIN:
                return TokenType.OPERATION_GROUP_BEGIN;
            case OP_GROUPING_END:
                return TokenType.OPERATION_GROUP_END;
            default:
                throw new LexerException(
                        String.format("Not such operation type '%s'", operation));
        }
    }

    private String getActualString() {
        return getString(this.actualContent.charAt(actualIndex));
    }

    private String getString(final char c) {
        return c + "";
    }

    private boolean isTerminalQuotation(final String s) {
        return TERMINAL_QUOTATION.equals(s);
    }

    private boolean isOperation(final String s) {
        for (final String operation : OPERATIONS) {
            if (operation.equals(s)) {
                return true;
            }
        }

        return false;
    }

    private boolean isNonTerminal(final String s) {
        return !isOperation(s) && !isTerminalQuotation(s) && !isWhitespace(s);
    }

    private boolean isWhitespace(final String s) {
        return s.matches(WHITESPACES_REGEX);
    }

    private String buildTerminal(final String value, final int fromIndex) {
        final String quotationStart = value.substring(fromIndex + 1);
        final int quotationLength = quotationStart.indexOf(TERMINAL_QUOTATION);

        if (quotationLength != -1) {
            final String terminal = quotationStart.substring(0, quotationLength);
            this.actualIndex += terminal.length() + 2;

            return terminal;
        }

        throw new LexerException(
                String.format("Quotation started at %d is not closed", fromIndex));
    }

    private Optional<String> buildNonTerminal(final String value, final int fromIndex) {
        final String nonTerminalStart = value.substring(fromIndex);
        final StringBuilder nonTerminalBuilder = new StringBuilder();

        for (int i = 0; i < nonTerminalStart.length(); i++) {
            if (isNonTerminal(getString(nonTerminalStart.charAt(i)))) {
                nonTerminalBuilder.append(nonTerminalStart.charAt(i));
            } else {
                break;
            }
        }

        final String nonTerminal = nonTerminalBuilder.toString();
        this.actualIndex += nonTerminal.length();

        if (StringUtils.isAllBlank(nonTerminal)) {
            return Optional.empty();
        } else {
            return Optional.of(removeWhitespaces(nonTerminal));
        }
    }

    private String buildWhitespace(final String value, final int fromIndex) {
        final String whitespaceStart = value.substring(fromIndex);
        final StringBuilder whitespaceBuilder = new StringBuilder();

        for (int i = 0; i < whitespaceStart.length(); i++) {
            if (isWhitespace(getString(whitespaceStart.charAt(i)))) {
                whitespaceBuilder.append(whitespaceStart.charAt(i));
            } else {
                break;
            }
        }

        final String whitespace = whitespaceBuilder.toString();
        this.actualIndex += whitespace.length();

        return whitespaceBuilder.toString();
    }

    private String removeWhitespaces(final String line) {
        return line.replaceAll(WHITESPACES_REGEX, "");
    }
}
