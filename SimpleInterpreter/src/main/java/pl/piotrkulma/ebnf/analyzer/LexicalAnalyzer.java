package pl.piotrkulma.ebnf.analyzer;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import pl.piotrkulma.ebnf.Token;
import pl.piotrkulma.ebnf.TokenType;
import pl.piotrkulma.ebnf.analyzer.utils.Loader;
import pl.piotrkulma.ebnf.exceptions.LexicalAnalyzerException;
import pl.piotrkulma.ebnf.parser.ParsedTree;
import pl.piotrkulma.ebnf.parser.GrammarParser;
import pl.piotrkulma.ebnf.parser.ProgramParser;
import pl.piotrkulma.ebnf.parser.TreeNode;

import java.util.ArrayList;
import java.util.List;

public final class LexicalAnalyzer {
    private static final Logger LOG = LogManager.getLogger(LexicalAnalyzer.class);

    private final List<ParsedTree> grammarTree;
    private final List<ParsedTree> programTree;

    public static LexicalAnalyzer getInstance(final String grammarFile, final String programFile) {
        return new LexicalAnalyzer(grammarFile, programFile);
    }

    public void analyze() {
        final List<Token> programTokens = new ArrayList<>();
        flatProgramTree(this.programTree.get(0).getTree(), programTokens);

        final List<Token> expectedTokens = new ArrayList<>();
/*
        expectedTokens(grammarTree.get(0).getTree(), expectedTokens, 0, 0);

        for (Token token : expectedTokens) {
            LOG.info(token);
        }
*/
/*        boolean found;

        for (int i = 0; i < programTokens.size(); i++) {
            expectedTokens(this.grammarTree.get(0).getTree(), expectedTokens, 0, i);

            found = false;
            for (Token token : expectedTokens) {
                if (programTokens.get(i).getValue().equals(token.getValue())) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                throw new LexicalAnalyzerException(String.format("Expected %s found %s", expectedString(expectedTokens), programTokens.get(i)));
            }
        }*/
    }

    private String expectedString(final List<Token> expectedTokens) {
        final StringBuilder builder = new StringBuilder();

        for (Token token : expectedTokens) {
            builder.append(token);
            builder.append(" ");
        }

        return builder.toString();
    }

    private void expectedTokens(final TreeNode<Token> node, final List<Token> expectedTokens, int actLevel, int expLevel) {
        if (actLevel == expLevel) {
            if (node.getData().getType() == TokenType.OPERATION_CONCAT) {
                expectedTokens(node.getChildren().get(0), expectedTokens, actLevel, -1);
            } else {
                expectedTokens(node, expectedTokens, actLevel, -1);
            }
            return;
        }

        if (node.getChildren() != null) {
            if (expLevel == -1) {
                expectedTokens(node.getChildren().get(0), expectedTokens, actLevel + 1, expLevel);
            }
            expectedTokens(node.getChildren().get(1), expectedTokens, actLevel + 1, expLevel);
        } else {
            expectedTokens.add(node.getData());
        }
    }

    private void flatProgramTree(final TreeNode<Token> node, final List<Token> programTokens) {
        if (node.getData().getType() == TokenType.OPERATION_CONCAT || node.getData().getType() == TokenType.OPERATION_ALTER) {
            flatProgramTree(node.getChildren().get(0), programTokens);
            flatProgramTree(node.getChildren().get(1), programTokens);
        } else {
            programTokens.add(node.getData());
        }
    }

    private LexicalAnalyzer(final String grammarFile, final String programFile) {
/*        final GrammarParser grammarParser = new GrammarParser(
                Loader.getInstance().loadGrammarFromResource(grammarFile).getTokenStreams());
        this.grammarTree = grammarParser.createTrees();*/
        this.grammarTree = null;

        final ProgramParser programGrammarParser = new ProgramParser(
                Loader.getInstance().loadProgramFromResource(programFile).getTokenStreams());
        this.programTree = programGrammarParser.createTrees();
    }
}
