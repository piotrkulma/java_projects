package pl.piotrkulma.ebnf.analyzer.utils;

import pl.piotrkulma.ebnf.analyzer.lexer.GrammarLexer;
import pl.piotrkulma.ebnf.analyzer.lexer.Lexer;
import pl.piotrkulma.ebnf.analyzer.lexer.ProgramLexer;
import pl.piotrkulma.ebnf.exceptions.LoaderException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public final class Loader {
    public Lexer loadGrammarFromResource(final String fileName) {
        final Lexer lexer = new GrammarLexer();
        return loadFromResource(lexer, fileName);
    }

    public Lexer loadProgramFromResource(final String fileName) {
        final Lexer lexer = new ProgramLexer();
        return loadFromResource(lexer, fileName);
    }

    private Lexer loadFromResource(final Lexer grammarLexer, final String fileName) {
        final InputStream inputStream = Loader.class.getResourceAsStream(fileName);
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while((line = bufferedReader.readLine()) != null) {
                grammarLexer.append(line);
            }
        } catch (IOException e) {
            throw new LoaderException(String.format("Error in file '%s", fileName), e);
        }

        grammarLexer.build();
        return grammarLexer;
    }

    public static Loader getInstance() {
        return new Loader();
    }

    private Loader() {
    }
}
