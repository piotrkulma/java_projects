package pl.piotrkulma.ebnf.analyzer.lexer;

import pl.piotrkulma.ebnf.tokenstream.TokenStreamProgramValidator;
import pl.piotrkulma.ebnf.tokenstream.Validator;

public class ProgramLexer extends Lexer {
    private StringBuilder content;

    public ProgramLexer() {
        super();

        content = new StringBuilder();
    }

    @Override
    public void append(final String line) {
        content.append(line + " ");
    }

    @Override
    public void build() {
        this.rules.add(content.toString());
        this.built = true;
    }

    @Override
    public Validator getValidator() {
        return new TokenStreamProgramValidator();
    }
}
