package pl.piotrkulma.ebnf;

public final class Token {
    private final String value;
    private final TokenType type;

    public static Token getInstance(String value, TokenType type) {
        return new Token(value, type);
    }

    public String getValue() {
        return value;
    }

    public TokenType getType() {
        return type;
    }

    public Token copy() {
        return new Token(this.value, this.type);
    }

    private Token(String value, TokenType type) {
        this.value = value;
        this.type = type;
    }

    @Override
    public String toString() {
        return "Token{" +
                "value='" + value + '\'' +
                ", type=" + type +
                '}';
    }
}
