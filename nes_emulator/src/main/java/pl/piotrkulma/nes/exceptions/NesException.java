package pl.piotrkulma.nes.exceptions;

public class NesException extends RuntimeException {
    public NesException(String message) {
        super(message);
    }

    public NesException(String message, Throwable cause) {
        super(message, cause);
    }
}
