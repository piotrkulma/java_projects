package pl.piotrkulma.nes.memory;

import pl.piotrkulma.nes.ExecStats;
import pl.piotrkulma.nes.exceptions.NesException;
import pl.piotrkulma.nes.utils.NumberUtils;

import static pl.piotrkulma.nes.utils.NumberUtils.*;
import static pl.piotrkulma.nes.utils.NumberUtils.getHi8BitFrom16Bit;

public class MemoryBus {
    private boolean accessControl;
    private Memory nesMemory;
    protected MemoryBusReadCallback readCallback;

    public MemoryBus(Memory nesMemory) {
        this.accessControl = true;
        this.nesMemory = nesMemory;
    }

    public int read8(final int address) {
        int realAddr = getReadAddress(address);
        if (readCallback != null) {
            readCallback.read8(realAddr);
        }

        int ret = this.nesMemory.read(realAddr);
        ExecStats.getInstance().setLastRead8(ret);
        return ret;
    }

    public int read16(final int address) {
        return read16(address + 1, address);
    }

    public int read16(final int addrHi, final int addrLo) {
        int realAddrHi = getReadAddress(addrHi);
        int realAddrLo = getReadAddress(addrLo);

        int lo = this.read8(realAddrLo);
        int hi = this.read8(realAddrHi);

        read16Log(hi, lo);

        return get16BitFromTwo8Bit(hi, lo);
    }

    public void write8(final int address, final int value8) {
        int realAddr = getWriteAddress(address);

        ExecStats.getInstance().setLastWrite8(this.nesMemory.read(realAddr));
        this.nesMemory.write(realAddr, value8);
    }

    public void write16(final int address, final int value16) {
        int lo = getLo8BitFrom16Bit(value16);
        int hi = getHi8BitFrom16Bit(value16);

        int realAddressLo = getWriteAddress(address);
        int realAddressHi = getWriteAddress(address + 1);
        this.write8(realAddressLo, lo);
        this.write8(realAddressHi, hi);

        write16Log(realAddressHi, realAddressLo);
    }

    public int[] dump() {
        return this.nesMemory.dump();
    }

    public void setAccessControl(boolean accessControl) {
        this.accessControl = accessControl;
    }

    public void setReadCallback(MemoryBusReadCallback readCallback) {
        this.readCallback = readCallback;
    }

    private int getReadAddress(final int address) {
        if(!accessControl) {
            return address;
        }

        if (isInRange(address, 0x0000, 0x1FFF)) {
            return address & 0x7FF;
        }

        if (isInRange(address, 0x2000, 0x3FFF)) {
            return address & 0x2007;
        }

        //prg rom
        if (isInRange(address, 0x8000, 0x10000)) {
            return address;
        }

        throw new NesException("Illegal read attempt to memory at 0x%s".formatted(NumberUtils.hexString16(address)));
    }

    private int getWriteAddress(final int address) {
        if(!accessControl) {
            return address;
        }

        if (isInRange(address, 0x0000, 0x1FFF)) {
            return address & 0x7FF;
        }

        if (isInRange(address, 0x2000, 0x3FFF)) {
            return address & 0x2007;
        }

        throw new NesException("Illegal write attempt to memory at 0x%s".formatted(NumberUtils.hexString16(address)));
    }

    private boolean isInRange(final int val, final int from, final int to) {
        return val >= from && val <= to;
    }

    private void read16Log(final int hi, final int lo) {
        ExecStats.getInstance().setLastReadLo16(lo);
        ExecStats.getInstance().setLastReadHi16(hi);
        ExecStats.getInstance().setLastRead16(get16BitFromTwo8Bit(hi, lo));
    }

    private void write16Log(final int realAddressHi, final int realAddressLo) {
        ExecStats.getInstance().setLastWriteLo16(this.nesMemory.read(realAddressLo));
        ExecStats.getInstance().setLastWriteHi16(this.nesMemory.read(realAddressHi));
        ExecStats.getInstance().setLastWrite16(NumberUtils.get16BitFromTwo8Bit(
                ExecStats.getInstance().getLastWriteHi16(),
                ExecStats.getInstance().getLastWriteLo16()));
    }
}
