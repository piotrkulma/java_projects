package pl.piotrkulma.nes.memory;

public interface MemoryBusReadCallback {
    void read8(final int address);
}
