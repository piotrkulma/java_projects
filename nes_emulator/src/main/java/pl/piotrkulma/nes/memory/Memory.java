package pl.piotrkulma.nes.memory;

import static pl.piotrkulma.nes.utils.NumberUtils.*;

public class Memory {
    private static final int MEMORY_SIZE = 0x10000;

    private int memorySize;
    protected int[] memory;

    public Memory() {
        this.memorySize = MEMORY_SIZE;
        initMemory();
    }

    public Memory(final int memorySize) {
        this.memorySize = memorySize;
        initMemory();
    }

    public int read(final int address) {
        return norm8(this.memory[address]);
    }

    public void write(final int address, final int value8) {
        this.memory[address] = norm8(value8);
    }

    public int[] dump() {
        return this.memory.clone();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < memory.length; i++) {
            if (i % 16 == 0) {
                if (i != 0) {
                    stringBuilder.append("\r\n");
                }
                stringBuilder.append("%s : ".formatted(hexString16(i)));
            }
            stringBuilder.append(hexString8(this.memory[i]));
            stringBuilder.append(" ");
        }

        return stringBuilder.toString();
    }

    private void initMemory() {
        this.memory = new int[this.memorySize];
        for (int i = 0; i < this.memorySize; i++) {
            this.memory[i] = 0;
        }
    }
}
