package pl.piotrkulma.nes.utils;

public final class NumberUtils {
    private static final String LEADING_ZEROS[] = {"", "0", "00", "000"};

    public static int norm8(final int value) {
        return value & 0xFF;
    }

    public static int norm16(final int value) {
        return value & 0xFFFF;
    }

    public static int shiftLeft(int value, int bits) {
        return value << bits;
    }

    public static int shiftRight(int value, int bits) {
        return value >> bits;
    }

    public static int get16BitFromTwo8Bit(int hi8, int lo8) {
        return norm16((shiftLeft(hi8, 8) | lo8));
    }

    public static int getLo8BitFrom16Bit(int value16) {
        return norm8(value16);
    }

    public static int getHi8BitFrom16Bit(int value16) {
        return norm16(shiftRight(value16, 8));
    }

    public static int getBit(int value, int bitNo) {
        return shiftRight(value & (shiftLeft(1, bitNo)), bitNo);
    }

    public static int setBitValue(int val, int bitNo, int bit) {
        if(bit == 0) {
            return clearBit(val, bitNo);
        }

        return setBit(val, bitNo);
    }

    public static int setBit(int val, int bitNo) {
        return val | (0x1 << bitNo);
    }

    public static int clearBit(int val, int bitNo) {
        return val & (~(0x1 << bitNo));
    }

    public static String hexString8(int val) {
        return hexString(val, 2);
    }

    public static String hexString16(int val) {
        return hexString(val, 4);
    }

    private static String hexString(int value, int maxLength) {
        String hexValue = Integer.toHexString(value);
        return (LEADING_ZEROS[maxLength - hexValue.length()] + hexValue).toUpperCase();
    }

    private NumberUtils() {
    }
}
