package pl.piotrkulma.nes.utils;

import pl.piotrkulma.nes.memory.Memory;

public final class MemoryUtils {
    public static void write8Array(final Memory memory, final int startAddr, final int... value8) {
        for (int i = 0; i < value8.length; i++) {
            memory.write(startAddr + i, value8[i]);
        }
    }

    private MemoryUtils() {
    }
}
