package pl.piotrkulma.nes.utils;

import org.apache.commons.lang3.StringUtils;
import pl.piotrkulma.nes.ExecStats;
import pl.piotrkulma.nes.cpu.operations.OpCode;

import static pl.piotrkulma.nes.utils.OpCodeUtils.*;
import static pl.piotrkulma.nes.utils.NumberUtils.*;

public final class LoggerUtils {
    private static final String OP_CODE_LOG_TEMPLATE = "%s  %s  %s  %s";
    private static final String OP_CODE_HEX_DUMP_TEMPLATE = "%s %s %s";
    private static final String CPU_STATS_DUMP_TEMPLATE = "A:%s X:%s Y:%s P:%s SP:%s";

    public static String opCodeLog(final int[] memorySnapshot) {

        ExecStats execStats = ExecStats.getInstance();

        return OP_CODE_LOG_TEMPLATE.formatted(
                hexString16(execStats.getLastOperationPc()),
                hexDump(execStats),
                StringUtils.rightPad(opCode(execStats, memorySnapshot), 30),
                cpuDump(execStats));
    }

    private static String cpuDump(final ExecStats execStats) {
        return CPU_STATS_DUMP_TEMPLATE.formatted(
                hexString8(execStats.getRegAABeforeExec()),
                hexString8(execStats.getRegXBeforeExec()),
                hexString8(execStats.getRegYBeforeExec()),
                hexString8(execStats.getStatusBeforeExec()),
                hexString8(execStats.getStackPointerBeforeExec()));
    }

    private static String hexDump(final ExecStats execStats) {
        String opCodeHexDump;

        if (execStats.getLastOpCode().getBytes() == 3) {
            opCodeHexDump = OP_CODE_HEX_DUMP_TEMPLATE.formatted(
                    hexString8(execStats.getLastOpCode().getOpcode()),
                    hexString8(execStats.getLastArg16ReadLo()),
                    hexString8(execStats.getLastArg16ReadHi()));
        } else if (execStats.getLastOpCode().getBytes() == 2) {
            opCodeHexDump = OP_CODE_HEX_DUMP_TEMPLATE.formatted(
                    hexString8(execStats.getLastOpCode().getOpcode()),
                    hexString8(execStats.getLastArg8Read()),
                    "  ");
        } else {
            opCodeHexDump = OP_CODE_HEX_DUMP_TEMPLATE.formatted(
                    hexString8(execStats.getLastOpCode().getOpcode()),
                    "  ",
                    "  ");
        }

        return opCodeHexDump;
    }

    private static String opCode(final ExecStats execStats, final int[] memorySnapshot) {
        int lastRead = -1;
        OpCode opCode = execStats.getLastOpCode();

        if (opCode.getBytes() == 3) {
            lastRead = execStats.getLastRead16();
        } else if (opCode.getBytes() == 2) {
            lastRead = execStats.getLastRead8();
        }

        switch (opCode.getAddressingMode()) {
            case IMPLIED:
                return opCode.getName();
            case ACCUMULATOR:
                return "%s A".formatted(opCode.getName());
            case IMMEDIATE:
                return "%s #$%s".formatted(opCode.getName(), hexString8(lastRead));
            case ZERO_PAGE:
                return "%s $%s = %s".formatted(
                        opCode.getName(),
                        hexString8(execStats.getLastArg8Read()),
                        hexString8(memorySnapshot[execStats.getLastArg8Read()]));
            case ZERO_PAGE_X:
                return getZpXFormatted(
                        opCode,
                        execStats,
                        lastRead);
            case ZERO_PAGE_Y:
                return getZpYFormatted(
                        opCode,
                        execStats,
                        lastRead);
            case RELATIVE:
                return "%s $%s".formatted(
                        opCode.getName(),
                        hexString16(norm16((execStats.getLastOperationPc() + opCode.getBytes()) + lastRead)));
            case ABSOLUTE:
                return getAbsFormatted(execStats, opCode, memorySnapshot, lastRead);
            case ABSOLUTE_X:
                return getAbsXFormatted(opCode, execStats, lastRead);
            case ABSOLUTE_Y:
                return getAbsYFormatted(opCode, execStats, lastRead);
            case INDIRECT:
                return "%s ($%s) = %s".formatted(
                        opCode.getName(),
                        hexString16(execStats.getLastArg16Read()),
                        hexString16(lastRead));
            case INDEXED_INDIRECT_X:
                return getIndXFormatted(opCode, execStats, lastRead);
            case INDIRECT_INDEXED_Y:
                return getIndYFormatted(opCode, execStats, lastRead);
        }

        throw new RuntimeException("Addressing mode %s not implemented".formatted(opCode.getAddressingMode().name()));
    }

    private static String getZpYFormatted(final OpCode opCode, final ExecStats execStats, final int lastRead) {
        int lastVal = lastRead;
        if (opCode.getOpcode() == STX_0x96) {
            lastVal = execStats.getLastWrite8();
        }
        return "%s $%s,Y @ %s = %s".formatted(
                opCode.getName(),
                hexString8(execStats.getLastArg8Read()),
                hexString8(execStats.getLastAdditional8Ref()),
                hexString8(lastVal));
    }

    private static String getZpXFormatted(final OpCode opCode, final ExecStats execStats, final int lastRead) {
        int lastVal = lastRead;
        if (opCode.getOpcode() == STY_0x94 ||
                opCode.getOpcode() == STA_0x95 ||
                opCode.getOpcode() == INC_0xF6 ||
                opCode.getOpcode() == DEC_0xD6
        ) {
            lastVal = execStats.getLastWrite8();
        }
        return "%s $%s,X @ %s = %s".formatted(
                opCode.getName(),
                hexString8(execStats.getLastArg8Read()),
                hexString8(execStats.getLastAdditional8Ref()),
                hexString8(lastVal));
    }

    private static String getAbsXFormatted(final OpCode opCode, final ExecStats execStats, final int lastRead) {
        int lastVal = execStats.getLastRead8();
        if (opCode.getOpcode() == STA_0x9D ||
                opCode.getOpcode() == INC_0xFE ||
                opCode.getOpcode() == DEC_0xDE
        ) {
            lastVal = execStats.getLastWrite8();
        }
        return "%s $%s,X @ %s = %s".formatted(
                opCode.getName(),
                hexString16(lastRead),
                hexString16(execStats.getLastAdditional16Ref()),
                hexString8(lastVal));
    }

    private static String getAbsYFormatted(final OpCode opCode, final ExecStats execStats, final int lastRead) {
        int lastVal = execStats.getLastRead8();
        if (opCode.getOpcode() == STA_0x99) {
            lastVal = execStats.getLastWrite8();
        }
        return "%s $%s,Y @ %s = %s".formatted(
                opCode.getName(),
                hexString16(lastRead),
                hexString16(execStats.getLastAdditional16Ref()),
                hexString8(lastVal));
    }

    private static String getIndYFormatted(final OpCode opCode, final ExecStats execStats, final int lastRead) {
        int lastVal = lastRead;
        if (opCode.getOpcode() == STA_0x91) {
            lastVal = execStats.getLastWrite8();
        }
        return "%s ($%s),Y = %s @ %s = %s".formatted(
                opCode.getName(),
                hexString8(execStats.getLastArg8Read()),
                hexString16(execStats.getLastArg16Read()),
                hexString16(execStats.getLastAdditional16Ref()),
                hexString8(lastVal));
    }

    private static String getIndXFormatted(final OpCode opCode, final ExecStats execStats, final int lastRead) {

        int lastVal = lastRead;
        if (opCode.getOpcode() == STA_0x81) {
            lastVal = execStats.getLastWrite8();
        }

        return "%s ($%s,X) @ %s = %s = %s".formatted(
                opCode.getName(),
                hexString8(execStats.getLastArg8Read()),
                hexString8(execStats.getLastAdditional8Ref()),
                hexString16(execStats.getLastArg16Read()),
                hexString8(lastVal));
    }

    private static String getAbsFormatted(final ExecStats execStats,
                                          final OpCode opCode,
                                          final int[] memorySnapshot,
                                          final int opCodeParam) {
        if (opCode.getOpcode() != JMP_0x4C && opCode.getOpcode() != JSR_0x20) {
            return "%s $%s = %s".formatted(
                    opCode.getName(),
                    hexString16(opCodeParam),
                    hexString8(memorySnapshot[execStats.getLastArg16Read()]));
        }
        return "%s $%s".formatted(
                opCode.getName(),
                hexString16(opCodeParam));
    }

    private LoggerUtils() {
    }
}
