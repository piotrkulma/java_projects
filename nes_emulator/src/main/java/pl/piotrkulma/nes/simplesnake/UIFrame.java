package pl.piotrkulma.nes.simplesnake;

import javax.swing.*;

public class UIFrame extends JFrame {
    private UIDrawPanel drawPanel;

    public UIFrame(UIDrawPanel drawPanel) {
        this.drawPanel = drawPanel;

        init();
    }

    private void init() {
        add(this.drawPanel);

        setSize(500, 500);
        setTitle("UI");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
