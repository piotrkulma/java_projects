package pl.piotrkulma.nes.simplesnake;

import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

@Slf4j
public class UIDrawPanel extends JPanel {
    public interface KeyTypedCallback {
        void keyTyped(int keyCode);
    }

    private DrawingCallback drawingCallback;
    private KeyTypedCallback keyTypedCallback;

    private class UIDrawPanelKeyListener implements KeyListener {
        private boolean pressed;

        public UIDrawPanelKeyListener() {
            this.pressed = false;
        }

        @Override
        public void keyPressed(final KeyEvent e) {
            if (!this.pressed) {
                keyTypedCallback.keyTyped(e.getKeyCode());
                this.pressed = true;
            }
        }

        @Override
        public void keyReleased(final KeyEvent e) {
            this.pressed = false;
        }

        @Override
        public void keyTyped(final KeyEvent e) {
        }
    }

    public UIDrawPanel() {
        this.setFocusable(true);
        this.addKeyListener(new UIDrawPanelKeyListener());
    }

    @Override
    public void paintComponent(final Graphics g) {
        super.paintComponent(g);

        if (drawingCallback != null) {
            drawingCallback.draw((Graphics2D) g);
        }
    }

    public interface DrawingCallback {
        void draw(final Graphics2D g2d);
    }

    public void setDrawingCallback(DrawingCallback drawingCallback) {
        this.drawingCallback = drawingCallback;
    }

    public void setKeyTypedCallback(KeyTypedCallback keyTypedCallback) {
        this.keyTypedCallback = keyTypedCallback;
    }
}
