package pl.piotrkulma.nes.file;

import pl.piotrkulma.nes.file.exceptions.INesFileReaderException;

import java.io.InputStream;

public final class INesFileReader {
    private static INesFileReader INSTANCE;

    public static INesFileReader getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new INesFileReader();
        }

        return INSTANCE;
    }

    public INesFile readFromStream(final InputStream inputStream) {
        try {
            return readINesFile(inputStream);
        } catch (Exception e) {
            throw new INesFileReaderException("Error while reading iNes file", e);
        } finally {
            try {
                inputStream.close();
            } catch (Exception e) {
                throw new INesFileReaderException("Error while closing iNes file stream", e);
            }
        }
    }

    private INesFile readINesFile(final InputStream inputStream) {
        INesFileHeader header = readINesHeader(inputStream);

        return INesFile.builder()
                .header(header)
                .trainer(readBytesInt(inputStream, header.isTrainer() ? 512 : 0))
                .prgRomData(readBytesInt(inputStream, header.getPrgRom16Size() * 16384))
                .chrRomData(readBytesInt(inputStream, header.getChrRom8Size() * 8192))
                .build();
    }

    private INesFileHeader readINesHeader(final InputStream inputStream) {
        return INesFileHeader.builder()
                .nesMarker(new String(readBytes(inputStream, 4)))
                .prgRom16Size(readByteInt(inputStream))
                .chrRom8Size(readByteInt(inputStream))
                .flags6(readByteInt(inputStream))
                .flags7(readByteInt(inputStream))
                .flags8(readByteInt(inputStream))
                .flags9(readByteInt(inputStream))
                .flags10(readByteInt(inputStream))
                .unusedPadding(readBytesInt(inputStream, 5))
                .build();
    }

    private int readByteInt(final InputStream inputStream) {
        byte[] bytes = readBytes(inputStream, 1);

        return bytes[0];
    }

    private int[] readBytesInt(final InputStream inputStream, final int len) {
        byte[] bytes = readBytes(inputStream, len);
        int[] bytesInt = new int[len];

        for (int i = 0; i < len; i++) {
            bytesInt[i] = bytes[i];
        }

        return bytesInt;
    }

    private byte[] readBytes(final InputStream inputStream, final int len) {
        byte[] bytes = new byte[len];

        try {
            int readBytes = inputStream.read(bytes, 0, len);
            checkReadBytes(len, readBytes);
            return bytes;
        } catch (Exception e) {
            throw new INesFileReaderException(("" +
                    "Error while reading iNes file, len %d").formatted(len), e);
        }
    }

    private void checkReadBytes(final int expectedBytes, final int readBytes) {
        if (readBytes != expectedBytes) {
            throw new INesFileReaderException(
                    "Expected %d but read %d bytes".formatted(expectedBytes, readBytes));
        }
    }

    private INesFileReader() {
    }
}
