package pl.piotrkulma.nes.file;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class INesFile {
    private static final int PRG_ROM_BANK_SIZE = 16384;

    private INesFileHeader header;
    private int[] trainer;
    private int[] prgRomData;
    private int[] chrRomData;

    public int[] getPrgRomBank(int no) {
        int[] prgRomBank = new int[PRG_ROM_BANK_SIZE];

        int prgRomOffset = PRG_ROM_BANK_SIZE * no;
        for (int i = 0; i < PRG_ROM_BANK_SIZE; i++) {
            prgRomBank[i] = this.prgRomData[prgRomOffset + i];
        }

        return prgRomBank;
    }
}
