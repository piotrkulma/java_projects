package pl.piotrkulma.nes.file.exceptions;

public class INesFileReaderException extends RuntimeException {
    public INesFileReaderException(String message) {
        super(message);
    }

    public INesFileReaderException(String message, Throwable cause) {
        super(message, cause);
    }
}
