package pl.piotrkulma.nes.file;

import lombok.Builder;
import lombok.Getter;
import pl.piotrkulma.nes.utils.NumberUtils;

@Builder
@Getter
public class INesFileHeader {
    private String nesMarker;
    private int prgRom16Size;
    private int chrRom8Size;
    //Mapper, mirroring, battery, trainer
    private int flags6;
    // Mapper, VS/Playchoice, NES 2.0
    private int flags7;
    //PRG-RAM size (rarely used extension)
    private int flags8;
    //TV system (rarely used extension)
    private int flags9;
    //TV system, PRG-RAM presence (unofficial, rarely used extension)
    private int flags10;
    private int[] unusedPadding;

    public boolean isVerticalMirroring() {
        return NumberUtils.getBit(flags6, 0) == 1;
    }

    public boolean isBatteryBackedRam() {
        return NumberUtils.getBit(flags6, 1) == 1;
    }

    public boolean isTrainer() {
        return NumberUtils.getBit(flags6, 2) == 1;
    }

    public boolean isFourScreenVRam() {
        return NumberUtils.getBit(flags6, 3) == 1;
    }

    public int loBitsRomMapperNumber() {
        return NumberUtils.getBit(flags6, 4) << 0 |
                NumberUtils.getBit(flags6, 5) << 1 |
                NumberUtils.getBit(flags6, 6) << 2 |
                NumberUtils.getBit(flags6, 7) << 3;
    }

    public int hiBitsRomMapperNumber() {
        return NumberUtils.getBit(flags7, 4) << 0 |
                NumberUtils.getBit(flags7, 5) << 1 |
                NumberUtils.getBit(flags7, 6) << 2 |
                NumberUtils.getBit(flags7, 7) << 3;
    }

    public int romMapperNumber() {
        return NumberUtils.get16BitFromTwo8Bit(
                hiBitsRomMapperNumber(), loBitsRomMapperNumber());
    }
}
