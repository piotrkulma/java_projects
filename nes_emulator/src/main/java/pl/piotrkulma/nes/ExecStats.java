package pl.piotrkulma.nes;

import pl.piotrkulma.nes.cpu.operations.OpCode;
import pl.piotrkulma.nes.utils.NumberUtils;

public final class ExecStats {
    private static ExecStats INSTANCE;

    protected int lastRead8;
    protected int lastRead16;
    protected int lastReadLo16;
    protected int lastReadHi16;
    protected int lastWrite8;
    protected int lastWrite16;
    protected int lastWriteLo16;
    protected int lastWriteHi16;

    private int lastPcIncr;
    private int lastArg8Read;
    private int lastArg16Read;
    private int lastArg16ReadLo;
    private int lastArg16ReadHi;

    private int lastAdditional8Ref;
    private int lastAdditional16Ref;

    private int regAABeforeExec;
    private int regXBeforeExec;
    private int regYBeforeExec;
    private int statusBeforeExec;
    private int stackPointerBeforeExec;
    private int lastOperationPc;
    private OpCode lastOpCode;

    public static ExecStats getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ExecStats();
        }

        return INSTANCE;
    }

    public int getLastRead8() {
        return lastRead8;
    }

    public void setLastRead8(int lastRead8) {
        this.lastRead8 = lastRead8;
    }

    public int getLastRead16() {
        return lastRead16;
    }

    public void setLastRead16(int lastRead16) {
        this.lastRead16 = lastRead16;
    }

    public int getLastReadLo16() {
        return lastReadLo16;
    }

    public void setLastReadLo16(int lastReadLo16) {
        this.lastReadLo16 = lastReadLo16;
    }

    public int getLastReadHi16() {
        return lastReadHi16;
    }

    public void setLastReadHi16(int lastReadHi16) {
        this.lastReadHi16 = lastReadHi16;
    }

    public int getLastWrite8() {
        return lastWrite8;
    }

    public void setLastWrite8(int lastWrite8) {
        this.lastWrite8 = lastWrite8;
    }

    public int getLastWrite16() {
        return lastWrite16;
    }

    public void setLastWrite16(int lastWrite16) {
        this.lastWrite16 = lastWrite16;
    }

    public int getLastWriteLo16() {
        return lastWriteLo16;
    }

    public void setLastWriteLo16(int lastWriteLo16) {
        this.lastWriteLo16 = lastWriteLo16;
    }

    public int getLastWriteHi16() {
        return lastWriteHi16;
    }

    public void setLastWriteHi16(int lastWriteHi16) {
        this.lastWriteHi16 = lastWriteHi16;
    }

    public int getLastPcIncr() {
        return lastPcIncr;
    }

    public void setLastPcIncr(int lastPcIncr) {
        this.lastPcIncr = lastPcIncr;
    }

    public int getLastArg8Read() {
        return lastArg8Read;
    }

    public void setLastArg8Read(int lastArg8Read) {
        this.lastArg8Read = lastArg8Read;
    }

    public int getLastArg16Read() {
        return lastArg16Read;
    }

    public void setLastArg16Read(int lastArg16ReadHi, int lastArg16ReadLo) {
        this.lastArg16ReadHi = lastArg16ReadHi;
        this.lastArg16ReadLo = lastArg16ReadLo;
        this.lastArg16Read = NumberUtils.get16BitFromTwo8Bit(this.lastArg16ReadHi, this.lastArg16ReadLo);
    }

    public int getLastArg16ReadLo() {
        return lastArg16ReadLo;
    }

    public void setLastArg16ReadLo(int lastArg16ReadLo) {
        this.lastArg16ReadLo = lastArg16ReadLo;
    }

    public int getLastArg16ReadHi() {
        return lastArg16ReadHi;
    }

    public void setLastArg16ReadHi(int lastArg16ReadHi) {
        this.lastArg16ReadHi = lastArg16ReadHi;
    }

    public int getLastAdditional8Ref() {
        return lastAdditional8Ref;
    }

    public void setLastAdditional8Ref(int lastAdditional8Ref) {
        this.lastAdditional8Ref = lastAdditional8Ref;
    }

    public int getLastAdditional16Ref() {
        return lastAdditional16Ref;
    }

    public void setLastAdditional16Ref(int lastAdditional16Ref) {
        this.lastAdditional16Ref = lastAdditional16Ref;
    }

    public int getRegAABeforeExec() {
        return regAABeforeExec;
    }

    public void setRegAABeforeExec(int regAABeforeExec) {
        this.regAABeforeExec = regAABeforeExec;
    }

    public int getRegXBeforeExec() {
        return regXBeforeExec;
    }

    public void setRegXBeforeExec(int regXBeforeExec) {
        this.regXBeforeExec = regXBeforeExec;
    }

    public int getRegYBeforeExec() {
        return regYBeforeExec;
    }

    public void setRegYBeforeExec(int regYBeforeExec) {
        this.regYBeforeExec = regYBeforeExec;
    }

    public int getStatusBeforeExec() {
        return statusBeforeExec;
    }

    public void setStatusBeforeExec(int statusBeforeExec) {
        this.statusBeforeExec = statusBeforeExec;
    }

    public int getStackPointerBeforeExec() {
        return stackPointerBeforeExec;
    }

    public void setStackPointerBeforeExec(int stackPointerBeforeExec) {
        this.stackPointerBeforeExec = stackPointerBeforeExec;
    }

    public int getLastOperationPc() {
        return lastOperationPc;
    }

    public void setLastOperationPc(int lastOperationPc) {
        this.lastOperationPc = lastOperationPc;
    }

    public OpCode getLastOpCode() {
        return lastOpCode;
    }

    public void setLastOpCode(OpCode lastOpCode) {
        this.lastOpCode = lastOpCode;
    }
}
