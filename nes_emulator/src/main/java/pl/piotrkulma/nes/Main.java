package pl.piotrkulma.nes;

import pl.piotrkulma.nes.file.INesFile;
import pl.piotrkulma.nes.file.INesFileReader;

import java.io.InputStream;

public class Main {
    public static void main(String[] args) {
        InputStream romFileStream = Main.class.getResourceAsStream("/nestest.nes");

        assert romFileStream != null;

        INesFileReader fileReader = INesFileReader.getInstance();
        INesFile iNesFile = fileReader.readFromStream(romFileStream);
        Nes nes = new Nes();
        nes.start(iNesFile);
    }
}
