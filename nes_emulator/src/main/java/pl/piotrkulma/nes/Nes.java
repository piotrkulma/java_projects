package pl.piotrkulma.nes;

import pl.piotrkulma.nes.cpu.Cpu;
import pl.piotrkulma.nes.exceptions.NesException;
import pl.piotrkulma.nes.file.INesFile;
import pl.piotrkulma.nes.memory.Memory;
import pl.piotrkulma.nes.memory.MemoryBus;
import pl.piotrkulma.nes.utils.MemoryUtils;

public class Nes {
    private static final int PROGRAM_START_INDEX = 0x8000;
    private static final int PROGRAM_START_REF = 0xFFFC;
    private static final int PRG_ROM_BANK_SIZE = 16384;

    private final Cpu cpu;
    private final Memory memory;
    private final MemoryBus memoryBus;

    public Nes() {
        this.cpu = new Cpu();
        this.memory = new Memory();
        this.memoryBus = new MemoryBus(this.memory);
    }

    public void start(final INesFile iNesFile) {
        this.load(iNesFile);
        this.reset();

        cpu.interpret(this.memoryBus);
    }

    private void reset() {
        this.cpu.setRegA(0);
        this.cpu.setRegX(0);
        this.cpu.setRegY(0);
        this.cpu.setStaC(0);
        this.cpu.setStaZ(0);
        this.cpu.setStaI(1);
        this.cpu.setStaD(0);
        this.cpu.setStaB(0);
        this.cpu.setStaV(0);
        this.cpu.setStaN(0);
        this.cpu.setSp(0xFD);
        //this.cpu.setPc(this.memoryBus.read16(PROGRAM_START_REF));
        this.cpu.setPc(0xC000);
    }

    private void load(final INesFile iNesFile) {
        if (iNesFile.getHeader().getPrgRom16Size() > 2) {
            throw new NesException(
                    "Prg rom size %d is not supported yet".formatted(iNesFile.getHeader().getPrgRom16Size()));
        }

        if (iNesFile.getHeader().getPrgRom16Size() == 0) {
            throw new NesException("Prg rom size must be greater than zero");
        }

        if (iNesFile.getHeader().getPrgRom16Size() == 1) {
            int[] prgBank = iNesFile.getPrgRomBank(0);
            writeProgramToMemory(prgBank, prgBank);
        }

        if (iNesFile.getHeader().getPrgRom16Size() == 2) {
            writeProgramToMemory(iNesFile.getPrgRomBank(0), iNesFile.getPrgRomBank(1));
        }

        //this.memoryBus.write16(PROGRAM_START_REF, PROGRAM_START_INDEX);
    }

    private void writeProgramToMemory(final int[] lowerBank, final int[] upperBank) {
        MemoryUtils.write8Array(this.memory, PROGRAM_START_INDEX, lowerBank);
        MemoryUtils.write8Array(this.memory, PROGRAM_START_INDEX + PRG_ROM_BANK_SIZE, upperBank);
    }
}
