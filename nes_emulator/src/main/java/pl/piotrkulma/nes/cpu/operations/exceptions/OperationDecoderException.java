package pl.piotrkulma.nes.cpu.operations.exceptions;

public class OperationDecoderException extends RuntimeException {
    public OperationDecoderException(String message) {
        super(message);
    }

    public OperationDecoderException(String message, Throwable cause) {
        super(message, cause);
    }
}
