package pl.piotrkulma.nes.cpu;

import lombok.extern.slf4j.Slf4j;
import pl.piotrkulma.nes.ExecStats;
import pl.piotrkulma.nes.cpu.exceptions.CpuException;
import pl.piotrkulma.nes.cpu.operations.OpCode;
import pl.piotrkulma.nes.cpu.operations.OperationDecoder;
import pl.piotrkulma.nes.cpu.operations.OperationImpl;
import pl.piotrkulma.nes.memory.MemoryBus;
import pl.piotrkulma.nes.utils.LoggerUtils;

import java.lang.reflect.Method;

import static pl.piotrkulma.nes.utils.NumberUtils.*;

@Slf4j
public class Cpu {
    public interface StepCallback {
        void step(Cpu cpu);
    }

    private int regA;
    private int regX;
    private int regY;
    private int pc;
    private int sp;
    private int staC;
    private int staZ;
    private int staI;
    private int staD;
    private int staB;
    private int staV;
    private int staN;

    private Integer breakOpCode;
    private StepCallback stepCallback;

    public Cpu() {
        this.regA = 0;
        this.regX = 0;
        this.regY = 0;
        this.pc = 0;
        this.sp = 0;
        this.staC = 0;
        this.staZ = 0;
        this.staI = 0;
        this.staD = 0;
        this.staB = 0;
        this.staV = 0;
        this.staN = 0;
    }

    public void interpret(final MemoryBus memory) {
        int param;
        int[] memorySnapshot;
        OperationImpl operation = OperationImpl.getInstance();

        while (true) {
            if (stepCallback != null) {
                stepCallback.step(this);
            }

            ExecStats.getInstance().setLastOperationPc(this.pc);
            memorySnapshot = memory.dump();

            param = memory.read8(this.pc);
            this.pc = norm16(this.pc + 1);

            if (checkIfBreakOnOpCode(param)) {
                break;
            }

            OpCode opCode = OperationDecoder.decodeOpCode(param);
            Method method = OperationDecoder.decodeMethod(opCode);
            setStatsBeforeExecution(opCode);
            try {
                method.invoke(operation, this, memory, opCode.getAddressingMode());
                checkBytes(opCode.getBytes());
                logLastExec(memorySnapshot);
            } catch (Exception e) {
                throw new CpuException("Error while invoking method", e);
            }
        }
    }

    public void breakOnOpCode(int breakOpCode) {
        this.breakOpCode = breakOpCode;
    }

    public int getRegA() {
        return regA;
    }

    public void setRegA(int regA) {
        this.regA = regA;
    }

    public int getRegX() {
        return regX;
    }

    public void setRegX(int regX) {
        this.regX = regX;
    }

    public int getRegY() {
        return regY;
    }

    public void setRegY(int regY) {
        this.regY = regY;
    }

    public int getPc() {
        return pc;
    }

    public void setPc(int pc) {
        this.pc = pc;
    }

    public int getSp() {
        return sp;
    }

    public void setSp(int sp) {
        this.sp = sp;
    }

    public int getStaC() {
        return staC;
    }

    public void setStaC(int staC) {
        this.staC = staC;
    }

    public int getStaZ() {
        return staZ;
    }

    public void setStaZ(int staZ) {
        this.staZ = staZ;
    }

    public int getStaI() {
        return staI;
    }

    public void setStaI(int staI) {
        this.staI = staI;
    }

    public int getStaD() {
        return staD;
    }

    public void setStaD(int staD) {
        this.staD = staD;
    }

    public int getStaB() {
        return staB;
    }

    public void setStaB(int staB) {
        this.staB = staB;
    }

    public int getStaV() {
        return staV;
    }

    public void setStaV(int staV) {
        this.staV = staV;
    }

    public int getStaN() {
        return staN;
    }

    public void setStaN(int staN) {
        this.staN = staN;
    }

    public void setStepCallback(StepCallback stepCallback) {
        this.stepCallback = stepCallback;
    }

    public int getStatusByte() {
        return (staN << 7) |
                (staV << 6) |
                (1 << 5) |
                (staB << 4) |
                (staD << 3) |
                (staI << 2) |
                (staZ << 1) |
                (staC);
    }

    public void setStatusByte(int statusByte) {
        this.setStaN(getBit(statusByte, 7));
        this.setStaV(getBit(statusByte, 6));
        this.setStaB(getBit(statusByte, 4));
        this.setStaD(getBit(statusByte, 3));
        this.setStaI(getBit(statusByte, 2));
        this.setStaZ(getBit(statusByte, 1));
        this.setStaC(getBit(statusByte, 0));
    }

    private boolean checkIfBreakOnOpCode(int opCode) {
        return this.breakOpCode != null && this.breakOpCode.equals(opCode);
    }

    private void checkBytes(int opBytes) {
        int read = ExecStats.getInstance().getLastPcIncr() + 1;
        if (read != opBytes) {
            throw new CpuException(
                    "Difference between read opcode bytes (%d) and opcode declared size (%d bytes)"
                            .formatted(read, opBytes));
        }
    }

    private void logLastExec(final int[] memorySnapshot) {
        log.debug(LoggerUtils.opCodeLog(memorySnapshot));
    }

    private void setStatsBeforeExecution(OpCode opCode) {
        ExecStats.getInstance().setLastOpCode(opCode);
        ExecStats.getInstance().setRegAABeforeExec(this.regA);
        ExecStats.getInstance().setRegXBeforeExec(this.regX);
        ExecStats.getInstance().setRegYBeforeExec(this.regY);
        ExecStats.getInstance().setStatusBeforeExec(this.getStatusByte());
        ExecStats.getInstance().setStackPointerBeforeExec(this.getSp());
    }
}
