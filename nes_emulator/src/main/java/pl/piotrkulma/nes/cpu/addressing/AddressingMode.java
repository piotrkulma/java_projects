package pl.piotrkulma.nes.cpu.addressing;

import pl.piotrkulma.nes.ExecStats;
import pl.piotrkulma.nes.cpu.Cpu;
import pl.piotrkulma.nes.memory.MemoryBus;

import static pl.piotrkulma.nes.utils.NumberUtils.*;
import static pl.piotrkulma.nes.utils.NumberUtils.get16BitFromTwo8Bit;

public final class AddressingMode {
    private static AddressingMode INSTANCE;

    public static AddressingMode getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AddressingMode();
        }

        return INSTANCE;
    }

    public int immediate(final Cpu cpu, final MemoryBus memory) {
        int addr = cpu.getPc();
        ExecStats.getInstance().setLastArg8Read(memory.read8(addr));

        incrementPc(cpu, 1);

        return addr;
    }

    public int zeroPage(final Cpu cpu, final MemoryBus memory) {
        int addr = memory.read8(cpu.getPc());
        ExecStats.getInstance().setLastArg8Read(addr);

        incrementPc(cpu, 1);

        return addr;
    }

    public int zeroPageX(final Cpu cpu, final MemoryBus memory) {
        int off = memory.read8(cpu.getPc());
        ExecStats.getInstance().setLastArg8Read(off);

        incrementPc(cpu, 1);

        ExecStats.getInstance().setLastAdditional8Ref(norm8(off + cpu.getRegX()));

        return norm8(off + cpu.getRegX());
    }

    public int zeroPageY(final Cpu cpu, final MemoryBus memory) {
        int off = memory.read8(cpu.getPc());
        ExecStats.getInstance().setLastArg8Read(off);

        incrementPc(cpu, 1);

        ExecStats.getInstance().setLastAdditional8Ref(norm8(off + cpu.getRegY()));

        return norm8(off + cpu.getRegY());
    }

    public int absolute(final Cpu cpu, final MemoryBus memory) {
        int addr = memory.read16(cpu.getPc());
        ExecStats.getInstance().setLastArg16Read(
                ExecStats.getInstance().getLastReadHi16(),
                ExecStats.getInstance().getLastReadLo16());
        incrementPc(cpu, 2);

        return addr;
    }

    public int absoluteX(final Cpu cpu, final MemoryBus memory) {
        int off = memory.read16(cpu.getPc());
        ExecStats.getInstance().setLastArg16Read(
                ExecStats.getInstance().getLastReadHi16(),
                ExecStats.getInstance().getLastReadLo16());
        incrementPc(cpu, 2);

        ExecStats.getInstance().setLastAdditional16Ref(norm16(off + cpu.getRegX()));

        return norm16(off + cpu.getRegX());
    }

    public int absoluteY(final Cpu cpu, final MemoryBus memory) {
        int off = memory.read16(cpu.getPc());
        ExecStats.getInstance().setLastArg16Read(
                ExecStats.getInstance().getLastReadHi16(),
                ExecStats.getInstance().getLastReadLo16());;
        incrementPc(cpu, 2);

        ExecStats.getInstance().setLastAdditional16Ref(norm16(off + cpu.getRegY()));

        return norm16(off + cpu.getRegY());
    }

    /**
     * An original 6502 has does not correctly fetch the target address if the indirect.
     * Fetches the LSB from $xxFF as expected but takes the MSB from $xx00.
     */
    public int indirectForJMP(final Cpu cpu, final MemoryBus memory) {
        int valFromMemory = memory.read16(cpu.getPc());
        ExecStats.getInstance().setLastArg16Read(
                ExecStats.getInstance().getLastReadHi16(),
                ExecStats.getInstance().getLastReadLo16());
        incrementPc(cpu, 2);

        if(norm8(valFromMemory) == 0xFF) {
            return memory.read16(valFromMemory & 0xFF00, valFromMemory);
        }

        return memory.read16(valFromMemory);
    }

    public int indirect(final Cpu cpu, final MemoryBus memory) {
        int valFromMemory = memory.read16(cpu.getPc());
        ExecStats.getInstance().setLastArg16Read(
                ExecStats.getInstance().getLastReadHi16(),
                ExecStats.getInstance().getLastReadLo16());
        incrementPc(cpu, 2);

        return memory.read16(valFromMemory);
    }

    public int indexedIndirect(final Cpu cpu, final MemoryBus memory) {
        int valFromMemory = memory.read8(cpu.getPc());
        ExecStats.getInstance().setLastArg8Read(valFromMemory);
        incrementPc(cpu, 1);

        int add = norm8(valFromMemory + cpu.getRegX());

        ExecStats.getInstance().setLastAdditional8Ref(add);

        int lo = memory.read8(norm8(add));
        int hi = memory.read8(norm8(add + 1));

        ExecStats.getInstance().setLastArg16Read(hi, lo);

        return get16BitFromTwo8Bit(hi, lo);
    }

    public int indirectIndexed(final Cpu cpu, final MemoryBus memory) {
        int valFromMemory = memory.read8(cpu.getPc());
        ExecStats.getInstance().setLastArg8Read(valFromMemory);

        incrementPc(cpu, 1);

        int lo = memory.read8(norm8(valFromMemory));
        int hi = memory.read8(norm8(valFromMemory + 1));

        ExecStats.getInstance().setLastArg16Read(hi, lo);

        int memRef = norm16(get16BitFromTwo8Bit(hi, lo) + cpu.getRegY());
        ExecStats.getInstance().setLastAdditional16Ref(memRef);
        return memRef;
    }

    public int relative(final Cpu cpu, final MemoryBus memory) {
        int addr = cpu.getPc();
        ExecStats.getInstance().setLastArg8Read(memory.read8(addr));

        incrementPc(cpu, 1);

        return addr;
    }

    public int implied(final Cpu cpu, final MemoryBus memory) {
        incrementPc(cpu, 0);
        return -1;
    }

    public int accumulator(final Cpu cpu, final MemoryBus memory) {
        incrementPc(cpu, 0);
        return -2;
    }

    private void incrementPc(final Cpu cpu, final int value) {
        if (value > 0) {
            cpu.setPc(norm16(cpu.getPc() + value));
        }
        ExecStats.getInstance().setLastPcIncr(value);
    }

    private AddressingMode() {
    }
}
