package pl.piotrkulma.nes.cpu.operations;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Cycles {
    private int cycles;
    private int ifPageCrossed;
    private int ifBranchSucceeds;
    private int ifBranchToNewPage;
}
