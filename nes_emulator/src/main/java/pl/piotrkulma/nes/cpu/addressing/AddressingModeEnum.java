package pl.piotrkulma.nes.cpu.addressing;

public enum AddressingModeEnum {
    ACCUMULATOR,
    IMPLIED,
    IMMEDIATE,
    ZERO_PAGE, ZERO_PAGE_X, ZERO_PAGE_Y,
    ABSOLUTE, ABSOLUTE_X, ABSOLUTE_Y,
    INDIRECT,
    INDEXED_INDIRECT_X,
    INDIRECT_INDEXED_Y,
    RELATIVE
}
