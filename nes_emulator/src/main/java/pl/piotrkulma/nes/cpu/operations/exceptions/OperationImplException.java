package pl.piotrkulma.nes.cpu.operations.exceptions;

public class OperationImplException extends RuntimeException {
    public OperationImplException(String message) {
        super(message);
    }

    public OperationImplException(String message, Throwable cause) {
        super(message, cause);
    }
}
