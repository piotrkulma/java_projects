package pl.piotrkulma.nes.cpu.operations;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import pl.piotrkulma.nes.cpu.addressing.AddressingModeEnum;

@Data
@AllArgsConstructor
@ToString
public class OpCode {
    private int opcode;
    private String name;
    private AddressingModeEnum addressingMode;
    private int bytes;
    private Cycles cycles;
}
