package pl.piotrkulma.nes.cpu.operations;

import pl.piotrkulma.nes.cpu.Cpu;
import pl.piotrkulma.nes.cpu.addressing.AddressingModeEnum;
import pl.piotrkulma.nes.cpu.operations.exceptions.OperationDecoderException;
import pl.piotrkulma.nes.memory.MemoryBus;
import pl.piotrkulma.nes.utils.NumberUtils;

import java.lang.reflect.Method;
import java.util.Optional;
import java.util.stream.Stream;

public final class OperationDecoder {
    public static OpCode decodeOpCode(final int opcodeNum) {
        Optional<OpCode> opcodeObj = Stream.of(OpCodes.OPCODES).filter(opc -> opc.getOpcode() == opcodeNum).findFirst();

        if (opcodeObj.isEmpty()) {
            throw new OperationDecoderException("No such opcode %s".formatted(NumberUtils.hexString16(opcodeNum)));
        }

        return opcodeObj.get();
    }

    public static Method decodeMethod(final OpCode opcode) {
        return findMethod(opcode.getName());
    }


    private static Method findMethod(final String name) {
        try {
            return OperationImpl.class.getMethod(name.toLowerCase(), Cpu.class, MemoryBus.class, AddressingModeEnum.class);
        } catch (Exception e) {
            throw new OperationDecoderException("No such method %s".formatted(name.toLowerCase()));
        }
    }

    private OperationDecoder() {
    }
}
