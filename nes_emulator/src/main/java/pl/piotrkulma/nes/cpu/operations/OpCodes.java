package pl.piotrkulma.nes.cpu.operations;

import static pl.piotrkulma.nes.cpu.addressing.AddressingModeEnum.*;
import static pl.piotrkulma.nes.cpu.addressing.AddressingModeEnum.IMPLIED;

public final class OpCodes {
    public static final OpCode[] OPCODES = {
            new OpCode(0x69, "ADC", IMMEDIATE, 2, cycles(2).build()),
            new OpCode(0x65, "ADC", ZERO_PAGE, 2, cycles(3).build()),
            new OpCode(0x75, "ADC", ZERO_PAGE_X, 2, cycles(4).build()),
            new OpCode(0x6D, "ADC", ABSOLUTE, 3, cycles(4).build()),
            new OpCode(0x7D, "ADC", ABSOLUTE_X, 3, cycles(4).ifPageCrossed(1).build()),/*(+1 if page crossed)*/
            new OpCode(0x79, "ADC", ABSOLUTE_Y, 3, cycles(4).ifPageCrossed(1).build()),/*(+1 if page crossed)*/
            new OpCode(0x61, "ADC", INDEXED_INDIRECT_X, 2, cycles(6).build()),
            new OpCode(0x71, "ADC", INDIRECT_INDEXED_Y, 2, cycles(5).ifPageCrossed(1).build()),/*(+1 if page crossed)*/

            new OpCode(0x29, "AND", IMMEDIATE, 2, cycles(2).build()),
            new OpCode(0x25, "AND", ZERO_PAGE, 2, cycles(3).build()),
            new OpCode(0x35, "AND", ZERO_PAGE_X, 2, cycles(4).build()),
            new OpCode(0x2D, "AND", ABSOLUTE, 3, cycles(4).build()),
            new OpCode(0x3D, "AND", ABSOLUTE_X, 3, cycles(4).ifPageCrossed(1).build()),/*(+1 if page crossed)*/
            new OpCode(0x39, "AND", ABSOLUTE_Y, 3, cycles(4).ifPageCrossed(1).build()),/*(+1 if page crossed)*/
            new OpCode(0x21, "AND", INDEXED_INDIRECT_X, 2, cycles(6).build()),
            new OpCode(0x31, "AND", INDIRECT_INDEXED_Y, 2, cycles(5).ifPageCrossed(1).build()),/*(+1 if page crossed)*/

            new OpCode(0x0A, "ASL", ACCUMULATOR, 1, cycles(2).build()),
            new OpCode(0x06, "ASL", ZERO_PAGE, 2, cycles(5).build()),
            new OpCode(0x16, "ASL", ZERO_PAGE_X, 2, cycles(6).build()),
            new OpCode(0x0E, "ASL", ABSOLUTE, 3, cycles(6).build()),
            new OpCode(0x1E, "ASL", ABSOLUTE_X, 3, cycles(7).build()),

            new OpCode(0x90, "BCC", RELATIVE, 2, cycles(2).ifBranchSucceeds(1).ifBranchToNewPage(2).build()),/*(+1 if branch succeeds +2 if to a new page)*/
            new OpCode(0xB0, "BCS", RELATIVE, 2, cycles(2).ifBranchSucceeds(1).ifBranchToNewPage(2).build()),/*(+1 if branch succeeds +2 if to a new page)*/
            new OpCode(0xF0, "BEQ", RELATIVE, 2, cycles(2).ifBranchSucceeds(1).ifBranchToNewPage(2).build()),/*(+1 if branch succeeds +2 if to a new page)*/

            new OpCode(0x24, "BIT", ZERO_PAGE, 2, cycles(3).build()),
            new OpCode(0x2C, "BIT", ABSOLUTE, 3, cycles(4).build()),

            new OpCode(0x30, "BMI", RELATIVE, 2, cycles(2).ifBranchSucceeds(1).ifBranchToNewPage(2).build()),/*(+1 if branch succeeds +2 if to a new page)*/
            new OpCode(0xD0, "BNE", RELATIVE, 2, cycles(2).ifBranchSucceeds(1).ifBranchToNewPage(2).build()),/*(+1 if branch succeeds +2 if to a new page)*/
            new OpCode(0x10, "BPL", RELATIVE, 2, cycles(2).ifBranchSucceeds(1).ifBranchToNewPage(2).build()),/*(+1 if branch succeeds +2 if to a new page)*/

            new OpCode(0x00, "BRK", IMPLIED, 1, cycles(7).build()),

            new OpCode(0x50, "BVC", RELATIVE, 2, cycles(2).ifBranchSucceeds(1).ifBranchToNewPage(2).build()),/*(+1 if branch succeeds +2 if to a new page)*/
            new OpCode(0x70, "BVS", RELATIVE, 2, cycles(2).ifBranchSucceeds(1).ifBranchToNewPage(2).build()),/*(+1 if branch succeeds +2 if to a new page)*/

            new OpCode(0x18, "CLC", IMPLIED, 1, cycles(2).build()),
            new OpCode(0xD8, "CLD", IMPLIED, 1, cycles(2).build()),
            new OpCode(0x58, "CLI", IMPLIED, 1, cycles(2).build()),
            new OpCode(0xB8, "CLV", IMPLIED, 1, cycles(2).build()),

            new OpCode(0xC9, "CMP", IMMEDIATE, 2, cycles(2).build()),
            new OpCode(0xC5, "CMP", ZERO_PAGE, 2, cycles(3).build()),
            new OpCode(0xD5, "CMP", ZERO_PAGE_X, 2, cycles(4).build()),
            new OpCode(0xCD, "CMP", ABSOLUTE, 3, cycles(4).build()),
            new OpCode(0xDD, "CMP", ABSOLUTE_X, 3, cycles(4).ifPageCrossed(1).build()),/*(+1 if page crossed)*/
            new OpCode(0xD9, "CMP", ABSOLUTE_Y, 3, cycles(4).ifPageCrossed(1).build()),/*(+1 if page crossed)*/
            new OpCode(0xC1, "CMP", INDEXED_INDIRECT_X, 2, cycles(6).build()),
            new OpCode(0xD1, "CMP", INDIRECT_INDEXED_Y, 2, cycles(5).ifPageCrossed(1).build()),/*(+1 if page crossed)*/

            new OpCode(0xE0, "CPX", IMMEDIATE, 2, cycles(2).build()),
            new OpCode(0xE4, "CPX", ZERO_PAGE, 2, cycles(3).build()),
            new OpCode(0xEC, "CPX", ABSOLUTE, 3, cycles(4).build()),

            new OpCode(0xC0, "CPY", IMMEDIATE, 2, cycles(2).build()),
            new OpCode(0xC4, "CPY", ZERO_PAGE, 2, cycles(3).build()),
            new OpCode(0xCC, "CPY", ABSOLUTE, 3, cycles(4).build()),

            new OpCode(0xC6, "DEC", ZERO_PAGE, 2, cycles(5).build()),
            new OpCode(0xD6, "DEC", ZERO_PAGE_X, 2, cycles(6).build()),
            new OpCode(0xCE, "DEC", ABSOLUTE, 3, cycles(6).build()),
            new OpCode(0xDE, "DEC", ABSOLUTE_X, 3, cycles(7).build()),

            new OpCode(0xCA, "DEX", IMPLIED, 1, cycles(2).build()),
            new OpCode(0x88, "DEY", IMPLIED, 1, cycles(2).build()),

            new OpCode(0x49, "EOR", IMMEDIATE, 2, cycles(2).build()),
            new OpCode(0x45, "EOR", ZERO_PAGE, 2, cycles(3).build()),
            new OpCode(0x55, "EOR", ZERO_PAGE_X, 2, cycles(4).build()),
            new OpCode(0x4D, "EOR", ABSOLUTE, 3, cycles(4).build()),
            new OpCode(0x5D, "EOR", ABSOLUTE_X, 3, cycles(4).ifPageCrossed(1).build()),/*(+1 if page crossed)*/
            new OpCode(0x59, "EOR", ABSOLUTE_Y, 3, cycles(4).ifPageCrossed(1).build()),/*(+1 if page crossed)*/
            new OpCode(0x41, "EOR", INDEXED_INDIRECT_X, 2, cycles(6).build()),
            new OpCode(0x51, "EOR", INDIRECT_INDEXED_Y, 2, cycles(5).ifPageCrossed(1).build()),/*(+1 if page crossed)*/

            new OpCode(0xE6, "INC", ZERO_PAGE, 2, cycles(5).build()),
            new OpCode(0xF6, "INC", ZERO_PAGE_X, 2, cycles(6).build()),
            new OpCode(0xEE, "INC", ABSOLUTE, 3, cycles(6).build()),
            new OpCode(0xFE, "INC", ABSOLUTE_X, 3, cycles(7).build()),

            new OpCode(0xE8, "INX", IMPLIED, 1, cycles(2).build()),
            new OpCode(0xC8, "INY", IMPLIED, 1, cycles(2).build()),

            new OpCode(0x4C, "JMP", ABSOLUTE, 3, cycles(3).build()),
            new OpCode(0x6C, "JMP", INDIRECT, 3, cycles(5).build()),

            new OpCode(0x20, "JSR", ABSOLUTE, 3, cycles(6).build()),

            new OpCode(0xA9, "LDA", IMMEDIATE, 2, cycles(2).build()),
            new OpCode(0xA5, "LDA", ZERO_PAGE, 2, cycles(3).build()),
            new OpCode(0xB5, "LDA", ZERO_PAGE_X, 2, cycles(4).build()),
            new OpCode(0xAD, "LDA", ABSOLUTE, 3, cycles(4).build()),
            new OpCode(0xBD, "LDA", ABSOLUTE_X, 3, cycles(4).ifPageCrossed(1).build()),/*(+1 if page crossed)*/
            new OpCode(0xB9, "LDA", ABSOLUTE_Y, 3, cycles(4).ifPageCrossed(1).build()),/*(+1 if page crossed)*/
            new OpCode(0xA1, "LDA", INDEXED_INDIRECT_X, 2, cycles(6).build()),
            new OpCode(0xB1, "LDA", INDIRECT_INDEXED_Y, 2, cycles(5).ifPageCrossed(1).build()),/*(+1 if page crossed)*/

            new OpCode(0xA2, "LDX", IMMEDIATE, 2, cycles(2).build()),
            new OpCode(0xA6, "LDX", ZERO_PAGE, 2, cycles(3).build()),
            new OpCode(0xB6, "LDX", ZERO_PAGE_Y, 2, cycles(4).build()),
            new OpCode(0xAE, "LDX", ABSOLUTE, 3, cycles(4).build()),
            new OpCode(0xBE, "LDX", ABSOLUTE_Y, 3, cycles(4).ifPageCrossed(1).build()),/*(+1 if page crossed)*/

            new OpCode(0xA0, "LDY", IMMEDIATE, 2, cycles(2).build()),
            new OpCode(0xA4, "LDY", ZERO_PAGE, 2, cycles(3).build()),
            new OpCode(0xB4, "LDY", ZERO_PAGE_X, 2, cycles(4).build()),
            new OpCode(0xAC, "LDY", ABSOLUTE, 3, cycles(4).build()),
            new OpCode(0xBC, "LDY", ABSOLUTE_X, 3, cycles(4).ifPageCrossed(1).build()),/*(+1 if page crossed)*/

            new OpCode(0x4A, "LSR", ACCUMULATOR, 1, cycles(2).build()),
            new OpCode(0x46, "LSR", ZERO_PAGE, 2, cycles(5).build()),
            new OpCode(0x56, "LSR", ZERO_PAGE_X, 2, cycles(6).build()),
            new OpCode(0x4E, "LSR", ABSOLUTE, 3, cycles(6).build()),
            new OpCode(0x5E, "LSR", ABSOLUTE_X, 3, cycles(7).build()),

            new OpCode(0xEA, "NOP", IMPLIED, 1, cycles(2).build()),

            new OpCode(0x09, "ORA", IMMEDIATE, 2, cycles(2).build()),
            new OpCode(0x05, "ORA", ZERO_PAGE, 2, cycles(3).build()),
            new OpCode(0x15, "ORA", ZERO_PAGE_X, 2, cycles(4).build()),
            new OpCode(0x0D, "ORA", ABSOLUTE, 3, cycles(4).build()),
            new OpCode(0x1D, "ORA", ABSOLUTE_X, 3, cycles(4).ifPageCrossed(1).build()),/*(+1 if page crossed)*/
            new OpCode(0x19, "ORA", ABSOLUTE_Y, 3, cycles(4).ifPageCrossed(1).build()),/*(+1 if page crossed)*/
            new OpCode(0x01, "ORA", INDEXED_INDIRECT_X, 2, cycles(6).build()),
            new OpCode(0x11, "ORA", INDIRECT_INDEXED_Y, 2, cycles(5).ifPageCrossed(1).build()),/*(+1 if page crossed)*/

            new OpCode(0x48, "PHA", IMPLIED, 1, cycles(2).build()),
            new OpCode(0x08, "PHP", IMPLIED, 1, cycles(2).build()),
            new OpCode(0x68, "PLA", IMPLIED, 1, cycles(2).build()),
            new OpCode(0x28, "PLP", IMPLIED, 1, cycles(2).build()),

            new OpCode(0x2A, "ROL", ACCUMULATOR, 1, cycles(2).build()),
            new OpCode(0x26, "ROL", ZERO_PAGE, 2, cycles(5).build()),
            new OpCode(0x36, "ROL", ZERO_PAGE_X, 2, cycles(6).build()),
            new OpCode(0x2E, "ROL", ABSOLUTE, 3, cycles(6).build()),
            new OpCode(0x3E, "ROL", ABSOLUTE_X, 3, cycles(7).build()),

            new OpCode(0x6A, "ROR", ACCUMULATOR, 1, cycles(2).build()),
            new OpCode(0x66, "ROR", ZERO_PAGE, 2, cycles(5).build()),
            new OpCode(0x76, "ROR", ZERO_PAGE_X, 2, cycles(6).build()),
            new OpCode(0x6E, "ROR", ABSOLUTE, 3, cycles(6).build()),
            new OpCode(0x7E, "ROR", ABSOLUTE_X, 3, cycles(7).build()),

            new OpCode(0x40, "RTI", IMPLIED, 1, cycles(6).build()),

            new OpCode(0x60, "RTS", IMPLIED, 1, cycles(6).build()),

            new OpCode(0xE9, "SBC", IMMEDIATE, 2, cycles(2).build()),
            new OpCode(0xE5, "SBC", ZERO_PAGE, 2, cycles(3).build()),
            new OpCode(0xF5, "SBC", ZERO_PAGE_X, 2, cycles(4).build()),
            new OpCode(0xED, "SBC", ABSOLUTE, 3, cycles(4).build()),
            new OpCode(0xFD, "SBC", ABSOLUTE_X, 3, cycles(4).ifPageCrossed(1).build()),//(+1 if page crossed)
            new OpCode(0xF9, "SBC", ABSOLUTE_Y, 3, cycles(4).ifPageCrossed(1).build()),//(+1 if page crossed)
            new OpCode(0xE1, "SBC", INDEXED_INDIRECT_X, 2, cycles(6).build()),
            new OpCode(0xF1, "SBC", INDIRECT_INDEXED_Y, 2, cycles(5).ifPageCrossed(1).build()),//(+1 if page crossed)

            new OpCode(0x38, "SEC", IMPLIED, 1, cycles(2).build()),
            new OpCode(0xF8, "SED", IMPLIED, 1, cycles(2).build()),
            new OpCode(0x78, "SEI", IMPLIED, 1, cycles(2).build()),

            new OpCode(0x85, "STA", ZERO_PAGE, 2, cycles(3).build()),
            new OpCode(0x95, "STA", ZERO_PAGE_X, 2, cycles(4).build()),
            new OpCode(0x8D, "STA", ABSOLUTE, 3, cycles(4).build()),
            new OpCode(0x9D, "STA", ABSOLUTE_X, 3, cycles(5).build()),
            new OpCode(0x99, "STA", ABSOLUTE_Y, 3, cycles(5).build()),
            new OpCode(0x81, "STA", INDEXED_INDIRECT_X, 2, cycles(6).build()),
            new OpCode(0x91, "STA", INDIRECT_INDEXED_Y, 2, cycles(6).build()),

            new OpCode(0x86, "STX", ZERO_PAGE, 2, cycles(3).build()),
            new OpCode(0x96, "STX", ZERO_PAGE_Y, 2, cycles(4).build()),
            new OpCode(0x8E, "STX", ABSOLUTE, 3, cycles(4).build()),

            new OpCode(0x84, "STY", ZERO_PAGE, 2, cycles(3).build()),
            new OpCode(0x94, "STY", ZERO_PAGE_X, 2, cycles(4).build()),
            new OpCode(0x8C, "STY", ABSOLUTE, 3, cycles(4).build()),

            new OpCode(0xAA, "TAX", IMPLIED, 1, cycles(2).build()),
            new OpCode(0xA8, "TAY", IMPLIED, 1, cycles(2).build()),
            new OpCode(0xBA, "TSX", IMPLIED, 1, cycles(2).build()),
            new OpCode(0x8A, "TXA", IMPLIED, 1, cycles(2).build()),
            new OpCode(0x9A, "TXS", IMPLIED, 1, cycles(2).build()),
            new OpCode(0x98, "TYA", IMPLIED, 1,  cycles(2).build()),
    };

    private static Cycles.CyclesBuilder cycles(int cycles) {
        return Cycles.builder().cycles(cycles);
    }

    private OpCodes() {}
}
