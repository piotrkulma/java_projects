package pl.piotrkulma.nes.cpu.exceptions;

public class CpuException extends RuntimeException {
    public CpuException(String message) {
        super(message);
    }

    public CpuException(String message, Throwable cause) {
        super(message, cause);
    }
}
