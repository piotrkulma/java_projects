package pl.piotrkulma.nes.cpu.operations;

import pl.piotrkulma.nes.cpu.Cpu;
import pl.piotrkulma.nes.cpu.addressing.AddressingModeEnum;
import pl.piotrkulma.nes.cpu.addressing.AddressingMode;
import pl.piotrkulma.nes.cpu.operations.exceptions.OperationImplException;
import pl.piotrkulma.nes.memory.MemoryBus;
import pl.piotrkulma.nes.utils.NumberUtils;

import static pl.piotrkulma.nes.utils.NumberUtils.*;

public final class OperationImpl {
    private static final int STACK_POINTER_START_ADDR = 0x100;
    private static OperationImpl INSTANCE;
    private AddressingMode addressingMode;

    public static OperationImpl getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new OperationImpl();
        }

        return INSTANCE;
    }

    public void adc(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int addr = invokeAddressingMode(cpu, memory, addressingMode);
        int m = memory.read8(addr);
        int a = cpu.getRegA();
        int sum = a + m + cpu.getStaC();

        cpu.setRegA(norm8(sum));

        setCaryFlagAdc(cpu, sum);
        setZeroFlag(cpu, cpu.getRegA());
        setOverflowFlagAdc(cpu, a, m, sum);
        setNegativeFlag(cpu, cpu.getRegA());
    }

    public void and(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int addr = invokeAddressingMode(cpu, memory, addressingMode);
        int m = memory.read8(addr);

        cpu.setRegA(norm8(cpu.getRegA() & m));

        setZeroFlag(cpu, cpu.getRegA());
        setNegativeFlag(cpu, cpu.getRegA());
    }

    public void asl(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int old, shifted;
        int addr = invokeAddressingMode(cpu, memory, addressingMode);

        old = readMemory8(cpu, memory, addr);
        shifted = norm8(shiftLeft(old, 1));
        writeMemory8(cpu, memory, addr, shifted);

        cpu.setStaC(getBit(old, 7));
        setZeroFlag(cpu, shifted);
        setNegativeFlag(cpu, shifted);
    }

    public void bcc(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        branchIf(cpu, memory, addressingMode, cpu.getStaC(), 0);
    }

    public void bcs(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        branchIf(cpu, memory, addressingMode, cpu.getStaC(), 1);
    }

    public void beq(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        branchIf(cpu, memory, addressingMode, cpu.getStaZ(), 1);
    }

    public void bit(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int addr = invokeAddressingMode(cpu, memory, addressingMode);
        int m = memory.read8(addr);

        setZeroFlag(cpu, norm8(cpu.getRegA() & m));
        cpu.setStaV((m & 0b01000000) >> 6);
        cpu.setStaN((m & 0b10000000) >> 7);
    }

    public void bmi(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        branchIf(cpu, memory, addressingMode, cpu.getStaN(), 1);
    }

    public void bne(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        branchIf(cpu, memory, addressingMode, cpu.getStaZ(), 0);
    }

    public void bpl(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        branchIf(cpu, memory, addressingMode, cpu.getStaN(), 0);
    }

    public void brk(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);

        //padding byte
        cpu.setPc(norm16(cpu.getPc() + 1));

        cpu.setStaB(1);
        pushToStack8(cpu, memory, NumberUtils.getHi8BitFrom16Bit(cpu.getPc()));
        pushToStack8(cpu, memory, NumberUtils.getLo8BitFrom16Bit(cpu.getPc()));
        pushToStack8(cpu, memory, cpu.getStatusByte());
        cpu.setStaB(0);

        int newPc16 = memory.read16(0xFE);

        cpu.setPc(newPc16);
    }

    public void bvc(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        branchIf(cpu, memory, addressingMode, cpu.getStaV(), 0);
    }

    public void bvs(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        branchIf(cpu, memory, addressingMode, cpu.getStaV(), 1);
    }

    public void clc(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);
        cpu.setStaC(0);
    }

    public void cld(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);
        cpu.setStaD(0);
    }

    public void cli(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);
        cpu.setStaI(0);
    }

    public void clv(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);
        cpu.setStaV(0);
    }

    public void cmp(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int addr = invokeAddressingMode(cpu, memory, addressingMode);
        int m = memory.read8(addr);
        int res = norm8(cpu.getRegA() - m);

        cpu.setStaC(cpu.getRegA() >= m ? 1 : 0);
        cpu.setStaZ(cpu.getRegA() == m ? 1 : 0);
        setNegativeFlag(cpu, res);
    }

    public void cpx(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int addr = invokeAddressingMode(cpu, memory, addressingMode);
        int m = memory.read8(addr);
        int res = norm8(cpu.getRegX() - m);

        cpu.setStaC(cpu.getRegX() >= m ? 1 : 0);
        cpu.setStaZ(cpu.getRegX() == m ? 1 : 0);
        setNegativeFlag(cpu, res);
    }

    public void cpy(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int addr = invokeAddressingMode(cpu, memory, addressingMode);
        int m = memory.read8(addr);
        int res = norm8(cpu.getRegY() - m);

        cpu.setStaC(cpu.getRegY() >= m ? 1 : 0);
        cpu.setStaZ(cpu.getRegY() == m ? 1 : 0);
        setNegativeFlag(cpu, res);
    }

    public void dec(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int addr = invokeAddressingMode(cpu, memory, addressingMode);

        memory.write8(addr, memory.read8(addr) - 1);

        setZeroFlag(cpu, memory.read8(addr));
        setNegativeFlag(cpu, memory.read8(addr));
    }

    public void dex(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);
        cpu.setRegX(norm8(cpu.getRegX() - 1));

        setZeroFlag(cpu, cpu.getRegX());
        setNegativeFlag(cpu, cpu.getRegX());
    }

    public void dey(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);
        cpu.setRegY(norm8(cpu.getRegY() - 1));

        setZeroFlag(cpu, cpu.getRegY());
        setNegativeFlag(cpu, cpu.getRegY());
    }

    public void eor(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int addr = invokeAddressingMode(cpu, memory, addressingMode);
        int m = memory.read8(addr);

        cpu.setRegA(norm8(cpu.getRegA() ^ m));

        setZeroFlag(cpu, cpu.getRegA());
        setNegativeFlag(cpu, cpu.getRegA());
    }

    public void inc(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int addr = invokeAddressingMode(cpu, memory, addressingMode);
        memory.write8(addr, memory.read8(addr) + 1);

        setZeroFlag(cpu, memory.read8(addr));
        setNegativeFlag(cpu, memory.read8(addr));
    }

    public void inx(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);
        cpu.setRegX(norm8(cpu.getRegX() + 1));

        setZeroFlag(cpu, cpu.getRegX());
        setNegativeFlag(cpu, cpu.getRegX());
    }

    public void iny(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);
        cpu.setRegY(norm8(cpu.getRegY() + 1));

        setZeroFlag(cpu, cpu.getRegY());
        setNegativeFlag(cpu, cpu.getRegY());
    }

    public void jmp(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int addr;
        if (addressingMode == AddressingModeEnum.INDIRECT) {
            addr = this.addressingMode.indirectForJMP(cpu, memory);
        } else {
            addr = invokeAddressingMode(cpu, memory, addressingMode);
        }

        cpu.setPc(norm16(addr));
    }

    public void jsr(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int addr = invokeAddressingMode(cpu, memory, addressingMode);
        int retAddr = cpu.getPc() - 1;

        pushToStack8(cpu, memory, getHi8BitFrom16Bit(retAddr));
        pushToStack8(cpu, memory, getLo8BitFrom16Bit(retAddr));

        cpu.setPc(norm16(addr));
    }

    public void lda(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int addr = invokeAddressingMode(cpu, memory, addressingMode);
        int param = memory.read8(addr);
        cpu.setRegA(norm8(param));

        setZeroFlag(cpu, cpu.getRegA());
        setNegativeFlag(cpu, cpu.getRegA());
    }

    public void ldx(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int addr = invokeAddressingMode(cpu, memory, addressingMode);
        int param = memory.read8(addr);
        cpu.setRegX(norm8(param));

        setZeroFlag(cpu, cpu.getRegX());
        setNegativeFlag(cpu, cpu.getRegX());
    }

    public void ldy(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int addr = invokeAddressingMode(cpu, memory, addressingMode);
        int param = memory.read8(addr);
        cpu.setRegY(norm8(param));

        setZeroFlag(cpu, cpu.getRegY());
        setNegativeFlag(cpu, cpu.getRegY());
    }

    public void lsr(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int old, shifted;
        int addr = invokeAddressingMode(cpu, memory, addressingMode);

        old = readMemory8(cpu, memory, addr);
        shifted = norm8(shiftRight(old, 1));
        writeMemory8(cpu, memory, addr, shifted);

        cpu.setStaC(getBit(old, 0));
        setZeroFlag(cpu, shifted);
        setNegativeFlag(cpu, shifted);
    }

    public void nop(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);
    }

    public void ora(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int addr = invokeAddressingMode(cpu, memory, addressingMode);
        int m = memory.read8(addr);

        cpu.setRegA(norm8(cpu.getRegA() | m));

        setZeroFlag(cpu, cpu.getRegA());
        setNegativeFlag(cpu, cpu.getRegA());
    }

    public void pha(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);
        pushToStack8(cpu, memory, cpu.getRegA());
    }

    public void php(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);
        cpu.setStaB(1);
        pushToStack8(cpu, memory, cpu.getStatusByte());
        cpu.setStaB(0);
    }

    public void pla(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);

        cpu.setRegA(pullFromStack8(cpu, memory));

        setZeroFlag(cpu, cpu.getRegA());
        setNegativeFlag(cpu, cpu.getRegA());
    }

    public void plp(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);
        cpu.setStatusByte(pullFromStack8(cpu, memory));
        cpu.setStaB(0);
    }

    public void rol(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int old, shifted;
        int addr = invokeAddressingMode(cpu, memory, addressingMode);

        old = readMemory8(cpu, memory, addr);
        shifted = norm8(shiftLeft(old, 1));
        shifted = setBitValue(shifted, 0, cpu.getStaC());
        writeMemory8(cpu, memory, addr, shifted);

        cpu.setStaC(getBit(old, 7));
        setZeroFlag(cpu, shifted);
        setNegativeFlag(cpu, shifted);
    }

    public void ror(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int old, shifted;
        int addr = invokeAddressingMode(cpu, memory, addressingMode);

        old = readMemory8(cpu, memory, addr);
        shifted = norm8(shiftRight(old, 1));
        shifted = setBitValue(shifted, 7, cpu.getStaC());
        writeMemory8(cpu, memory, addr, shifted);

        cpu.setStaC(getBit(old, 0));
        setZeroFlag(cpu, shifted);
        setNegativeFlag(cpu, shifted);
    }

    public void rti(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);

        int statusByte = pullFromStack8(cpu, memory);
        int lo = pullFromStack8(cpu, memory);
        int hi = pullFromStack8(cpu, memory);

        cpu.setStatusByte(statusByte);
        cpu.setStaB(0);
        cpu.setPc(norm16(get16BitFromTwo8Bit(hi, lo)));
    }

    public void rts(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);

        int lo = pullFromStack8(cpu, memory);
        int hi = pullFromStack8(cpu, memory);

        cpu.setPc(norm16(get16BitFromTwo8Bit(hi, lo) + 1));
    }

    public void sbc(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int addr = invokeAddressingMode(cpu, memory, addressingMode);
        int m = memory.read8(addr);
        int a = cpu.getRegA();
        int sum = a - m - (1 - cpu.getStaC());

        cpu.setRegA(norm8(sum));

        setCaryFlagSbc(cpu, sum);
        setZeroFlag(cpu, cpu.getRegA());
        setOverflowFlagSub(cpu, a, m, sum);
        setNegativeFlag(cpu, cpu.getRegA());
    }

    public void sec(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);
        cpu.setStaC(1);
    }

    public void sed(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);
        cpu.setStaD(1);
    }

    public void sei(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);
        cpu.setStaI(1);
    }

    public void sta(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int addr = invokeAddressingMode(cpu, memory, addressingMode);
        memory.write8(addr, cpu.getRegA());
    }

    public void stx(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int addr = invokeAddressingMode(cpu, memory, addressingMode);
        memory.write8(addr, cpu.getRegX());
    }

    public void sty(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        int addr = invokeAddressingMode(cpu, memory, addressingMode);
        memory.write8(addr, cpu.getRegY());
    }

    public void tax(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);
        cpu.setRegX(norm8(cpu.getRegA()));

        setZeroFlag(cpu, cpu.getRegX());
        setNegativeFlag(cpu, cpu.getRegX());
    }

    public void tay(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);
        cpu.setRegY(norm8(cpu.getRegA()));

        setZeroFlag(cpu, cpu.getRegY());
        setNegativeFlag(cpu, cpu.getRegY());
    }

    public void tsx(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);
        cpu.setRegX(norm8(cpu.getSp()));

        setZeroFlag(cpu, cpu.getRegX());
        setNegativeFlag(cpu, cpu.getRegX());
    }

    public void txa(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);
        cpu.setRegA(norm8(cpu.getRegX()));

        setZeroFlag(cpu, cpu.getRegA());
        setNegativeFlag(cpu, cpu.getRegA());
    }

    public void txs(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);
        cpu.setSp(norm8(cpu.getRegX()));
    }

    public void tya(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode) {
        invokeAddressingMode(cpu, memory, addressingMode);
        cpu.setRegA(norm8(cpu.getRegY()));

        setZeroFlag(cpu, cpu.getRegA());
        setNegativeFlag(cpu, cpu.getRegA());
    }

    /**
     * @param a Accumulator before the addition
     * @param v the value adding to the accumulator
     * @param s the sum of the addition (a+v+c)
     */
    private void setOverflowFlagAdc(final Cpu cpu, final int a, final int v, final int s) {
        if (((a ^ s) & (v ^ s) & 0x80) > 0) {
            cpu.setStaV(1);
        } else {
            cpu.setStaV(0);
        }
    }

    private void setOverflowFlagSub(final Cpu cpu, final int a, final int v, final int s) {
        if (((a ^ s) & ((0xFF - v) ^ s) & 0x80) > 0) {
            cpu.setStaV(1);
        } else {
            cpu.setStaV(0);
        }
    }

    /**
     * @param cpu
     * @param rawValue value before normalization
     */
    private void setCaryFlagAdc(final Cpu cpu, final int rawValue) {
        if (rawValue > 0xFF) {
            cpu.setStaC(1);
        } else {
            cpu.setStaC(0);
        }
    }

    private void setCaryFlagSbc(final Cpu cpu, final int rawValue) {
        if (NumberUtils.norm16(rawValue) > 0xFF) {
            cpu.setStaC(0);
        } else {
            cpu.setStaC(1);
        }
    }

    private void setZeroFlag(final Cpu cpu, final int value) {
        if (value == 0) {
            cpu.setStaZ(1);
        } else {
            cpu.setStaZ(0);
        }
    }

    private void setNegativeFlag(final Cpu cpu, final int value) {
        if (getBit(value, 7) == 1) {
            cpu.setStaN(1);
        } else {
            cpu.setStaN(0);
        }
    }

    /**
     * Branch if flag value is set to check value
     *
     * @param cpu
     * @param memory
     * @param addressingMode
     * @param flag           flag value
     * @param check          value
     */
    private void branchIf(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingMode, int flag, int check) {
        int addr = invokeAddressingMode(cpu, memory, addressingMode);

        if (flag == check) {
            int m = (byte) memory.read8(addr);
            cpu.setPc(norm16(cpu.getPc() + m));
        }
    }

    private int invokeAddressingMode(final Cpu cpu, final MemoryBus memory, final AddressingModeEnum addressingModeEnum) {
        switch (addressingModeEnum) {
            case IMMEDIATE:
                return this.addressingMode.immediate(cpu, memory);
            case ZERO_PAGE:
                return this.addressingMode.zeroPage(cpu, memory);
            case ZERO_PAGE_X:
                return this.addressingMode.zeroPageX(cpu, memory);
            case ZERO_PAGE_Y:
                return this.addressingMode.zeroPageY(cpu, memory);
            case ABSOLUTE:
                return this.addressingMode.absolute(cpu, memory);
            case ABSOLUTE_X:
                return this.addressingMode.absoluteX(cpu, memory);
            case ABSOLUTE_Y:
                return this.addressingMode.absoluteY(cpu, memory);
            case INDEXED_INDIRECT_X:
                return this.addressingMode.indexedIndirect(cpu, memory);
            case INDIRECT_INDEXED_Y:
                return this.addressingMode.indirectIndexed(cpu, memory);
            case RELATIVE:
                return this.addressingMode.relative(cpu, memory);
            case IMPLIED:
                return this.addressingMode.implied(cpu, memory);
            case ACCUMULATOR:
                return this.addressingMode.accumulator(cpu, memory);
            case INDIRECT:
                return this.addressingMode.indirect(cpu, memory);
        }

        throw new OperationImplException("Addressing mode %s not implemented".formatted(addressingModeEnum.name()));
    }

    private void pushToStack8(final Cpu cpu, final MemoryBus memory, final int valToPush) {
        memory.write8(STACK_POINTER_START_ADDR | cpu.getSp(), valToPush);
        cpu.setSp(norm8(cpu.getSp() - 1));
    }

    private int pullFromStack8(final Cpu cpu, final MemoryBus memory) {
        cpu.setSp(norm8(cpu.getSp() + 1));
        return memory.read8(STACK_POINTER_START_ADDR | cpu.getSp());
    }

    private int readMemory8(final Cpu cpu, final MemoryBus memory, int addr) {
        if (addr == -2) {
            return cpu.getRegA();
        }

        return memory.read8(addr);
    }

    private void writeMemory8(final Cpu cpu, final MemoryBus memory, int addr, int val) {
        if (addr == -2) {
            cpu.setRegA(val);
        } else {
            memory.write8(addr, val);
        }
    }

    private OperationImpl() {
        this.addressingMode = AddressingMode.getInstance();
    }
}
