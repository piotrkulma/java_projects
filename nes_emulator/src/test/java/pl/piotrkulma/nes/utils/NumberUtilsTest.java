package pl.piotrkulma.nes.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberUtilsTest {
    @Test
    public void testGetBit() {
        assertEquals(0, NumberUtils.getBit(0, 0));
        assertEquals(0, NumberUtils.getBit(0, 10));
        assertEquals(1, NumberUtils.getBit(0b0101, 0));
        assertEquals(0, NumberUtils.getBit(0b0101, 1));
        assertEquals(1, NumberUtils.getBit(0b0101, 2));
        assertEquals(0, NumberUtils.getBit(0b0101, 3));
    }

    @Test
    public void testSetBit() {
        assertEquals(0b111, NumberUtils.setBit(0b110, 0));
        assertEquals(0b111, NumberUtils.setBit(0b101, 1));
        assertEquals(0b111, NumberUtils.setBit(0b011, 2));
        assertEquals(0b001, NumberUtils.setBit(0b000, 0));
        assertEquals(0b010, NumberUtils.setBit(0b000, 1));
        assertEquals(0b100, NumberUtils.setBit(0b000, 2));
    }

    @Test
    public void testClearBit() {
        assertEquals(0b110, NumberUtils.clearBit(0b111, 0));
        assertEquals(0b101, NumberUtils.clearBit(0b111, 1));
        assertEquals(0b011, NumberUtils.clearBit(0b111, 2));
        assertEquals(0b000, NumberUtils.clearBit(0b001, 0));
        assertEquals(0b000, NumberUtils.clearBit(0b010, 1));
        assertEquals(0b000, NumberUtils.clearBit(0b100, 2));
        assertEquals(0b000, NumberUtils.clearBit(0b000, 0));
        assertEquals(0b000, NumberUtils.clearBit(0b000, 1));
        assertEquals(0b000, NumberUtils.clearBit(0b000, 2));
    }

    @Test
    public void testSetBitValue() {
        assertEquals(0b110, NumberUtils.setBitValue(0b111, 0, 0));
        assertEquals(0b111, NumberUtils.setBitValue(0b110, 0, 1));
    }
}