package pl.piotrkulma.nes.memory;

import org.junit.jupiter.api.Test;
import pl.piotrkulma.nes.utils.MemoryUtils;

import static org.junit.jupiter.api.Assertions.*;

class MemoryBusTest {
    @Test
    public void testMemoryWrite8Test1() {
        Memory memory = new Memory();
        MemoryBus nesMemoryBus = new MemoryBus(memory);
        nesMemoryBus.setAccessControl(false);
        nesMemoryBus.write8(100, 0x01);
        nesMemoryBus.write8(102, 0x03);
        nesMemoryBus.write8(104, 0xFF);

        assertEquals(0x01, nesMemoryBus.read8(100));
        assertEquals(0x03, nesMemoryBus.read8(102));
        assertEquals(0xFF, nesMemoryBus.read8(104));
    }

    @Test
    public void testMemoryWrite8Test2() {
        Memory memory = new Memory();
        MemoryBus nesMemoryBus = new MemoryBus(memory);
        nesMemoryBus.setAccessControl(false);
        nesMemoryBus.write8(100, 0x01FF);
        nesMemoryBus.write8(102, 0xABCD);
        nesMemoryBus.write8(104, 0x1001);

        assertEquals(0xFF, nesMemoryBus.read8(100));
        assertEquals(0xCD, nesMemoryBus.read8(102));
        assertEquals(0x01, nesMemoryBus.read8(104));
    }

    @Test
    public void testMemoryWrite8ArrayTest1() {
        Memory memory = new Memory();
        MemoryBus nesMemoryBus = new MemoryBus(memory);
        nesMemoryBus.setAccessControl(false);
        MemoryUtils.write8Array(memory, 100, 0x01, 0x02, 0x03);

        assertEquals(0x00, nesMemoryBus.read8(99));
        assertEquals(0x01, nesMemoryBus.read8(100));
        assertEquals(0x02, nesMemoryBus.read8(101));
        assertEquals(0x03, nesMemoryBus.read8(102));
        assertEquals(0x00, nesMemoryBus.read8(103));
    }

    @Test
    public void testMemoryWrite16Test1() {
        Memory memory = new Memory();
        MemoryBus nesMemoryBus = new MemoryBus(memory);
        nesMemoryBus.setAccessControl(false);
        nesMemoryBus.write16(100, 0x1001);
        nesMemoryBus.write16(102, 0xABCD);
        nesMemoryBus.write16(104, 0xFFFF);

        assertEquals(0x1001, nesMemoryBus.read16(100));
        assertEquals(0xABCD, nesMemoryBus.read16(102));
        assertEquals(0xFFFF, nesMemoryBus.read16(104));
    }

    @Test
    public void testMemoryWrite16Test2() {
        Memory memory = new Memory();
        MemoryBus nesMemoryBus = new MemoryBus(memory);
        nesMemoryBus.setAccessControl(false);
        nesMemoryBus.write16(100, 0x11001);
        nesMemoryBus.write16(102, 0xFFABCD);
        nesMemoryBus.write16(104, 0xABFFFF);

        assertEquals(0x1001, nesMemoryBus.read16(100));
        assertEquals(0xABCD, nesMemoryBus.read16(102));
        assertEquals(0xFFFF, nesMemoryBus.read16(104));
    }

    @Test
    public void testMemoryRead16Test1() {
        Memory memory = new Memory();
        MemoryBus nesMemoryBus = new MemoryBus(memory);
        nesMemoryBus.setAccessControl(false);
        nesMemoryBus.write8(100, 0xCD);
        nesMemoryBus.write8(101, 0xAB);

        assertEquals(0xABCD, nesMemoryBus.read16(100));
    }

    @Test
    public void testMemoryRead16Test2() {
        Memory memory = new Memory();
        MemoryBus nesMemoryBus = new MemoryBus(memory);
        nesMemoryBus.setAccessControl(false);
        nesMemoryBus.write8(100, 0xFFCD);
        nesMemoryBus.write8(101, 0xFFAB);

        assertEquals(0xABCD, nesMemoryBus.read16(100));
    }
}