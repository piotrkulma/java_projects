package pl.piotrkulma.nes;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.piotrkulma.nes.file.INesFile;
import pl.piotrkulma.nes.file.INesFileReader;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class NesTest {
    private static final String LOG_FROM_TEST = "tests/nesemu.log";
    private static final String LOG_PATTERN = "pattern_log.txt";

    @BeforeEach
    public void init() {
        File oldLogFile = new File(LOG_FROM_TEST);
        if (oldLogFile.exists()) {
            oldLogFile.delete();
        }
    }

    public void nesTest() {
        InputStream romFileStream = Main.class.getResourceAsStream("/nestest.nes");

        assert romFileStream != null;

        INesFileReader fileReader = INesFileReader.getInstance();
        INesFile iNesFile = fileReader.readFromStream(romFileStream);
        Nes nes = new Nes();

        try {
            nes.start(iNesFile);
        } catch (Exception e) {
        }

        try {
            List<String> patternLogLine = readFile(getClass().getClassLoader().getResourceAsStream(LOG_PATTERN));
            List<String> logFromTestLines = readFile(new FileInputStream(LOG_FROM_TEST));

            for (int i =0; i<patternLogLine.size(); i++) {
                if(i > 5002) break;
                assertEquals(patternLogLine.get(i).trim(), logFromTestLines.get(i).trim());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<String> readFile(InputStream inputStream) throws IOException {
        List<String> lines = new ArrayList<>();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        String line;
        while ((line = bufferedReader.readLine()) != null) {
            lines.add(line);
        }
        inputStream.close();

        return lines;
    }

}