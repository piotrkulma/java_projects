package pl.piotrkulma.nes.cpu.operations;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CyclesTest {
    @Test
    public void testCyclesBuilder() {
        Cycles cycles = new Cycles
                .CyclesBuilder()
                .cycles(1)
                .ifBranchToNewPage(2)
                .ifBranchSucceeds(3)
                .ifPageCrossed(4)
                .build();

        assertEquals(1, cycles.getCycles());
        assertEquals(2, cycles.getIfBranchToNewPage());
        assertEquals(3, cycles.getIfBranchSucceeds());
        assertEquals(4, cycles.getIfPageCrossed());
    }
}