package pl.piotrkulma.nes.cpu;

import org.junit.jupiter.api.Test;
import pl.piotrkulma.nes.memory.Memory;
import pl.piotrkulma.nes.memory.MemoryBus;

import static pl.piotrkulma.nes.utils.OpCodeUtils.*;
import static org.junit.jupiter.api.Assertions.*;

import static pl.piotrkulma.nes.utils.MemoryUtils.*;

class CpuTest {
    @Test
    void testADC_69_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                SEC_0x38,
                LDA_0xA9, 0x50,
                ADC_0x69, 0x10,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);

        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaV());
        assertEquals(0, cpu.getStaN());
        assertEquals(6, cpu.getPc());
        assertEquals(0x61, cpu.getRegA());
    }

    @Test
    void testADC_69_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                SEC_0x38,
                LDA_0xA9, 0x50,
                ADC_0x69, 0x50,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);

        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaV());
        assertEquals(1, cpu.getStaN());
        assertEquals(6, cpu.getPc());
        assertEquals(0xA1, cpu.getRegA());
    }

    @Test
    void testADC_69_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                CLC_0x18,
                LDA_0xA9, 0x50,
                ADC_0x69, 0x90,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setStaC(1);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaV());
        assertEquals(1, cpu.getStaN());
        assertEquals(6, cpu.getPc());
        assertEquals(0xE0, cpu.getRegA());
    }

    @Test
    void testADC_69_test4() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                SEC_0x38,
                LDA_0xA9, 0x50,
                ADC_0x69, 0xd0,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setStaC(0);
        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaV());
        assertEquals(0, cpu.getStaN());
        assertEquals(6, cpu.getPc());
        assertEquals(0x21, cpu.getRegA());
    }

    @Test
    void testADC_69_test5() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                SEC_0x38,
                LDA_0xA9, 0x50,
                ADC_0x69, 0xd0,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setStaC(0);
        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaV());
        assertEquals(0, cpu.getStaN());
        assertEquals(6, cpu.getPc());
        assertEquals(0x21, cpu.getRegA());
    }

    @Test
    void testADC_69_test6() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                CLC_0x18,
                LDA_0xA9, 0xd0,
                ADC_0x69, 0x10,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setStaC(0);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaV());
        assertEquals(1, cpu.getStaN());
        assertEquals(6, cpu.getPc());
        assertEquals(0xE0, cpu.getRegA());
    }

    @Test
    void testADC_69_test7() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                CLC_0x18,
                LDA_0xA9, 0xd0,
                ADC_0x69, 0x50,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setStaC(0);
        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaV());
        assertEquals(0, cpu.getStaN());
        assertEquals(6, cpu.getPc());
        assertEquals(0x20, cpu.getRegA());
    }

    @Test
    void testADC_69_test8() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                CLC_0x18,
                LDA_0xA9, 0xd0,
                ADC_0x69, 0x90,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setStaC(0);
        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaV());
        assertEquals(0, cpu.getStaN());
        assertEquals(6, cpu.getPc());
        assertEquals(0x60, cpu.getRegA());
    }

    @Test
    void testADC_69_test9() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                SEC_0x38,
                LDA_0xA9, 0xd0,
                ADC_0x69, 0xd0,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setStaC(0);
        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaV());
        assertEquals(1, cpu.getStaN());
        assertEquals(6, cpu.getPc());
        assertEquals(0xA1, cpu.getRegA());
    }

    @Test
    void testAND_29_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0xF0,
                AND_0x29, 0x0F,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setStaC(0);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(5, cpu.getPc());
        assertEquals(0x00, cpu.getRegA());
    }

    @Test
    void testAND_29_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0x0F,
                AND_0x29, 0xF0,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setStaC(0);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(5, cpu.getPc());
        assertEquals(0x00, cpu.getRegA());
    }

    @Test
    void testAND_29_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0xFF,
                AND_0x29, 0xF0,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setStaC(0);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(5, cpu.getPc());
        assertEquals(0xF0, cpu.getRegA());
    }

    @Test
    void testAND_29_test4() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0xA,
                AND_0x29, 0xB,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setStaC(0);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(5, cpu.getPc());
        assertEquals(0xA, cpu.getRegA());
    }

    @Test
    void testASL_0A_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b00001111,
                ASL_0A,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(0x604, cpu.getPc());
        assertEquals(0b00011110, cpu.getRegA());
    }

    @Test
    void testASL_0A_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b01111110,
                ASL_0A,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(0x604, cpu.getPc());
        assertEquals(0b11111100, cpu.getRegA());
    }

    @Test
    void testASL_0A_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b11110000,
                ASL_0A,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(0x604, cpu.getPc());
        assertEquals(0b11100000, cpu.getRegA());
    }

    @Test
    void testASL_0A_test4() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b00000000,
                ASL_0A,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(0x604, cpu.getPc());
        assertEquals(0b00000000, cpu.getRegA());
    }

    @Test
    void testASL_0A_test5() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b10000000,
                ASL_0A,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaC());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(0x604, cpu.getPc());
        assertEquals(0b00000000, cpu.getRegA());
    }

    @Test
    void testASL_06_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b00001111,
                STA_0x85, 0x10,
                ASL_06, 0x10,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(0x607, cpu.getPc());
        assertEquals(0b00001111, cpu.getRegA());
        assertEquals(0b00011110, memoryBus.read8(0x10));
    }

    @Test
    void testASL_06_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b01111110,
                STA_0x85, 0x10,
                ASL_06, 0x10,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(0x607, cpu.getPc());
        assertEquals(0b01111110, cpu.getRegA());
        assertEquals(0b11111100, memoryBus.read8(0x10));
    }

    @Test
    void testASL_06_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b11110000,
                STA_0x85, 0x10,
                ASL_06, 0x10,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(0x607, cpu.getPc());
        assertEquals(0b11110000, cpu.getRegA());
        assertEquals(0b11100000, memoryBus.read8(0x10));
    }

    @Test
    void testASL_06_test4() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b00000000,
                STA_0x85, 0x10,
                ASL_06, 0x10,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(0x607, cpu.getPc());
        assertEquals(0b00000000, cpu.getRegA());
        assertEquals(0b00000000, memoryBus.read8(0x10));
    }

    @Test
    void testASL_06_test5() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b10000000,
                STA_0x85, 0x10,
                ASL_06, 0x10,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaC());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(0x607, cpu.getPc());
        assertEquals(0b10000000, cpu.getRegA());
        assertEquals(0b00000000, memoryBus.read8(0x10));
    }

    @Test
    void testBCC_90_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                BCC_0x90, 0x04,
                LDA_0xA9, 0x0D,
                LDA_0xA9, 0x0C,
                LDX_0xA2, 0x0B,
                LDY_0xA0, 0x0A);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.setStaC(0);
        cpu.interpret(memoryBus);

        assertEquals(0x00, cpu.getRegA());
        assertEquals(0x0B, cpu.getRegX());
        assertEquals(0x0A, cpu.getRegY());
        assertEquals(0x60B, cpu.getPc());
    }

    @Test
    void testBCC_90_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                BCC_0x90, 0x05,
                LDA_0xA9, 0x0D,
                LDA_0xA9, 0x0C,
                BRK,
                LDA_0xA9, 0x00,
                LDX_0xA2, 0x0B,
                LDY_0xA0, 0x0A,
                BCC_0x90, 0xF5);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.setStaC(0);
        cpu.interpret(memoryBus);

        assertEquals(0x0C, cpu.getRegA());
        assertEquals(0x0B, cpu.getRegX());
        assertEquals(0x0A, cpu.getRegY());
        assertEquals(0x607, cpu.getPc());
    }

    @Test
    void testBCC_90_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                SEC_0x38,
                BCC_0x90, 0x05,
                LDA_0xA9, 0x0D,
                LDA_0xA9, 0x0C,
                BRK,
                LDA_0xA9, 0x00,
                LDX_0xA2, 0x0B,
                LDY_0xA0, 0x0A,
                BCC_0x90, 0xF3);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x0C, cpu.getRegA());
        assertEquals(0x00, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x608, cpu.getPc());
    }

    @Test
    void testBCS_B0_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                SEC_0x38,
                BCS_0xB0, 0x05,
                LDA_0xA9, 0x0D,
                LDA_0xA9, 0x0C,
                BRK,
                LDA_0xA9, 0x00,
                LDX_0xA2, 0x0B,
                LDY_0xA0, 0x0A,
                BCS_0xB0, 0xF3);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x0C, cpu.getRegA());
        assertEquals(0x0b, cpu.getRegX());
        assertEquals(0x0a, cpu.getRegY());
        assertEquals(0x608, cpu.getPc());
    }

    @Test
    void testBCS_B0_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                CLC_0x18,
                BCS_0xB0, 0x05,
                LDA_0xA9, 0x0D,
                LDA_0xA9, 0x0C,
                BRK,
                LDA_0xA9, 0x00,
                LDX_0xA2, 0x0B,
                LDY_0xA0, 0x0A,
                BCS_0xB0, 0xF3);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x0C, cpu.getRegA());
        assertEquals(0x00, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x608, cpu.getPc());
    }

    @Test
    void testBEQ_F0_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0x00,
                BEQ_0xF0, 0x05,
                LDA_0xA9, 0x0D,
                LDA_0xA9, 0x0C,
                BRK,
                LDX_0xA2, 0x0B,
                LDY_0xA0, 0x0A,
                LDA_0xA9, 0x00,
                BEQ_0xF0, 0xF3);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x0C, cpu.getRegA());
        assertEquals(0x0b, cpu.getRegX());
        assertEquals(0x0a, cpu.getRegY());
        assertEquals(0x609, cpu.getPc());
    }

    @Test
    void testBEQ_F0_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0x01,
                BEQ_0xF0, 0x05,
                LDA_0xA9, 0x0D,
                LDA_0xA9, 0x0C,
                BRK,
                LDX_0xA2, 0x0B,
                LDY_0xA0, 0x0A,
                LDA_0xA9, 0x01,
                BEQ_0xF0, 0xF3);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x0C, cpu.getRegA());
        assertEquals(0x00, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x609, cpu.getPc());
    }

    @Test
    void testBIT_24_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0xC1,
                LDX_0xA2, 0xC0,
                STX_0x86, 0x10,
                BIT_0x24, 0x10);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0xC0, memoryBus.read8(0x10));
        assertEquals(0xC1, cpu.getRegA());
        assertEquals(0xC0, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x00, cpu.getStaZ());
        assertEquals(0x01, cpu.getStaN());
        assertEquals(0x01, cpu.getStaV());
        assertEquals(0x609, cpu.getPc());
    }

    @Test
    void testBIT_24_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0x00,
                LDX_0xA2, 0xC0,
                STX_0x86, 0x10,
                BIT_0x24, 0x10);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0xC0, memoryBus.read8(0x10));
        assertEquals(0x00, cpu.getRegA());
        assertEquals(0xC0, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x01, cpu.getStaZ());
        assertEquals(0x01, cpu.getStaN());
        assertEquals(0x01, cpu.getStaV());
        assertEquals(0x609, cpu.getPc());
    }

    @Test
    void testBIT_24_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0xF1,
                LDX_0xA2, 0x01,
                STX_0x86, 0x10,
                BIT_0x24, 0x10);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x01, memoryBus.read8(0x10));
        assertEquals(0xF1, cpu.getRegA());
        assertEquals(0x01, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x00, cpu.getStaZ());
        assertEquals(0x00, cpu.getStaN());
        assertEquals(0x00, cpu.getStaV());
        assertEquals(0x609, cpu.getPc());
    }

    @Test
    void testBMI_30_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0xF0,
                BMI_0x30, 0x05,
                LDA_0xA9, 0x0D,
                LDA_0xA9, 0x0C,
                BRK,
                LDX_0xA2, 0x0B,
                LDY_0xA0, 0x0A,
                LDA_0xA9, 0xF0,
                BMI_0x30, 0xF3);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x0C, cpu.getRegA());
        assertEquals(0x0b, cpu.getRegX());
        assertEquals(0x0a, cpu.getRegY());
        assertEquals(0x609, cpu.getPc());
    }

    @Test
    void testBMI_30_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0x0F,
                BMI_0x30, 0x05,
                LDA_0xA9, 0x0D,
                LDA_0xA9, 0x0C,
                BRK,
                LDX_0xA2, 0x0B,
                LDY_0xA0, 0x0A,
                LDA_0xA9, 0x0F,
                BMI_0x30, 0xF3);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x0C, cpu.getRegA());
        assertEquals(0x00, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x609, cpu.getPc());
    }

    @Test
    void testBNE_D0_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0x01,
                BNE_0xD0, 0x05,
                LDA_0xA9, 0x0D,
                LDA_0xA9, 0x0C,
                BRK,
                LDX_0xA2, 0x0B,
                LDY_0xA0, 0x0A,
                LDA_0xA9, 0x01,
                BNE_0xD0, 0xF3);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x0C, cpu.getRegA());
        assertEquals(0x0b, cpu.getRegX());
        assertEquals(0x0a, cpu.getRegY());
        assertEquals(0x609, cpu.getPc());
    }

    @Test
    void testBNE_D0_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0x00,
                BNE_0xD0, 0x05,
                LDA_0xA9, 0x0D,
                LDA_0xA9, 0x0C,
                BRK,
                LDX_0xA2, 0x0B,
                LDY_0xA0, 0x0A,
                LDA_0xA9, 0x00,
                BNE_0xD0, 0xF3);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x0C, cpu.getRegA());
        assertEquals(0x00, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x609, cpu.getPc());
    }

    @Test
    void testBPL_10_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0x01,
                BPL_0x10, 0x05,
                LDA_0xA9, 0x0D,
                LDA_0xA9, 0x0C,
                BRK,
                LDX_0xA2, 0x0B,
                LDY_0xA0, 0x0A,
                LDA_0xA9, 0x01,
                BPL_0x10, 0xF3);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x0C, cpu.getRegA());
        assertEquals(0x0b, cpu.getRegX());
        assertEquals(0x0a, cpu.getRegY());
        assertEquals(0x609, cpu.getPc());
    }

    @Test
    void testBPL_10_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0xF0,
                BPL_0x10, 0x05,
                LDA_0xA9, 0x0D,
                LDA_0xA9, 0x0C,
                BRK,
                LDX_0xA2, 0x0B,
                LDY_0xA0, 0x0A,
                LDA_0xA9, 0xF0,
                BPL_0x10, 0xF3);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x0C, cpu.getRegA());
        assertEquals(0x00, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x609, cpu.getPc());
    }

    @Test
    void tesBRK_00_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0xFF,
                LDX_0xA2, 0xFF,
                LDY_0xA0, 0xFF,
                BRK, 0x00,//BRK and padding byte
                LDA_0xA9, 0xA,
                LDX_0xA2, 0xB,
                LDY_0xA0, 0xC,
                CLC_0x18);

        memoryBus.write8(0xFE, 0x0C);
        memoryBus.write8(0xFF, 0x06);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(CLC_0x18);
        cpu.setPc(0x600);
        cpu.setSp(0xFF);
        cpu.interpret(memoryBus);

        assertEquals(0x608, memoryBus.read16(0x100 | 0xFE));
        assertEquals(0b10110000, memoryBus.read8(0x100 | 0xFD));
        assertEquals(0xFF, cpu.getRegA());
        assertEquals(0xFF, cpu.getRegX());
        assertEquals(0x0C, cpu.getRegY());
        assertEquals(0x00, cpu.getStaB());
        assertEquals(0x60F, cpu.getPc());
        assertEquals(0xFC, cpu.getSp());
    }

    @Test
    void testBVC_50_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0x01,
                BVC_0x50, 0x05,
                LDA_0xA9, 0x0D,
                LDA_0xA9, 0x0C,
                BRK,
                LDX_0xA2, 0x0B,
                LDY_0xA0, 0x0A,
                LDA_0xA9, 0x01,
                BVC_0x50, 0xF3);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x0C, cpu.getRegA());
        assertEquals(0x0b, cpu.getRegX());
        assertEquals(0x0a, cpu.getRegY());
        assertEquals(0x609, cpu.getPc());
    }

    @Test
    void testBVC_50_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0x50,
                ADC_0x69, 0x50,
                BVC_0x50, 0x05,
                LDA_0xA9, 0x0D,
                LDA_0xA9, 0x0C,
                BRK,
                LDX_0xA2, 0x0B,
                LDY_0xA0, 0x0A,
                LDA_0xA9, 0x01,
                BVC_0x50, 0xF3);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x01, cpu.getStaV());
        assertEquals(0x0C, cpu.getRegA());
        assertEquals(0x00, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x60b, cpu.getPc());
    }

    @Test
    void testBVS_70_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0x50,
                ADC_0x69, 0x50,
                BVS_0x70, 0x05,
                LDA_0xA9, 0x0D,
                LDA_0xA9, 0x0C,
                BRK,
                LDX_0xA2, 0x0B,
                LDY_0xA0, 0x0A,
                LDA_0xA9, 0x01,
                BVS_0x70, 0xF3);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x01, cpu.getStaV());
        assertEquals(0x0C, cpu.getRegA());
        assertEquals(0x0B, cpu.getRegX());
        assertEquals(0x0A, cpu.getRegY());
        assertEquals(0x60b, cpu.getPc());
    }

    @Test
    void testBVS_70_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0x50,
                ADC_0x69, 0x01,
                BVS_0x70, 0x05,
                LDA_0xA9, 0x0D,
                LDA_0xA9, 0x0C,
                BRK,
                LDX_0xA2, 0x0B,
                LDY_0xA0, 0x0A,
                LDA_0xA9, 0x01,
                BVS_0x70, 0xF3);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x00, cpu.getStaV());
        assertEquals(0x0C, cpu.getRegA());
        assertEquals(0x00, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x60b, cpu.getPc());
    }

    @Test
    void testCLC_18_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0x18);
        memoryBus.write8(1, 0x00);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setStaC(1);

        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(2, cpu.getPc());
    }

    @Test
    void testCLD_D8_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0xD8);
        memoryBus.write8(1, 0x00);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setStaD(1);

        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaD());
        assertEquals(2, cpu.getPc());
    }

    @Test
    void testCLI_58_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0x58);
        memoryBus.write8(1, 0x00);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setStaI(1);

        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaI());
        assertEquals(2, cpu.getPc());
    }

    @Test
    void testCLV_B8_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0xB8);
        memoryBus.write8(1, 0x00);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setStaV(1);

        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaV());
        assertEquals(2, cpu.getPc());
    }

    @Test
    void testCMP_C9_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0x06,
                CMP_0xC9, 0x08,
                BRK);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(5, cpu.getPc());
    }

    @Test
    void testCMP_C9_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0x06,
                CMP_0xC9, 0x06,
                BRK);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaC());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(5, cpu.getPc());
    }

    @Test
    void testCMP_C9_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0x06,
                CMP_0xC9, 0x04,
                BRK);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(5, cpu.getPc());
    }

    @Test
    void testCPX_E0_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDX_0xA2, 0x06,
                CPX_0xE0, 0x08,
                BRK);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(5, cpu.getPc());
    }

    @Test
    void testCPX_E0_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDX_0xA2, 0x06,
                CPX_0xE0, 0x06,
                BRK);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaC());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(5, cpu.getPc());
    }

    @Test
    void testCPX_E0_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDX_0xA2, 0x06,
                CPX_0xE0, 0x04,
                BRK);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(5, cpu.getPc());
    }

    @Test
    void testCPY_C0_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDY_0xA0, 0x06,
                CPY_0xC0, 0x08,
                BRK);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(5, cpu.getPc());
    }

    @Test
    void testCPY_C0_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDY_0xA0, 0x06,
                CPY_0xC0, 0x06,
                BRK);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaC());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(5, cpu.getPc());
    }

    @Test
    void testCPY_C0_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDY_0xA0, 0x06,
                CPY_0xC0, 0x04,
                BRK);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(5, cpu.getPc());
    }

    @Test
    void testDEC_C6_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0x02,
                STA_0x85, 0x08,
                DEC_0xC6, 0x08,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(0x01, memoryBus.read8(0x08));
        assertEquals(7, cpu.getPc());
    }

    @Test
    void testDEC_C6_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0x00,
                STA_0x85, 0x08,
                DEC_0xC6, 0x08,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(0xFF, memoryBus.read8(0x08));
        assertEquals(7, cpu.getPc());
    }

    @Test
    void testDEX_CA_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDX_0xA2, 0x05,
                DEX_0xCA,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(4, cpu.getRegX());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testDEX_CA_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDX_0xA2, 0x01,
                DEX_0xCA,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getRegX());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testDEX_CA_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDX_0xA2, 0x00,
                DEX_0xCA,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0xFF, cpu.getRegX());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testDEY_88_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDY_0xA0, 0x05,
                DEY_0x88,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(4, cpu.getRegY());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testDEY_88_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDY_0xA0, 0x01,
                DEY_0x88,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getRegY());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testDEY_88_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDY_0xA0, 0x00,
                DEY_0x88,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0xFF, cpu.getRegY());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testEOR_49_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0xA,
                EOR_0x49, 0xB,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(5, cpu.getPc());
        assertEquals(0x01, cpu.getRegA());
    }

    @Test
    void testEOR_49_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0x1,
                EOR_0x49, 0x1,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(5, cpu.getPc());
        assertEquals(0x00, cpu.getRegA());
    }

    @Test
    void testEOR_49_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0x10,
                EOR_0x49, 0xFF,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(5, cpu.getPc());
        assertEquals(0xEF, cpu.getRegA());
    }

    @Test
    void testINC_E6_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0x02,
                STA_0x85, 0x08,
                INC_0xE6, 0x08,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(0x03, memoryBus.read8(0x08));
        assertEquals(7, cpu.getPc());
    }

    @Test
    void testINC_E6_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0xFF,
                STA_0x85, 0x08,
                INC_0xE6, 0x08,
                BRK);
        memoryBus.write8(0x08, 100);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(0x00, memoryBus.read8(0x08));
        assertEquals(7, cpu.getPc());
    }

    @Test
    void testINC_E6_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0x7F,
                STA_0x85, 0x08,
                INC_0xE6, 0x08,
                BRK);
        memoryBus.write8(0x08, 100);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(0x80, memoryBus.read8(0x08));
        assertEquals(7, cpu.getPc());
    }

    @Test
    void testINX_E8_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0xE8);
        memoryBus.write8(1, 0xE8);
        memoryBus.write8(2, 0xE8);
        memoryBus.write8(3, 0x00);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(3, cpu.getRegX());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testINX_E8_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0xA9);
        memoryBus.write8(1, 0b11111111);
        memoryBus.write8(2, 0xAA);
        memoryBus.write8(3, 0xE8);
        memoryBus.write8(4, 0x00);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getRegX());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(5, cpu.getPc());
    }

    @Test
    void testINX_E8_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0xA9);
        memoryBus.write8(1, 0b11111110);
        memoryBus.write8(2, 0xAA);
        memoryBus.write8(3, 0xE8);
        memoryBus.write8(4, 0x00);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0b11111111, cpu.getRegX());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(5, cpu.getPc());
    }

    @Test
    void testINX_E8_test4() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDX_0xA2, 0xFF,
                INX_0xE8,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getRegX());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testINY_C8_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDY_0xA0, 0x05,
                INY_0xC8,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0x06, cpu.getRegY());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testINY_C8_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDY_0xA0, 0xFF,
                INY_0xC8,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0x00, cpu.getRegY());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testINY_C8_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDY_0xA0, 0x7F,
                INY_0xC8,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0x80, cpu.getRegY());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testJMP_4C_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0x03,
                JMP_0x4C, 0x08, 0x06,
                BRK,
                BRK,
                BRK,
                STA_0x8D, 0x00, 0x02);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x03, cpu.getRegA());
        assertEquals(0x03, memoryBus.read8(0x0200));
        assertEquals(0x60c, cpu.getPc());
    }

    @Test
    void testJMP_6C_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0x01,
                STA_0x85, 0xf0,
                LDA_0xA9, 0xcc,
                STA_0x85, 0xf1,
                JMP_0x6C, 0xf0, 00);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x01, cpu.getStaN());
        assertEquals(0xcc, cpu.getRegA());
        assertEquals(0x01, memoryBus.read8(0x0f0));
        assertEquals(0xcc, memoryBus.read8(0x0f1));
        assertEquals(0xcc02, cpu.getPc());
    }

    @Test
    void testJSR_20_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                JSR_0x20, 0x0b, 0x06,
                LDA_0xA9, 0x0A,
                LDA_0xA9, 0x0B,
                LDA_0xA9, 0x0C,
                LDA_0xA9, 0x0D,
                LDX_0xA2, 0x0F,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setSp(0xFF);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x02, memoryBus.read8(0x1FE));
        assertEquals(0x06, memoryBus.read8(0x1FF));
        assertEquals(0xFD, cpu.getSp());
        assertEquals(0, cpu.getRegA());
        assertEquals(0x0F, cpu.getRegX());
        assertEquals(0x60e, cpu.getPc());
    }

    @Test
    void testJSR_20_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                BNE_0xD0, 0x05,
                LDA_0xA9, 0x0A,
                BRK,
                LDA_0xA9, 0x0B,
                JSR_0x20, 0x02, 0x06,
                LDX_0xA2, 0x0C,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setSp(0xFF);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x09, memoryBus.read8(0x1FE));
        assertEquals(0x06, memoryBus.read8(0x1FF));
        assertEquals(0xFD, cpu.getSp());
        assertEquals(0x0A, cpu.getRegA());
        assertEquals(0x00, cpu.getRegX());
        assertEquals(0x605, cpu.getPc());
    }

    @Test
    void testLDA_A9_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0xA9);
        memoryBus.write8(1, 0x0F);
        memoryBus.write8(2, 0x00);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0x0F, cpu.getRegA());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(3, cpu.getPc());
    }

    @Test
    void testLDA_A9_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0xA9);
        memoryBus.write8(1, 0x00);
        memoryBus.write8(2, 0x00);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setRegA(100);
        cpu.interpret(memoryBus);

        assertEquals(0x00, cpu.getRegA());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(3, cpu.getPc());
    }

    @Test
    void testLDA_A9_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0xA9);
        memoryBus.write8(1, 0b10000011);
        memoryBus.write8(2, 0x00);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0b10000011, cpu.getRegA());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(3, cpu.getPc());
    }

    @Test
    void testLDX_A2_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0xA2);
        memoryBus.write8(1, 0x0F);
        memoryBus.write8(2, 0x00);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0x0F, cpu.getRegX());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(3, cpu.getPc());
    }

    @Test
    void testLDX_A9_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0xA2);
        memoryBus.write8(1, 0x00);
        memoryBus.write8(2, 0x00);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setRegX(100);
        cpu.interpret(memoryBus);

        assertEquals(0x00, cpu.getRegX());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(3, cpu.getPc());
    }

    @Test
    void testLDX_A9_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0xA2);
        memoryBus.write8(1, 0b10000011);
        memoryBus.write8(2, 0x00);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0b10000011, cpu.getRegX());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(3, cpu.getPc());
    }

    @Test
    void testLDY_A2_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0xA0);
        memoryBus.write8(1, 0x0F);
        memoryBus.write8(2, 0x00);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0x0F, cpu.getRegY());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(3, cpu.getPc());
    }

    @Test
    void testLDY_A9_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0xA0);
        memoryBus.write8(1, 0x00);
        memoryBus.write8(2, 0x00);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setRegY(100);
        cpu.interpret(memoryBus);

        assertEquals(0x00, cpu.getRegY());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(3, cpu.getPc());
    }

    @Test
    void testLDY_A9_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0xA0);
        memoryBus.write8(1, 0b10000011);
        memoryBus.write8(2, 0x00);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0b10000011, cpu.getRegY());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(3, cpu.getPc());
    }

    @Test
    void testLSR_4A_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b11110000,
                LSR_0x4A,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(0x604, cpu.getPc());
        assertEquals(0b01111000, cpu.getRegA());
    }

    @Test
    void testLSR_4A_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b01111110,
                LSR_0x4A,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(0x604, cpu.getPc());
        assertEquals(0b00111111, cpu.getRegA());
    }

    @Test
    void testLSR_4A_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b00000001,
                LSR_0x4A,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaC());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(0x604, cpu.getPc());
        assertEquals(0b00000000, cpu.getRegA());
    }

    @Test
    void testLSR_4A_test4() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b00000000,
                LSR_0x4A,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(0x604, cpu.getPc());
        assertEquals(0b00000000, cpu.getRegA());
    }


    @Test
    void testLSR_46_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b11110000,
                STA_0x85, 0x10,
                LSR_0x46, 0x10,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(0x607, cpu.getPc());
        assertEquals(0b01111000, memoryBus.read8(0x10));
    }

    @Test
    void testLSR_46_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b01111110,
                STA_0x85, 0x10,
                LSR_0x46, 0x10,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(0x607, cpu.getPc());
        assertEquals(0b00111111, memoryBus.read8(0x10));
    }

    @Test
    void testLSR_46_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b00000001,
                STA_0x85, 0x10,
                LSR_0x46, 0x10,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaC());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(0x607, cpu.getPc());
        assertEquals(0b00000000, memoryBus.read8(0x10));
    }

    @Test
    void testLSR_46_test4() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b00000000,
                STA_0x85, 0x10,
                LSR_0x46, 0x10,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaC());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(0x607, cpu.getPc());
        assertEquals(0b00000000, memoryBus.read8(0x10));
    }

    @Test
    void testNOP_EA_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                NOP_0xEA,
                NOP_0xEA,
                NOP_0xEA,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(4, cpu.getPc());
    }

    @Test
    void testORA_09_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0xF0,
                ORA_0x09, 0x0F,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(5, cpu.getPc());
        assertEquals(0xFF, cpu.getRegA());
    }

    @Test
    void testORA_09_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0x0F,
                ORA_0x09, 0xF0,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(5, cpu.getPc());
        assertEquals(0xFF, cpu.getRegA());
    }

    @Test
    void testORA_09_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0x00,
                ORA_0x09, 0xAB,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(5, cpu.getPc());
        assertEquals(0xAB, cpu.getRegA());
    }

    @Test
    void testORA_09_test4() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0x00,
                ORA_0x09, 0x00,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(5, cpu.getPc());
        assertEquals(0x00, cpu.getRegA());
    }

    @Test
    void testPHA_48_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDX_0xA2, 0x05,
                TXS_0x9A,
                LDA_0xA9, 0xFF,
                PHA_0x48,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(7, cpu.getPc());
        assertEquals(0x04, cpu.getSp());
        assertEquals(0xFF, memoryBus.read8(0x100 + 0x05));
    }

    @Test
    void testPHA_48_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDX_0xA2, 0x00,
                TXS_0x9A,
                LDA_0xA9, 0xFF,
                PHA_0x48,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(7, cpu.getPc());
        assertEquals(0xFF, cpu.getSp());
        assertEquals(0xFF, memoryBus.read8(0x100 + 0x00));
    }

    @Test
    void testPHP_08_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                PHP_0x08,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setStaN(1);
        cpu.setStaV(0);
        cpu.setStaB(1);
        cpu.setStaD(0);
        cpu.setStaI(1);
        cpu.setStaZ(0);
        cpu.setStaC(1);
        cpu.setSp(0x05);
        cpu.interpret(memoryBus);

        assertEquals(2, cpu.getPc());
        assertEquals(0x05 - 1, cpu.getSp());
        assertEquals(0b10110101, memoryBus.read8(0x100 + 0x05));
    }

    @Test
    void testPHP_08_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                PHP_0x08,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setStaN(0);
        cpu.setStaV(1);
        cpu.setStaB(0);
        cpu.setStaD(1);
        cpu.setStaI(0);
        cpu.setStaZ(1);
        cpu.setStaC(0);
        cpu.setSp(0x0);
        cpu.interpret(memoryBus);

        assertEquals(2, cpu.getPc());
        assertEquals(0xFF, cpu.getSp());
        assertEquals(0b01111010, memoryBus.read8(0x100 + 0x00));
    }

    @Test
    void testPLA_68_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                PLA_0x68,
                BRK);
        memoryBus.write8(0x100 + 0x05, 0xF0);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setSp(0x04);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(2, cpu.getPc());
        assertEquals(0x05, cpu.getSp());
        assertEquals(0xF0, cpu.getRegA());
    }

    @Test
    void testPLA_68_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                PLA_0x68,
                BRK);
        memoryBus.write8(0x100 + 0xFF, 0x0F);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setSp(0xFE);
        cpu.interpret(memoryBus);

        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(2, cpu.getPc());
        assertEquals(0xff, cpu.getSp());
        assertEquals(0x0F, cpu.getRegA());
    }

    @Test
    void testPLA_68_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                PLA_0x68,
                BRK);
        memoryBus.write8(0x100 + 0x05, 0x00);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setSp(0x05);
        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(2, cpu.getPc());
        assertEquals(0x06, cpu.getSp());
        assertEquals(0x00, cpu.getRegA());
    }

    @Test
    void testPLP_28_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                PLP_0x28,
                BRK);
        memoryBus.write8(0x100 + 0x05, 0b01101010);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setSp(0x04);
        cpu.interpret(memoryBus);

        assertEquals(2, cpu.getPc());
        assertEquals(0x05, cpu.getSp());
        assertEquals(0, cpu.getStaN());
        assertEquals(1, cpu.getStaV());
        assertEquals(0, cpu.getStaB());
        assertEquals(1, cpu.getStaD());
        assertEquals(0, cpu.getStaI());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaC());
    }

    @Test
    void testPLP_28_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                PLP_0x28,
                BRK);
        memoryBus.write8(0x100 + 0xFF, 0b10110101);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setSp(0xFE);
        cpu.interpret(memoryBus);

        assertEquals(2, cpu.getPc());
        assertEquals(0xFF, cpu.getSp());
        assertEquals(1, cpu.getStaN());
        assertEquals(0, cpu.getStaV());
        assertEquals(0, cpu.getStaB());
        assertEquals(0, cpu.getStaD());
        assertEquals(1, cpu.getStaI());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaC());
    }

    @Test
    void testROL_2A_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0xF0,
                ROL_0x2A);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.setSp(0xFF);
        cpu.interpret(memoryBus);

        assertEquals(0xe0, cpu.getRegA());
        assertEquals(0x01, cpu.getStaN());
        assertEquals(0x00, cpu.getStaZ());
        assertEquals(0x01, cpu.getStaC());

        assertEquals(0xFF, cpu.getSp());
        assertEquals(0x0, cpu.getRegX());
        assertEquals(0x0, cpu.getRegY());
        assertEquals(0x0604, cpu.getPc());
    }

    @Test
    void testROL_2A_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                SEC_0x38,
                LDA_0xA9, 0x02,
                ROL_0x2A);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.setSp(0xFF);
        cpu.interpret(memoryBus);

        assertEquals(0b0101, cpu.getRegA());
        assertEquals(0x00, cpu.getStaN());
        assertEquals(0x00, cpu.getStaZ());
        assertEquals(0x00, cpu.getStaC());

        assertEquals(0xFF, cpu.getSp());
        assertEquals(0x0, cpu.getRegX());
        assertEquals(0x0, cpu.getRegY());
        assertEquals(0x0605, cpu.getPc());
    }

    @Test
    void testROL_2A_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b10000000,
                ROL_0x2A);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.setSp(0xFF);
        cpu.interpret(memoryBus);

        assertEquals(0b0, cpu.getRegA());
        assertEquals(0x00, cpu.getStaN());
        assertEquals(0x01, cpu.getStaZ());
        assertEquals(0x01, cpu.getStaC());
        assertEquals(0x0604, cpu.getPc());

        assertEquals(0xFF, cpu.getSp());
        assertEquals(0x0, cpu.getRegX());
        assertEquals(0x0, cpu.getRegY());
    }

    @Test
    void testROL_2A_test4() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b0,
                ROL_0x2A);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.setSp(0xFF);
        cpu.interpret(memoryBus);

        assertEquals(0x0, cpu.getRegA());
        assertEquals(0x00, cpu.getStaN());
        assertEquals(0x01, cpu.getStaZ());
        assertEquals(0x00, cpu.getStaC());
        assertEquals(0x0604, cpu.getPc());

        assertEquals(0xFF, cpu.getSp());
        assertEquals(0x0, cpu.getRegX());
        assertEquals(0x0, cpu.getRegY());
    }

    @Test
    void testROL_26_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0xF0,
                STA_0x85, 0x10,
                ROL_0x26, 0x10);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.setSp(0xFF);
        cpu.interpret(memoryBus);

        assertEquals(0xe0, memoryBus.read8(0x10));
        assertEquals(0x01, cpu.getStaN());
        assertEquals(0x00, cpu.getStaZ());
        assertEquals(0x01, cpu.getStaC());

        assertEquals(0xFF, cpu.getSp());
        assertEquals(0x0, cpu.getRegX());
        assertEquals(0x0, cpu.getRegY());
        assertEquals(0x0607, cpu.getPc());
    }

    @Test
    void testROL_26_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                SEC_0x38,
                LDA_0xA9, 0x02,
                STA_0x85, 0x10,
                ROL_0x26, 0x10);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.setSp(0xFF);
        cpu.interpret(memoryBus);

        assertEquals(0b0101, memoryBus.read8(0x10));
        assertEquals(0x00, cpu.getStaN());
        assertEquals(0x00, cpu.getStaZ());
        assertEquals(0x00, cpu.getStaC());

        assertEquals(0xFF, cpu.getSp());
        assertEquals(0x0, cpu.getRegX());
        assertEquals(0x0, cpu.getRegY());
        assertEquals(0x0608, cpu.getPc());
    }

    @Test
    void testROL_26_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b10000000,
                STA_0x85, 0x10,
                ROL_0x26, 0x10);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.setSp(0xFF);
        cpu.interpret(memoryBus);

        assertEquals(0b0, memoryBus.read8(0x10));
        assertEquals(0x00, cpu.getStaN());
        assertEquals(0x01, cpu.getStaZ());
        assertEquals(0x01, cpu.getStaC());
        assertEquals(0x0607, cpu.getPc());

        assertEquals(0xFF, cpu.getSp());
        assertEquals(0x0, cpu.getRegX());
        assertEquals(0x0, cpu.getRegY());
    }

    @Test
    void testROL_26_test4() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0b0,
                STA_0x85, 0x10,
                ROL_0x26, 0x10);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.setSp(0xFF);
        cpu.interpret(memoryBus);

        assertEquals(0x0, memoryBus.read8(0x10));
        assertEquals(0x00, cpu.getStaN());
        assertEquals(0x01, cpu.getStaZ());
        assertEquals(0x00, cpu.getStaC());
        assertEquals(0x0607, cpu.getPc());

        assertEquals(0xFF, cpu.getSp());
        assertEquals(0x0, cpu.getRegX());
        assertEquals(0x0, cpu.getRegY());
    }

    @Test
    void testROR_6A_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0x0f,
                ROR_0x6A);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.setSp(0xFF);
        cpu.interpret(memoryBus);

        assertEquals(0x07, cpu.getRegA());
        assertEquals(0x00, cpu.getStaN());
        assertEquals(0x00, cpu.getStaZ());
        assertEquals(0x01, cpu.getStaC());
        assertEquals(0x0604, cpu.getPc());

        assertEquals(0xFF, cpu.getSp());
        assertEquals(0x0, cpu.getRegX());
        assertEquals(0x0, cpu.getRegY());
    }

    @Test
    void testROR_6A_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                SEC_0x38,
                LDA_0xA9, 0x0f,
                ROR_0x6A);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.setSp(0xFF);
        cpu.interpret(memoryBus);

        assertEquals(0x87, cpu.getRegA());
        assertEquals(0x01, cpu.getStaN());
        assertEquals(0x00, cpu.getStaZ());
        assertEquals(0x01, cpu.getStaC());
        assertEquals(0x0605, cpu.getPc());

        assertEquals(0xFF, cpu.getSp());
        assertEquals(0x0, cpu.getRegX());
        assertEquals(0x0, cpu.getRegY());
    }

    @Test
    void testROR_6A_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0x01,
                ROR_0x6A);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.setSp(0xFF);
        cpu.interpret(memoryBus);

        assertEquals(0x00, cpu.getRegA());
        assertEquals(0x00, cpu.getStaN());
        assertEquals(0x01, cpu.getStaZ());
        assertEquals(0x01, cpu.getStaC());
        assertEquals(0x0604, cpu.getPc());

        assertEquals(0xFF, cpu.getSp());
        assertEquals(0x0, cpu.getRegX());
        assertEquals(0x0, cpu.getRegY());
    }

    @Test
    void testROR_6A_test4() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0x00,
                ROR_0x6A);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.setSp(0xFF);
        cpu.interpret(memoryBus);

        assertEquals(0x00, cpu.getRegA());
        assertEquals(0x00, cpu.getStaN());
        assertEquals(0x01, cpu.getStaZ());
        assertEquals(0x00, cpu.getStaC());
        assertEquals(0x0604, cpu.getPc());

        assertEquals(0xFF, cpu.getSp());
        assertEquals(0x0, cpu.getRegX());
        assertEquals(0x0, cpu.getRegY());
    }

    @Test
    void testROR_66_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0x0f,
                STA_0x85, 0x10,
                ROR_0x66, 0x10);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.setSp(0xFF);
        cpu.interpret(memoryBus);

        assertEquals(0x07, memoryBus.read8(0x10));
        assertEquals(0x00, cpu.getStaN());
        assertEquals(0x00, cpu.getStaZ());
        assertEquals(0x01, cpu.getStaC());
        assertEquals(0x0607, cpu.getPc());

        assertEquals(0xFF, cpu.getSp());
        assertEquals(0x0, cpu.getRegX());
        assertEquals(0x0, cpu.getRegY());
    }

    @Test
    void testROR_66_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                SEC_0x38,
                LDA_0xA9, 0x0f,
                STA_0x85, 0x10,
                ROR_0x66, 0x10);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.setSp(0xFF);
        cpu.interpret(memoryBus);

        assertEquals(0x87, memoryBus.read8(0x10));
        assertEquals(0x01, cpu.getStaN());
        assertEquals(0x00, cpu.getStaZ());
        assertEquals(0x01, cpu.getStaC());
        assertEquals(0x0608, cpu.getPc());

        assertEquals(0xFF, cpu.getSp());
        assertEquals(0x0, cpu.getRegX());
        assertEquals(0x0, cpu.getRegY());
    }

    @Test
    void testROR_66_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0x01,
                STA_0x85, 0x10,
                ROR_0x66, 0x10);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.setSp(0xFF);
        cpu.interpret(memoryBus);

        assertEquals(0x00, memoryBus.read8(0x10));
        assertEquals(0x00, cpu.getStaN());
        assertEquals(0x01, cpu.getStaZ());
        assertEquals(0x01, cpu.getStaC());
        assertEquals(0x0607, cpu.getPc());

        assertEquals(0xFF, cpu.getSp());
        assertEquals(0x0, cpu.getRegX());
        assertEquals(0x0, cpu.getRegY());
    }

    @Test
    void testROR_66_test4() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDA_0xA9, 0x00,
                STA_0x85, 0x10,
                ROR_0x66, 0x10);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.setSp(0xFF);
        cpu.interpret(memoryBus);

        assertEquals(0x00, memoryBus.read8(0x10));
        assertEquals(0x00, cpu.getStaN());
        assertEquals(0x01, cpu.getStaZ());
        assertEquals(0x00, cpu.getStaC());
        assertEquals(0x0607, cpu.getPc());

        assertEquals(0xFF, cpu.getSp());
        assertEquals(0x0, cpu.getRegX());
        assertEquals(0x0, cpu.getRegY());
    }

    @Test
    void testRTI_40_test4() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                /*0600*/LDA_0xA9, 0xFF,
                /*0602*/LDX_0xA2, 0xFF,
                /*0604*/LDY_0xA0, 0xFF,
                /*0606*/BRK, 0x00,//BRK and padding byte
                /*0608*/LDA_0xA9, 0xA,
                /*060A*/CLC_0x18,
                /*060B*/LDX_0xA2, 0xB,
                /*060D*/LDY_0xA0, 0xC,
                /*060F*/RTI_0x40);

        memoryBus.write8(0xFE, 0x0D);
        memoryBus.write8(0xFF, 0x06);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(CLC_0x18);
        cpu.setPc(0x600);
        cpu.setSp(0xFF);
        cpu.interpret(memoryBus);

        assertEquals(0x608, memoryBus.read16(0x100 | 0xFE));
        assertEquals(0b10110000, memoryBus.read8(0x100 | 0xFD));
        assertEquals(0x0A, cpu.getRegA());
        assertEquals(0xFF, cpu.getRegX());
        assertEquals(0x0C, cpu.getRegY());
        assertEquals(0x00, cpu.getStaB());
        assertEquals(0x60B, cpu.getPc());
        assertEquals(0xFF, cpu.getSp());
    }

    /**
     * Address  Hexdump   Dissassembly
     * -------------------------------
     * $0600    20 09 06  JSR $0609
     * $0603    20 0c 06  JSR $060c
     * $0606    20 12 06  JSR $0612
     * $0609    a2 00     LDX #$00
     * $060b    60        RTS
     * $060c    e8        INX
     * $060d    e0 05     CPX #$05
     * $060f    d0 fb     BNE $060c
     * $0611    60        RTS
     * $0612    00        BRK
     */
    @Test
    void testRTS_60_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                JSR_0x20, 0x09, 0x06,
                JSR_0x20, 0x0c, 0x06,
                JSR_0x20, 0x12, 0x06,
                LDX_0xA2, 0x00,
                RTS_0x60,
                INX_0xE8,
                CPX_0xE0, 0x05,
                BNE_0xD0, 0xfb,
                RTS_0x60,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setSp(0xFF);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x08, memoryBus.read8(0x1FE));
        assertEquals(0x06, memoryBus.read8(0x1FF));
        assertEquals(0xfd, cpu.getSp());
        assertEquals(0x05, cpu.getRegX());
        assertEquals(0x613, cpu.getPc());
    }

    @Test
    void testSBC_E9_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                SEC_0x38,
                LDA_0xA9, 0x50,
                SBC_0xE9, 0xF0);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setSp(0xFF);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x00, cpu.getStaC());
        assertEquals(0x00, cpu.getStaZ());
        assertEquals(0x00, cpu.getStaV());
        assertEquals(0x00, cpu.getStaN());
        assertEquals(0x60, cpu.getRegA());
        assertEquals(0x00, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x606, cpu.getPc());
    }

    @Test
    void testSBC_E9_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                CLC_0x18,
                LDA_0xA9, 0x50,
                SBC_0xE9, 0xF0);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setSp(0xFF);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x00, cpu.getStaC());
        assertEquals(0x00, cpu.getStaZ());
        assertEquals(0x00, cpu.getStaV());
        assertEquals(0x00, cpu.getStaN());
        assertEquals(0x5F, cpu.getRegA());
        assertEquals(0x00, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x606, cpu.getPc());
    }

    @Test
    void testSBC_E9_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                CLC_0x18,
                LDA_0xA9, 0x50,
                SBC_0xE9, 0xB0);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setSp(0xFF);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x00, cpu.getStaC());
        assertEquals(0x00, cpu.getStaZ());
        assertEquals(0x01, cpu.getStaV());
        assertEquals(0x01, cpu.getStaN());
        assertEquals(0x9F, cpu.getRegA());
        assertEquals(0x00, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x606, cpu.getPc());
    }

    @Test
    void testSBC_E9_test4() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                SEC_0x38,
                LDA_0xA9, 0x50,
                SBC_0xE9, 0xB0);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setSp(0xFF);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x00, cpu.getStaC());
        assertEquals(0x00, cpu.getStaZ());
        assertEquals(0x01, cpu.getStaV());
        assertEquals(0x01, cpu.getStaN());
        assertEquals(0xA0, cpu.getRegA());
        assertEquals(0x00, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x606, cpu.getPc());
    }

    @Test
    void testSBC_E9_test5() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                SEC_0x38,
                LDA_0xA9, 0xD0,
                SBC_0xE9, 0x70);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setSp(0xFF);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x01, cpu.getStaC());
        assertEquals(0x00, cpu.getStaZ());
        assertEquals(0x01, cpu.getStaV());
        assertEquals(0x00, cpu.getStaN());
        assertEquals(0x60, cpu.getRegA());
        assertEquals(0x00, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x606, cpu.getPc());
    }

    @Test
    void testSBC_E9_test6() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                CLC_0x18,
                LDA_0xA9, 0xD0,
                SBC_0xE9, 0x70);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setSp(0xFF);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x01, cpu.getStaC());
        assertEquals(0x00, cpu.getStaZ());
        assertEquals(0x01, cpu.getStaV());
        assertEquals(0x00, cpu.getStaN());
        assertEquals(0x5F, cpu.getRegA());
        assertEquals(0x00, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x606, cpu.getPc());
    }

    @Test
    void testSBC_E9_test7() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                CLC_0x18,
                LDA_0xA9, 0xD0,
                SBC_0xE9, 0x30);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setSp(0xFF);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x01, cpu.getStaC());
        assertEquals(0x00, cpu.getStaZ());
        assertEquals(0x00, cpu.getStaV());
        assertEquals(0x01, cpu.getStaN());
        assertEquals(0x9F, cpu.getRegA());
        assertEquals(0x00, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x606, cpu.getPc());
    }

    @Test
    void testSBC_E9_test8() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                SEC_0x38,
                LDA_0xA9, 0xD0,
                SBC_0xE9, 0x30);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setSp(0xFF);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x01, cpu.getStaC());
        assertEquals(0x00, cpu.getStaZ());
        assertEquals(0x00, cpu.getStaV());
        assertEquals(0x01, cpu.getStaN());
        assertEquals(0xA0, cpu.getRegA());
        assertEquals(0x00, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x606, cpu.getPc());
    }

    @Test
    void testSBC_E9_test9() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                SEC_0x38,
                LDA_0xA9, 0x03,
                SBC_0xE9, 0x03);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setSp(0xFF);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x01, cpu.getStaC());
        assertEquals(0x01, cpu.getStaZ());
        assertEquals(0x00, cpu.getStaV());
        assertEquals(0x00, cpu.getStaN());
        assertEquals(0x00, cpu.getRegA());
        assertEquals(0x00, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x606, cpu.getPc());
    }

    @Test
    void testSBC_E9_test10() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                CLC_0x18,
                LDA_0xA9, 0x03,
                SBC_0xE9, 0x03);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setSp(0xFF);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x00, cpu.getStaC());
        assertEquals(0x00, cpu.getStaZ());
        assertEquals(0x00, cpu.getStaV());
        assertEquals(0x01, cpu.getStaN());
        assertEquals(0xFF, cpu.getRegA());
        assertEquals(0x00, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x606, cpu.getPc());
    }

    @Test
    void testSBC_E9_test11() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                SEC_0x38,
                LDA_0xA9, 0x03,
                SBC_0xE9, 0x02);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setSp(0xFF);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x01, cpu.getStaC());
        assertEquals(0x00, cpu.getStaZ());
        assertEquals(0x00, cpu.getStaV());
        assertEquals(0x00, cpu.getStaN());
        assertEquals(0x01, cpu.getRegA());
        assertEquals(0x00, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x606, cpu.getPc());
    }

    @Test
    void testSBC_E9_test12() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                CLC_0x18,
                LDA_0xA9, 0x03,
                SBC_0xE9, 0x02);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setSp(0xFF);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x01, cpu.getStaC());
        assertEquals(0x01, cpu.getStaZ());
        assertEquals(0x00, cpu.getStaV());
        assertEquals(0x00, cpu.getStaN());
        assertEquals(0x00, cpu.getRegA());
        assertEquals(0x00, cpu.getRegX());
        assertEquals(0x00, cpu.getRegY());
        assertEquals(0x606, cpu.getPc());
    }

    @Test
    void testSEC_38_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0x38);
        memoryBus.write8(1, 0x00);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setStaC(0);

        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaC());
        assertEquals(2, cpu.getPc());
    }

    @Test
    void testSED_F8_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0xF8);
        memoryBus.write8(1, 0x00);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setStaD(0);

        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaD());
        assertEquals(2, cpu.getPc());
    }

    @Test
    void testSEI_78_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0x78);
        memoryBus.write8(1, 0x00);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.setStaI(0);

        cpu.interpret(memoryBus);

        assertEquals(1, cpu.getStaI());
        assertEquals(2, cpu.getPc());
    }

    @Test
    void testSTA_85_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0x05,
                STA_0x85, 0x08,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(0x08, memoryBus.read8(0x03));
        assertEquals(5, cpu.getPc());
        assertEquals(0x05, cpu.getRegA());
    }

    @Test
    void testSTX_86_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDX_0xA2, 0x05,
                STX_0x86, 0x08,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(0x08, memoryBus.read8(0x03));
        assertEquals(5, cpu.getPc());
        assertEquals(0x05, cpu.getRegX());
    }

    @Test
    void testSTY_84_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDY_0xA0, 0x05,
                STY_0x84, 0x08,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0);
        cpu.interpret(memoryBus);

        assertEquals(0x08, memoryBus.read8(0x03));
        assertEquals(5, cpu.getPc());
        assertEquals(0x05, cpu.getRegY());
    }

    @Test
    void testTAX_AA_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0xA9);
        memoryBus.write8(1, 0x0F);
        memoryBus.write8(2, 0xAA);
        memoryBus.write8(3, 0x00);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0x0F, cpu.getRegX());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testTAX_AA_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0xA9);
        memoryBus.write8(1, 0x00);
        memoryBus.write8(2, 0xAA);
        memoryBus.write8(3, 0x00);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0x00, cpu.getRegX());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testTAX_AA_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0, 0xA9);
        memoryBus.write8(1, 0b10000011);
        memoryBus.write8(2, 0xAA);
        memoryBus.write8(3, 0x00);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0b10000011, cpu.getRegX());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testTAY_A8_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0x05,
                TAY_0xA8,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0x05, cpu.getRegY());
        assertEquals(0, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testTAY_A8_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0xFF,
                TAY_0xA8,
                BRK);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0xFF, cpu.getRegY());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testTAY_A8_test3() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDA_0xA9, 0x00,
                TAY_0xA8,
                BRK);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setRegA(0xFF);
        cpu.interpret(memoryBus);

        assertEquals(0x00, cpu.getRegY());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testTSX_BA_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                TSX_0xBA, 0x00,
                BRK);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setSp(0xF0);
        cpu.interpret(memoryBus);

        assertEquals(0xF0, cpu.getRegX());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(2, cpu.getPc());
    }

    @Test
    void testTSX_BA_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                TSX_0xBA, 0x00,
                BRK);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setSp(0x00);
        cpu.interpret(memoryBus);

        assertEquals(0x00, cpu.getRegX());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(2, cpu.getPc());
    }

    @Test
    void testTXA_8A_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDX_0xA2, 0xF0,
                TXA_0x8A,
                BRK);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0xF0, cpu.getRegA());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testTXA_8A_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDX_0xA2, 0x00,
                TXA_0x8A,
                BRK);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0x00, cpu.getRegA());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testTXS_9A_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDX_0xA2, 0xF0,
                TXS_0x9A,
                BRK);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0xF0, cpu.getSp());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testTYA_98_test1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDY_0xA0, 0xF0,
                TYA_0x98,
                BRK);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0xF0, cpu.getRegA());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testTYA_98_test2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0,
                LDY_0xA0, 0x00,
                TYA_0x98,
                BRK);
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.interpret(memoryBus);

        assertEquals(0x00, cpu.getRegA());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaN());
        assertEquals(4, cpu.getPc());
    }

    @Test
    void testGetStatusByte1() {
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setStaN(1);
        cpu.setStaV(0);
        cpu.setStaB(1);
        cpu.setStaD(0);
        cpu.setStaI(1);
        cpu.setStaZ(0);
        cpu.setStaC(1);

        assertEquals(0b10110101, cpu.getStatusByte());
    }

    @Test
    void testGetStatusByte2() {
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setStaN(0);
        cpu.setStaV(1);
        cpu.setStaB(0);
        cpu.setStaD(1);
        cpu.setStaI(0);
        cpu.setStaZ(1);
        cpu.setStaC(0);

        assertEquals(0b01101010, cpu.getStatusByte());
    }

    @Test
    void testSetStatusByte1() {
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setStatusByte(0b10110101);

        assertEquals(1, cpu.getStaN());
        assertEquals(0, cpu.getStaV());
        assertEquals(1, cpu.getStaB());
        assertEquals(0, cpu.getStaD());
        assertEquals(1, cpu.getStaI());
        assertEquals(0, cpu.getStaZ());
        assertEquals(1, cpu.getStaC());
    }

    @Test
    void testSetStatusByte2() {
        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setStatusByte(0b01101010);

        assertEquals(0, cpu.getStaN());
        assertEquals(1, cpu.getStaV());
        assertEquals(0, cpu.getStaB());
        assertEquals(1, cpu.getStaD());
        assertEquals(0, cpu.getStaI());
        assertEquals(1, cpu.getStaZ());
        assertEquals(0, cpu.getStaC());
    }

    @Test
    void testLoop() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        write8Array(memory, 0x600,
                LDX_0xA2, 0x00,
                NOP_0xEA,
                NOP_0xEA,
                DEX_0xCA,
                BNE_0xD0, 0xFB);

        Cpu cpu = new Cpu();
        cpu.breakOnOpCode(BRK);
        cpu.setPc(0x600);
        cpu.interpret(memoryBus);

        assertEquals(0x0, cpu.getRegA());
        assertEquals(0x0, cpu.getRegX());
        assertEquals(0x0, cpu.getRegY());
        assertEquals(0x0608, cpu.getPc());
    }
}