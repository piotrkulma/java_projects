package pl.piotrkulma.nes.cpu.addressing;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.piotrkulma.nes.cpu.Cpu;
import pl.piotrkulma.nes.memory.Memory;
import pl.piotrkulma.nes.memory.MemoryBus;

import static org.junit.jupiter.api.Assertions.*;

class AddressingModeTest {
    private static AddressingMode addressingMode;

    @BeforeAll
    public static void init() {
        addressingMode = AddressingMode.getInstance();
    }

    @Test
    public void testImmediate() {
        Cpu cpu = new Cpu();
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        int address = addressingMode.immediate(cpu, memoryBus);

        assertEquals(0, address);
        assertEquals(1, cpu.getPc());
    }

    @Test
    public void testZeroPage() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(100, 0xF0);
        Cpu cpu = new Cpu();
        cpu.setPc(100);

        int address = addressingMode.zeroPage(cpu, memoryBus);

        assertEquals(0xF0, address);
        assertEquals(101, cpu.getPc());
    }

    @Test
    public void testZeroPageX1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(100, 0xF0);
        Cpu cpu = new Cpu();
        cpu.setRegX(1);
        cpu.setPc(100);

        int address = addressingMode.zeroPageX(cpu, memoryBus);

        assertEquals(0xF1, address);
        assertEquals(101, cpu.getPc());
    }

    @Test
    public void testZeroPageX2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(100, 0xFF);
        Cpu cpu = new Cpu();
        cpu.setRegX(2);
        cpu.setPc(100);

        int address = addressingMode.zeroPageX(cpu, memoryBus);

        assertEquals(1, address);
        assertEquals(101, cpu.getPc());
    }

    @Test
    public void testZeroPageY1() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(100, 0xF0);
        Cpu cpu = new Cpu();
        cpu.setRegY(1);
        cpu.setPc(100);

        int address = addressingMode.zeroPageY(cpu, memoryBus);

        assertEquals(0xF1, address);
        assertEquals(101, cpu.getPc());
    }

    @Test
    public void testZeroPageY2() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(100, 0xFF);
        Cpu cpu = new Cpu();
        cpu.setRegY(2);
        cpu.setPc(100);

        int address = addressingMode.zeroPageY(cpu, memoryBus);

        assertEquals(1, address);
        assertEquals(101, cpu.getPc());
    }

    @Test
    public void testAbsolute() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0x01, 0xCD);
        memoryBus.write8(0x02, 0xAB);

        Cpu cpu = new Cpu();
        cpu.setPc(1);

        int address = addressingMode.absolute(cpu, memoryBus);

        assertEquals(0xABCD, address);
        assertEquals(3, cpu.getPc());
    }

    @Test
    public void testAbsoluteX() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0x01, 0xC0);
        memoryBus.write8(0x02, 0xAB);

        Cpu cpu = new Cpu();
        cpu.setPc(1);
        cpu.setRegX(1);

        int address = addressingMode.absoluteX(cpu, memoryBus);

        assertEquals(0xABC1, address);
        assertEquals(3, cpu.getPc());
    }

    @Test
    public void testAbsoluteY() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0x01, 0xC0);
        memoryBus.write8(0x02, 0xAB);

        Cpu cpu = new Cpu();
        cpu.setPc(1);
        cpu.setRegY(1);

        int address = addressingMode.absoluteY(cpu, memoryBus);

        assertEquals(0xABC1, address);
        assertEquals(3, cpu.getPc());
    }

    @Test
    public void testIndexedIndirect() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0x01, 0xA5 - 5);
        memoryBus.write8(0xA5, 0xCD);
        memoryBus.write8(0xA6, 0xAB);

        Cpu cpu = new Cpu();
        cpu.setPc(1);
        cpu.setRegX(5);

        int address = addressingMode.indexedIndirect(cpu, memoryBus);

        assertEquals(0xABCD, address);
        assertEquals(2, cpu.getPc());
    }

    @Test
    public void testIndirectIndexed() {
        Memory memory = new Memory();
        MemoryBus memoryBus = new MemoryBus(memory);
        memoryBus.setAccessControl(false);
        memoryBus.write8(0x01, 0xA0);
        memoryBus.write8(0xA0, 0xCD - 5);
        memoryBus.write8(0xA1, 0xAB);

        Cpu cpu = new Cpu();
        cpu.setPc(1);
        cpu.setRegY(5);

        int address = addressingMode.indirectIndexed(cpu, memoryBus);

        assertEquals(0xABCD, address);
        assertEquals(2, cpu.getPc());
    }
}