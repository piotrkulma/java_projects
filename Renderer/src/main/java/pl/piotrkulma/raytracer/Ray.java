package pl.piotrkulma.raytracer;

public final class Ray {
    private Vector3 origin;
    private Vector3 direction;

    public Ray(Vector3 origin, Vector3 direction) {
        this.origin = new Vector3(origin);
        this.direction = new Vector3(direction);
    }

    public Vector3 at(double t) {
        return origin.add(direction.mult(t));
    }



}
