package pl.piotrkulma.raytracer;

public final class Vector3 {
    private double e[];

    public Vector3() {
        e = new double[3];
        e[0] = 0;
        e[1] = 0;
        e[2] = 0;
    }

    public Vector3(Vector3 vec) {
        e = new double[3];
        e[0] = vec.getElements()[0];
        e[1] = vec.getElements()[1];
        e[2] = vec.getElements()[2];
    }

    public Vector3(double e0, double e1, double e2) {
        e = new double[3];
        e[0] = e0;
        e[1] = e1;
        e[2] = e2;
    }

    public Vector3 add(Vector3 vec) {
        return new Vector3(
                e[0] + vec.e[0],
                e[1] + vec.e[1],
                e[2] + vec.e[2]);
    }

    public Vector3 sub(Vector3 vec) {
        return new Vector3(
                e[0] - vec.e[0],
                e[1] - vec.e[1],
                e[2] - vec.e[2]);
    }

    public Vector3 mult(Vector3 vec) {
        return new Vector3(
                e[0] * vec.e[0],
                e[1] * vec.e[1],
                e[2] * vec.e[2]);
    }

    public Vector3 mult(double t) {
        return new Vector3(
                e[0] * t,
                e[1] * t,
                e[2] * t);
    }

    public double dot(Vector3 vec) {
        return e[0] * vec.e[0] +
                e[1] * vec.e[1] +
                e[2] * vec.e[2];
    }

    public Vector3 cross(Vector3 vec) {
        return new Vector3(
                e[1] * vec.e[2] - e[2] * vec.e[1],
                e[2] * vec.e[0] - e[0] * vec.e[2],
                e[0] * vec.e[1] - e[1] * vec.e[0]);
    }

    public Vector3 div(double t) {
        double inverseScalar = 1.0d / t;
        return new Vector3(
                e[0] * inverseScalar,
                e[1] * inverseScalar,
                e[2] * inverseScalar);
    }

    public double length() {
        return Math.sqrt(lengthSquared());
    }

    public double lengthSquared() {
        return e[0] * e[0] + e[1] * e[1] + e[2] * e[2];
    }

    public double[] getElements() {
        return e.clone();
    }
}
