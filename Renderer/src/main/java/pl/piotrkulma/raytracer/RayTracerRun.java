package pl.piotrkulma.raytracer;

import pl.piotrkulma.raytracer.utils.ColorUtility;
import pl.piotrkulma.raytracer.utils.FileWriter;

/**
 * Based on https://raytracing.github.io/books/RayTracingInOneWeekend.html
 */

public class RayTracerRun {
    private final static int IMAGE_WIDTH = 256;
    private final static int IMAGE_HEIGHT = 256;

    public static void main(String[] args) {
        RayTracerRun rayTracerRun = new RayTracerRun();
        rayTracerRun.run();
    }

    public void run() {
        FileWriter fileWriter = new FileWriter();
        fileWriter.append("P3\n%s %s\n255\n".formatted(IMAGE_WIDTH, IMAGE_HEIGHT));

        for (int j = IMAGE_HEIGHT - 1; j >= 0; --j) {
            for (int i = 0; i < IMAGE_WIDTH; ++i) {
                System.out.println(i + " " + j);
                Vector3 pixelColor = new Vector3(
                        (double) i / (IMAGE_WIDTH - 1),
                        (double) j / (IMAGE_HEIGHT - 1),
                        0.25d);

                ColorUtility.writeColor(fileWriter, pixelColor);
            }

        }

        fileWriter.saveToFile("src/main/resources/test.ppm");
    }
}
