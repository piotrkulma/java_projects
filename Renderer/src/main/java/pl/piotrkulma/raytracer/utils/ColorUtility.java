package pl.piotrkulma.raytracer.utils;

import pl.piotrkulma.raytracer.Vector3;

public final class ColorUtility {
    public static void writeColor(FileWriter fileWriter, Vector3 color) {
        double[] colors = color.getElements();
        fileWriter.append("%s %s %s\n".formatted(
                (int)(255.999 * colors[0]),
                (int)(255.999 * colors[1]),
                (int)(255.999 * colors[2])));
    }
}
