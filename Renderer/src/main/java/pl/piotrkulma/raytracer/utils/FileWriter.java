package pl.piotrkulma.raytracer.utils;

import java.io.BufferedWriter;

public final class FileWriter {
    private StringBuffer dataBuffer;

    public FileWriter() {
        this.dataBuffer = new StringBuffer();
    }

    public void append(String string) {
        dataBuffer.append(string);
    }

    public void saveToFile(String path) {
        try {
            try (java.io.FileWriter ppmFileWriter = new java.io.FileWriter(path)) {
                try (BufferedWriter bufferedWriter = new BufferedWriter(ppmFileWriter)) {
                    bufferedWriter.write(dataBuffer.toString());
                    bufferedWriter.flush();
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("Exception while saving to file.", e);
        }
    }
}
